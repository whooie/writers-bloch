# Writer's Bloch

<div style="text-align: center; width: 50%; margin: auto auto;">
    <img src="assets/logo.svg" style="width: 100%"></img>
</div>

Welcome to my blog. Here you will find a collection of writings -- mostly
related to physics, programming, sometimes math, and some half-baked
pseudo-philosophical extensions thereof -- that I use mostly to just record and
structure my shower thoughts.
