# Summary

[Writer's Bloch](README.md)

---

- [Introduction](introduction.md)

- [Efficient simulation of quantum circuits](efficient-sim.md)

- [A swift introduction to matrix product states](mps.md)

- [`Maybe` from first principles](monads.md)

<!-- - [ZX calculator](zx-calculator/README.md) -->
<!--     - [Introduction to the ZX-calculus](zx-calculator/intro.md) -->
<!--     - [Naive ket-bra calculator](zx-calculator/naive.md) -->
<!--     - [Proper graph-based implementation](zx-calculator/graph.md) -->
- [ZX calculator]()
    - [Introduction to the ZX-calculus]()
    - [Naive ket-bra calculator]()
    - [Proper graph-based implementation]()

<!-- - [High-level quantum computing](high-level-quantum-computing/README.md) -->

<!-- - [Thoughts on OCaml](thoughts-on-ocaml.md) -->
- [Thoughts on OCaml]()

<!-- - [The universe is made of springs](harmonic-oscillators.md) -->
- [The universe is made of springs]()

<!-- - [Social couplings](coupling-constants.md) -->
<!-- - [Social couplings]() -->

<!-- - [The present state of things must be reckoned with](things-are-bad.md) -->
- [The present state of things must be reckoned with]()

<!-- - [Conceptual thermodynamic limits](definitions-as-thermodynamic-limits.md) -->
- [Conceptual thermodynamic limits]()

<!-- - [One must imagine Schrödinger happy](one-must-imagine-schrodinger-happy.md) -->
- [One must imagine Schrödinger happy]()

<!-- - [The `[0, 1]` interval](the-zero-one-interval.md) -->
<!-- - [The `[0, 1]` interval]() -->

<!-- - [The meta-perspective](the-meta-perspective.md) -->
- [The meta-perspective]()

<!-- - [The regime of reasonable assumptions](the-regime-of-reasonable-assumptions.md) -->
- [The regime of reasonable assumptions]()

<!-- - [Psychology is dumb](psychology-is-dumb.md) -->
- [Psychology is dumb]()

<!-- - [The conceptual basis](the-conceptual-basis.md) -->
- [The conceptual basis]()

<!-- - [Mental illnesses and addictions as memes](mental-illnesses-addictions.md) -->
- [Mental illnesses and addictions as memes]()

<!-- - [Echo chambers with light](echo-chambers-with-light.md) -->
- [Echo chambers with light]()

<!-- - [Just how I'm feeling](just-feelings.md) -->
- [Just how I'm feeling]()

<!-- - [Representations of computers and the internet](representations-of-the-internet.md) -->
- [Representations of computers and the internet]()

<!-- - [Character portrayals in international media](international-characters.md) -->
- [Character portrayals in international media]()

<!-- - [The optimal amount of social media is not zero](social-media-as-pollution.md) -->
<!-- - [The optimal amount of social media is not zero]() -->

<!-- - [Unrealized degrees of freedom](unrealized-degrees-of-freedom.md) -->
- [Unrealized degrees of freedom]()

<!-- - [Philosophies relating to subjective experiences are stupid](subjective-philosophy.md) -->
- [Philosophies relating to subjective experiences are stupid]()

<!-- - [Those who repeat aphorisms are doomed to ignorance](learning-history.md) -->
- [Those who repeat aphorisms are doomed to ignorance]()

<!-- - [Thermal fluids](thermal-fluids.md) -->
- [Thermal fluids]()

<!-- - [You either die a masterpiece or live long enough to see yourself become a parody](artistic-decay.md) -->
- [You either die a masterpiece or live long enough to see yourself become a parody]()

<!-- - [*Oppenheimer* is the most overrated piece of shit I've had the displeasure of watching](christopher-nolan-sucks.md) -->
- [*Oppenheimer* is the most overrated piece of shit I've had the displeasure of watching]()

- [Supplementary material](supplements/README.md)
    - [`Maybe` from first principles](supplements/monads/contents.md)
        - [Monoids and monoidal categories](supplements/monads/monoidal-categories.md)
        - [`State` usage example](supplements/monads/state-example.md)

---

- [Random math notes](math/README.md)
    - [Square pulse Fourier transform](math/square-pulse.md)
    - [Gaussian wave packet Fourier transform](math/gaussian-wave-packet.md)
    - [Solving the harmonic oscillator problem the cooler way](math/laplace-harmonic-oscillator.md)
    - [Building Hamiltonians for simulation](math/building-hamiltonians.md)
    - [Working with product-space states](math/product-space-states.md)
    - [Numerical evolution of coordinate-space wavefunctions](math/coord-space-evolution.md)
    - [The Markov chain view of the Schrodinger equation](math/markov-chain-tdse.md)
    - [Calculation of non-planar ZX-diagrams](math/non-planar-zx-diagrams.md)

- [Recipes](recipes/README.md)
    - [Chocolate Chip Cookies](recipes/chocolate-chip-cookies.md)
    - [Miso-Marinated Salmon](recipes/miso-marinated-salmon.md)
    - [Split Pea Soup with Ham](recipes/split-pea-soup.md)
    - [Pork Belly Stew](recipes/pork-belly-stew.md)
    - [Imitation Unagi Sauce](recipes/imitation-unagi-sauce.md)
    - [Sourdough English Muffins](recipes/sourdough-english-muffins.md)
    - [Ragù alla Bolognese](recipes/ragu-alla-bolognese.md)
    - [Pulled Pork](recipes/pulled-pork.md)
    - [Tiramisu](recipes/tiramisu.md)
    - [Blueberry Pancakes](recipes/blueberry-pancakes.md)
    - [Chantilly Cake](recipes/chantilly-cake.md)
    - [Bread Pudding](recipes/bread-pudding.md)
    - [Scallion Oil Noodles](recipes/scallion-oil-noodles.md)
    - [Mushroom Carbonara](recipes/mushroom-carbonara.md)
    - [Chicken Tikka Masala](recipes/chicken-tikka-masala.md)
    - [Mayo-Marinated Chicken Nuggets](recipes/mayo-marinade-chicken.md)

