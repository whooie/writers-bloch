# You either die a masterpiece or live long enough to see yourself become a parody

- All art either dies or becomes a parody of itself. This is a natural
  phenomenon arising from the nature of art (it's open to interpretation; this
  causes even the idea of art to decay as well) interacting with how humans tend
  to pick out only a finite set of features that they like, and then voice
  demand for more of only those things to the creator. Or worse, where their
  demands are formed from a mix of different pieces of art. And worst of all,
  when the artist doesn't really understand what makes their work
  popular/interesting

