# Oppenheimer is the most overrated piece of shit I've had the displeasure of watching

## Now I am become death, destroyer of worlds

## Now I am become Oppy, fucker of bitches
- can we all agree that the stuff with that commie bitch was fucking awful?
- how can anyone think that the scene where Oppenheimer reads the "now I am
  become death" passage *while she's riding him* is anything but terrible?
    - all the writing surrounding any sort of intellectualism is to bland and
      one-dimensional
        - "how is your algebra?"; "algebra is like music"
        - the flow of the movie is like the i-fucking-love-science version of
          Bayhem
        - ooh, aah, quantum mechanics -- so mystical and esoteric

## Now I am become Koh, stealer of faces
- the editing sucks, no time for acting
- look at the dialogue scenes and try to figure out what the character is
  thinking/feeling

## Now I am become le bomb, le killer of people
- every scene feels like it was edited to be a short clip
- too many one-liners, too many quips, too many fast cuts on dialogue
    - the very few clips that don't immediately cut after someone is finished
      speaking are decent
- the soundtrack is relentless and ruinous

## Now I am become X, Y-er of Zs
- the side plot with Strauss was boring and entirely unnecessary, like all of
  the black-and-white sections
    - focus on EITHER the bomb and the militarization of nuclear physics OR the
      politicization of the anti-nuclear perspective; covering both weakens the
      movie as a whole

