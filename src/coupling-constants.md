# Social couplings

- think about an "average coupling" to the local field of social
  currents/energies
- maybe try to draw an analogy to dispersive terms in Lagrangian densities and
  literal coupling constants?
- could be better to rephrase with a description of Schrodinger equation
  scattering off potentials -- higher energy waves resolve smaller potentials
  better

