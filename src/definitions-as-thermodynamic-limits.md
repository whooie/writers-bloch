# Definitions as thermodynamic limits

- e.g. introvert/extrovert, romantic/platonic, type A/type B
- there's always entropy in real systems (people) that these definitions don't
  capture
- actually, the definitions we use only refer to thermodynamic limits (T = 0 or
  T = \infty) a la Heisenberg spin models
- horseshoe theory |-> complex infinity / special relativity
