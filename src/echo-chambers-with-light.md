# Echo chambers with light

- "echo chambers" are more like atom-cavity systems: off-resonant modes are not
  only no supported from within, they're also reflected from the outside, and
  whatever's inside the cavity modifies the spectrum of the cavity

