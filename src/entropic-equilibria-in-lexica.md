# Entropic equilibria in definitions
- Scott Alexander wrote about hyperstitions: https://astralcodexten.substack.com/p/give-up-seventy-percent-of-the-way
- a lot of word definitions are in unstable equilibrium -- people like to talk
  poetically/figuratively and mix definitions in interesting ways, but doing so
  increases a kind of informational entropy in the set of definitions a given
  word could mean
- as the space of meanings increases, figurative usage then only tends to
  increase the space and dilute the meanings further (see definitions as
  harmonic oscillators/meaning as a conserved quantity)
- maintenance of nice definitions requires outside work -- people need to check
  for misuse

