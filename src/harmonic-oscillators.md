# The universe is made of springs

- derive the classical harmonic oscillator
- the resonance metaphor
- quality factors
- but you're constantly measuring data, not learning the underlying function
- so beware of resolution-limited measurements

- apply to words/language
- meaning and evocativeness are Fourier conjugates - quality factor of the
  resonance
- words have histories and meanings affected by both claimed dictionary
  definitions and actual usage: each time you use a word, its meaning is
  broadened proportionally to how far from the conceptual resonance you use it;
  language requires upkeep; cars also continuously decay, and it never makes
  sense to run it as hard as you can all the time

- algorithm to compute "grade level" of pieces of text: presidential speeches
  have decreasing grade level over time

