# In Defense of Feynman

Before we get into this, rest assured that I'm not going to argue that intellect
should somehow outweigh bad behavior. No, I'm going to take the hard route and
argue that the application of labels like "womanizer" and "misogynist" to
Richard Feynman *at all* is completely undeserved. And if you think otherwise,
then logically speaking, the label of "idiot" can then be rightfully applied to
you.

<!--
- re: smart people get a pass, Feynman was influential (and in some cases
  seminal) in ways most people complaining about Feynman don't even understand
-->
