# Character portrayals in international media

- watching Japanese (generally, east-Asian) live-action tv weirds me out
  sometimes
    - Japanese travel show presenters
        - tasting food: 3-4 chews then very conspicuous surprise at how good the
          food tastes
        - "oooh", "aah", "naruhodo" interjections when an interviewee is
          explaining things, in a tone of voice that sounds almost childish
    - Japanese dramas/fiction
        - conspicuous but broad vocalizations of inner thoughts
            - there's a special term introduced in dialogue and the listener
              repeats it in a murmur while frowning and looking away from the
              speaker
            - generally present in other media as well, but there seems to be
              less emphasis on a "show, don't tell" rule
        - huge acceptance/adherence to a canon of tropes
            - smart guy/eccentric genius: casual completion of well-known
              puzzles, drop-everything-and-do-math-on-the-nearest-surface,
              generally presented in an unrealistically reverent manner
            - crying scenes with child-like vocalization
            - nostalgic appreciation of food
            - greasy (possibly fat) man taking advantage of society
            - company head/important businessman or businessman-woman
            - Japanese (the language) in particular supports very distinct modes
              of speaking, many of which are only used in fiction and couple to
              behaviors that nearly every actual person accepts as things they
              would never do in real life
- these are all things that are present in other countries' media, of course,
  but when taken all together, it gives the whole thing the overall feeling of
  watching a televised stage performance
    - less dramatic/more static camera shots, unless that's explicitly the aim
        - anime is an exception because of production constraints
    - I wonder if this is something that's already been noticed/thought about,
      and whether there's a sort of reciprocity: do east Asians watch western
      movies and think everything is way too dramatic and/or feel like something
      is missing in how characters are presented
