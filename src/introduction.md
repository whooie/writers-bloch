# Doing things poorly as a method of learning

I've been accused of trying to reinvent the wheel several times in my work.
Mostly these are in reference to the programming I do, but I think it's probably
generalizable to other things I do as well.

That phrase is often used to refer to a bad practice, but in principle, I never
see this as a bad thing. Given real-world things like time constraints it can,
of course, be better to use tools created by other people, but absent these, I
always believe it's *preferable* to reinvent every "wheel" you can, even if you
end up making something that's inferior to whatever already exists. This is a
pattern that's repeated many times over, and codified in conventional knowledge:
*Practice makes the master.* This is a somewhat extreme form of what I'm trying
to say here -- I would argue for a generalized form: *It is the doing that
brings understanding.*

This is a guiding principle for me that I believe in general, and to which I
don't expect much push-back. But it has some nice properties, the foremost of
which is that it covers my ass when I'm wrong in public: If I'm wrong or doing
something poorly, all I (or anyone else) have to say is that I'm just learning.
Of course this has some caveats -- after all, one can only be so wrong before
credibility is lost -- but I mention it here because it's the principle on which
I've decided to build this blog. The blog itself and everything I talk about on
here are just notes I want to record on current projects or shower thoughts that
I have, and the thing as a whole comprises notes on developing and recording
ideas.

I'm not going to do much promotion for these writings, so if you've somehow come
across this small corner of the internet, have fun and don't @ me.

