# Just how I'm feeling

- "I'm just saying how I feel on the subject" should be considered an automatic
  forfeiture of consideration for worthy thought. 1) Who cares what you're
  feeling" What bearing does it have on anything that's real? 2) If this is how
  far you have to walk back any kind of statement to ensure accuracy/precision,
  then why should you have wanted to say anything at all?


