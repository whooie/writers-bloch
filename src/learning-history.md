# Those who repeat aphorisms are doomed to ignorance

- why should the repetition of history be avoided? We already know that war
  drives innovation in a very real way that yields huge technological
  improvements to the quality of life -- it could be that the optimal steady
  state is a slow oscillation between wartime and peacetime
- relatedly, it's dumb to say that people should know history and traditions as
  a blanket statement -- this requires an infinite amount of memory and an
  infinite amount of computing power to cross-reference everything that's
  current with everything that has happened
- "I don't really hold America's culture in high regard because the US doesn't
  have as long a history as e.g. China." Interesting interaction between
  cultures: I literally don't care about that. Further, with an attitude like
  that, a culture becomes entrenched in its traditions, which always act as a
  force against development (see: large telescope arrays on mountaintops, the
  money held by the federal government for the American Indians)

