# Random math notes

Here's a collection of physics and math notes I've compiled over the years as a
small resource for myself. I make no claims as to the significance of the
contents of these notes, as most of the results are relatively trivial to
derive.

