# Building Hamiltonians for simulation

We're interested in an systematic way to generate Hamiltonians to use in our
simulations where we may want to have multiple concurrent pulses of different
frequencies or polarizations. This particular example will be for our targeted
Rydberg scheme, but of the end result will be generalizable. For reference, here
is the level structure, with energies and drive strengths defined:

<img width=100% src=assets/building-hamiltonians_levels.svg>

Start with a naive approach to the Hamiltonian for a $\sigma^+$ drive of
frequency $\omega$:
$$
\begin{aligned}
    \frac{\hat{H}}{\hbar}
        &= \frac{\delta}{2} \ketbra{g_0}{g_0}
        - \frac{\delta}{2} \ketbra{g_1}{g_1}
        \\
        &+ \left( \omega_0 + \frac{\Delta}{2} \right) \ketbra{r_0^+}{r_0^+}
        + \left( \omega_0 + \frac{3 \Delta}{2} \right) \ketbra{r_1^+}{r_1^+}
        \\
        &+ \left( \omega_0 - \frac{\Delta}{2} \right) \ketbra{r_0^-}{r_0^-}
        + \left( \omega_0 - \frac{3 \Delta}{2} \right) \ketbra{r_1^-}{r_1^-}
        \\
        &+ \Omega \cos(\omega t)
            \left( \ketbra{g_1}{r_1^+} + \ketbra{r_1^+}{g_1} \right)
        \\
        &+ \Omega \cos(\omega t)
            \left( \ketbra{g_0}{r_0^+} + \ketbra{r_0^+}{g_0} \right)
        \\
        &+ \frac{\Omega}{k} \cos(\omega t)
            \left( \ketbra{g_1}{r_0^+} + \ketbra{r_0^+}{g_1} \right)
        \\
        &+ \frac{\Omega}{k} \cos(\omega t)
            \left( \ketbra{g_0}{r_0^-} + \ketbra{r_0^-}{g_0} \right)
\end{aligned}
$$
where $k$ parameterizes polarization defects. Then write the state in the
interaction picture,
$$
\begin{aligned}
    \ket{\psi}
        &= a_{g_0} e^{-\I \frac{\delta}{2} t} \ket{g_0}
        + a_{g_1} e^{+\I \frac{\delta}{2} t} \ket{g_1}
        \\
        &+ a_{r_1^-}
            e^{-\I \left( \omega_0 - \frac{3 \Delta}{2} \right) t}
            \ket{r_1^-}
        + a_{r_0^-}
            e^{-\I \left( \omega_0 - \frac{\Delta}{2} \right) t}
            \ket{r_0^-}
        \\
        &+ a_{r_0^+}
            e^{-\I \left( \omega_0 + \frac{\Delta}{2} \right) t}
            \ket{r_0^+}
        + a_{r_1^+}
            e^{-\I \left( \omega_0 + \frac{3 \Delta}{2} \right) t}
            \ket{r_1^+}
\end{aligned}
$$
Substituting into the Schrödinger equation (left as an exercise to the reader)
has the effect of separating the state into its "free-evolving" part, which is
already a solution to the equation, and the part that we are interested in.
After substituting and massaging things a bit, we are left with:
$$
\begin{dcases}
    \dot{a}_{g_0}
        = -\I \Omega \cos(\omega t)
            e^{
                \I \left(
                    -\omega_0 + \frac{\delta}{2} - \frac{\Delta}{2}
                \right) t
            }
            a_{r_0^+}
        - \I \frac{\Omega}{k} \cos(\omega t)
            e^{
                \I \left(
                    -\omega_0 + \frac{\delta}{2} + \frac{\Delta}{2}
                \right) t
            }
            a_{r_0^-}
    \\
    \dot{a}_{g_1}
        = -\I \Omega \cos(\omega t)
            e^{
                \I \left(
                    -\omega_0 - \frac{\delta}{2} - \frac{3 \Delta}{2}
                \right) t
            }
            a_{r_1^+}
        - \I \frac{\Omega}{k} \cos(\omega t)
            e^{
                \I \left(
                    -\omega_0 - \frac{\delta}{2} - \frac{\Delta}{2}
                \right) t
            }
            a_{r_0^+}
    \\
    \dot{a}_{r_1^-}
        = 0
    \\
    \dot{a}_{r_0^-}
        = -\I \frac{\Omega}{k} \cos(\omega t)
            e^{
                \I \left(
                    \omega_0 - \frac{\delta}{2} - \frac{\Delta}{2}
                \right) t
            }
            a_{g_0}
    \\
    \dot{a}_{r_0^+}
        = -\I \Omega \cos(\omega t)
            e^{
                \I \left(
                    \omega_0 - \frac{\delta}{2} + \frac{\Delta}{2}
                \right) t
            }
            a_{g_0}
        - \I \frac{\Omega}{k} \cos(\omega t)
            e^{
                \I \left(
                    \omega_0 + \frac{\delta}{2} + \frac{\Delta}{2}
                \right) t
            }
            a_{g_1}
    \\
    \dot{a}_{r_1^+}
        = -\I \Omega \cos(\omega t)
            e^{
                \I \left(
                    \omega_0 + \frac{\delta}{2} + \frac{3 \Delta}{2}
                \right) t
            }
            a_{g_1}
\end{dcases}
$$
After the rotating wave approximation is applied, we can then write an effective
Hamiltonian for the system that looks like this:
$$
\begin{aligned}
    \frac{\hat{H}'}{\hbar}
        &= \frac{\Omega}{2}
            e^{\I \big( \alpha - \tld{\omega}_{g_0}^{r_0^+} \big) t}
            \ketbra{g_0}{r_0^+}
        + \frac{\Omega / k}{2}
            e^{\I \big( \alpha - \tld{\omega}_{g_0}^{r_0^-} \big) t}
            \ketbra{g_0}{r_0^-}
        \\
        &+ \frac{\Omega}{2}
            e^{\I \big( \alpha - \tld{\omega}_{g_1}^{r_1^+} \big) t}
            \ketbra{g_1}{r_1^+}
        + \frac{\Omega / k}{2}
            e^{\I \big( \alpha - \tld{\omega}_{g_1}^{r_0^+} \big) t}
            \ketbra{g_1}{r_0^+}
        \\
        &+ \t{H.c.}
\end{aligned}
$$
where $\alpha = \omega - \omega_0$ and $\tld{\omega}_X^Y = \omega_Y - \omega_X -
\omega_0$ is the difference in the frequencies of the two states $Y$ and $X$
with $\omega_0$ subtracted out.

This formulation is useful for two main reasons. First, all the complex
exponential factors are entirely independent of $\omega_0$ (only under the RWA),
so it is much more easily mappable to arbitrary systems. Second, the Hamiltonian
is composed entirely from off-diagonal components, which makes the construction
of a "total" Hamiltonian describing multiple concurrent or overlapping pulses of
various frequencies and polarizations much easier to do.

The trade-off you incur by using this formulation, however, is that the
Hamiltonian necessarily becomes time-dependent and hence requires numerical
simulation to solve. (This is not such a problem, however, since the cases where
this formulation is most useful would require simulation anyway.)

As a fun corollary, this seems to indicate that the energy terms of a
Hamiltonian containing a drive can effectively be moved off the main diagonal to
a corresponding coupling term as a complex exponential with an appropriate sign.

The correct $n$-atom Hamiltonian (imaging this to be a system of $n$ Rydberg
atoms), then, can be computed with
$$
\begin{aligned}
    \hat{H}^{[n]}
        &= \sum\limits_{k = 0}^{n - 1}
            \hat{I}^{\otimes^{k - 1}}
            \otimes \hat{H}^{(1)}_k
            \otimes \hat{I}^{\otimes^{n - k}}
        \\
        &+ \sum\limits_X
            (\#_r X - 1) U \ketbra{X}{X}
\end{aligned}
$$
for all-to-all Rydberg interaction strength $U$, and $\#_r X$ is the number of
single-atom Rydberg states contained in the $n$-atom state $X$.

