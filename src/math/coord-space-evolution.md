# Numerical evolution of coordinate-space wavefunctions

We'd like to think about strategies for numerically evolving a given
coordinate-space wavefunction in time for some Hamiltonian -- say, for a
time-dependent potential. In other words -- given some (discretized) initial
wavefunction in, for simplicity, $1 + 1$ dimensions,
$$
\ket{\psi(t = 0)}
    = \sum_i \psi_i^0 \ket{x_i}
    \approx \int \ddv{x} \psi(x) \ket{x}
$$
(where the bottom index is for space and the top is for time) in a potential
$V(x, t)$, we want to numerically integrate the time-dependent Schrödinger
equation
$$
\frac{\hbar^2}{2 m} \hat{k}^2 \ket{\psi} + V(\hat{x}, t) \ket{\psi}
    = \I \hbar \pd_t \ket{\psi}
.$$

The naive approach would be to substitute for $\hat{k}$ its form in coordinate
space, $\hat{k} \to -\I \hbar \pd_x$, and then approximate both $\pd_x$ and
$\pd_t$ with finite differences:
$$
-\frac{\hbar^2}{2 m} \left(
    \frac{\psi_{i + 1}^j - 2 \psi_i^j + \psi_{i - 1}^j}{\Delta x^2}
\right)
    + V(x_i, t_j) \psi_i^j
    = \I \hbar \left( \frac{\psi_i^{j + 1} - \psi_i^{j - 1}}{2 \Delta x} \right)
.$$
This will work for small-enough $\Delta x$ and $\Delta t$ and be convenient to
program (it's easy to re-cast everything as a few matrix multiplication or
broadcasted array operations), but it's extremely limited by the low-order error
terms in the Taylor series expansions at play here -- these finite difference
approximations are each only good to second order, so the local truncation error
is $O(\Delta x^2 \, \Delta t^2)$. Even if you substituted a better integration
method in the time direction like the standard fourth-order Runge-Kutta, you'd
still be limited by the poor performance of the spatial derivative
approximation.

Instead, we'll turn to another method based around computing part of the
Hamiltonian in Fourier space and part in coordinate space. You can probably
guess which part is which.

First, let's start from a purely analytical picture. Suppose we have
$$
\hat{H} \ket{\psi}
    = \hat{H}_K \ket{\psi} + \hat{H}_X \ket{\psi}
    = \I \hbar \pd_t \ket{\psi}
,$$
with
$$
\begin{aligned}
    \hat{H}_K
        &= \frac{\hbar^2 \hat{k}^2}{2 m}
    &
    \hat{H}_X
        &= V(\hat{x}, t)
    &
    \ket{\psi}
        &= \int \ddv{x} \psi(x) \ket{x}
.\end{aligned}
$$
The action of $\hat{H}_X$ on $\ket{\psi}$ is trivial if $\ket{\psi}$ is already
represented in coordinate space (and also not explicitly defined here), so let's
consider the action of $\hat{H}_K$ instead:
$$
\begin{aligned}
    \hat{H}_K \ket{\psi}
        &= -\frac{\hbar^2}{2 m} \int \ddv{x} \pd_x^2 \psi(x) \ket{x}
        \\
        &= \frac{\hbar^2}{2 m} \int \ddv{k} \hat{k}^2 \phi(k) \ket{k}
        ,\quad \phi(k)
            = \frac{1}{\sqrt{2 \pi}} \int \ddv{x} \psi(x) e^{-\I k x}
        \\
        &\equiv \int \ddv{k} \tld{\phi}(k) \ket{k}
        ,\quad \tld{\phi}(k)
            = \frac{\hbar^2 k^2}{2 m} \phi(k)
        \\
        &\equiv \int \ddv{x} \tld{\psi}(x) \ket{x}
        ,\quad \tld{\psi}(x)
            = \frac{1}{\sqrt{2 \pi}} \int \ddv{k} \tld{\phi}(k) e^{\I k x}
.\end{aligned}
$$
Putting it all together, one can then run RK4 on something like
$$
\pd_t \psi(x, t)
    = -\frac{\I}{\hbar} \left(
        V(x, t) \psi(x, t)
        + \texttt{iFFT}\left[
            \frac{\hbar^2 k^2}{2 m} \texttt{FFT}\big[\psi(x, t)\big]
        \right]
    \right)
$$
where here $\texttt{FFT}$ and $\texttt{iFFT}$ refer to appropriately normalized
numerical routines.

Since the numerical error of an individual $\texttt{FFT}$ routine is $O(\log
N)$, where $N$ is the number of points in the array of values it's operating on,
the total error of this method should go something like $O(\Delta t^4 (\log
N_X)^2)$ for $N_X$ real-space coordinates in the discretization. Unfortunately,
the trade-off in this scheme is that one has to compute eight Fourier transforms
at every step (two for each term in the RK4 method), which means the runtime is
$O((N_X \log N_X)^2 / \Delta t)$ -- up from $O(N_X^2 / \Delta t)$ with finite
differences.

