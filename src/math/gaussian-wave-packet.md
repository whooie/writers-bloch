# Gaussian wave packets

Consider a localized matter wave packet in the form of a Gaussian:
$$
    \psi(x)
        = \left( \frac{1}{2 \pi \sigma^2} \right)^{1/4}
            e^{-\frac{x^2}{4 \sigma^2}}
$$
We wish to verify that the Fourier transform $\phi(k)$ of this real-space
wavefunction is also in the form of a Gaussian. Here we use the (quantum)
physicist's normalization convention.
$$
\begin{aligned}
    \phi(k)
        &= \frac{1}{\sqrt{2 \pi}} \int_{-\infty}^{\infty} \ddv{x}
            \left( \frac{1}{2 \pi \sigma^2} \right)^{1/4}
            e^{-\frac{x^2}{4 \sigma^2}}
            e^{-\I k x}
        \\
        &= A \int_{-\infty}^{\infty} \ddv{x}
            e^{-\frac{x^2}{4 \sigma^2} - \I k x}
        \quad\gets\quad
        -\frac{x^2}{4 \sigma^2} - \I k x
            = -\left(
                \frac{x}{2 \sigma}
                + \I \sigma k
            \right)^2
            - \sigma^2 k^2
        \\
        &= A e^{-\sigma^2 k^2} \int_{-\infty}^{\infty} \ddv{x}
            e^{
                -\left(
                    \frac{x}{2 \sigma} + \I \sigma k
                \right)^2
            }
        \quad\gets\quad
        u
            = \frac{x}{2 \sigma} + \I \sigma k
        \\
        &= 2 \sigma A e^{-\sigma^2 k^2}
            \int_{-\infty}^{\infty} \ddv{u} e^{-u^2}
        \\
        &= 2 \sqrt{\pi} \sigma A e^{-\sigma^2 k^2}
        \\
        &= \left( \frac{2 \sigma^2}{\pi} \right)^{1/4}
            e^{-\sigma^2 k^2}
\end{aligned}
$$
This can easily be seen to satisfy the proper wavefunction normalization
condition.

The inverse Fourier transform can be performed in a nearly identical manner,
$$
\begin{aligned}
    \psi(x)
        &= \frac{1}{\sqrt{2 \pi}} \int_{-\infty}^{\infty} \ddv{k}
            \left( \frac{2 \sigma^2}{\pi} \right)^{1/4}
            e^{-\sigma^2 k^2}
            e^{\I k x}
        \\
        &= B \int_{-\infty}^{\infty} \ddv{k}
            e^{-\sigma^2 k^2 + \I k x}
        \quad\gets\quad
        -\sigma^2 k^2 + \I k x
            = -\left(
                \sigma k
                -\I \frac{x}{2 \sigma}
            \right)^2
            - \frac{x^2}{4 \sigma^2}
        \\
        &= B e^{-\frac{x^2}{4 \sigma^2}} \int_{-\infty}^{\infty} \ddv{k}
            e^{
                -\left(
                    \sigma k - \I \frac{x}{4 \sigma}
                \right)^2
            }
        \quad\gets\quad
        v
            = \sigma k + \I \frac{x}{2 \sigma}
        \\
        &= \frac{1}{\sigma} B e^{-\frac{x^2}{4 \sigma^2}}
            \int_{-\infty}^{\infty} \ddv{v} e^{-v^2}
        \\
        &= \frac{\sqrt{\pi}}{\sigma} B e^{-\frac{x^2}{4 \sigma^2}}
        \\
        &= \left( \frac{1}{2 \pi \sigma^2} \right)^{1/4}
            e^{-\frac{x^2}{4 \sigma^2}}
\end{aligned}
$$
which verifies that $\phi(k)$ found above is indeed the correct Fourier
transform of $\psi(x)$.

It can also be verified (by inspection, of course) that
$$
\begin{aligned}
    \left\langle x^2 \right\rangle
        &= \sigma^2
    &
    \implies \Delta x
        &= \sigma
    \\
    \left\langle k^2 \right\rangle
        &= \frac{1}{4 \sigma^2}
    &
    \implies \Delta p
        &= \frac{\hbar}{2 \sigma}
\end{aligned}
$$
and so the Gaussian wavepacket is a minimum-uncertainty wavefunction,
$$
    \Delta x \Delta p = \frac{\hbar}{2}
$$

---

Now we'll consider the time evolution of this wavepacket in free space. Here,
our Hamiltonian is simply the kinetic energy of the packet,
$$
\hat{H}
    = \frac{\hbar^2 \hat{k}^2}{2 m}
$$
Considering this, we'll start in momentum space:
$$
\ket{\psi_0}
    = \int_\infty \ddv{k}
        \left( \frac{2 \sigma^2}{\pi} \right)^{1 / 4}
        e^{-\sigma^2 (k - k_0)^2} \ket{k}
$$
The time evolution of this state is found in the usual way,
$$
\ket{\psi(t)}
    = \int_\infty \ddv{k}
        \left( \frac{2 \sigma^2}{\pi} \right)^{1 / 4}
        e^{
            -\sigma^2 (k - k_0)^2 - \I \frac{\hbar k^2}{2 m} t
        } \ket{k}
$$
and we wish to calculate its projection onto the real-space basis,
$\set{\ket{x}}$. Recalling that
$$
\braket{x | k}
    = \frac{1}{\sqrt{2 \pi}} e^{\I k x}
$$
the integral we need to compute, then, is
$$
\braket{x | \psi(t)}
    = \frac{1}{\sqrt{2 \pi}} \left( \frac{2 \sigma^2}{\pi} \right)
        \int_\infty \ddv{k}
        e^{
            -\sigma^2 (k - k_0)^2
            - \I \frac{\hbar k^2}{2 m} t
            + \I k x
        }
$$

Now we'll marshal the argument of the exponential into the form $-a k^2 + b k +
c$ with
$$
\begin{aligned}
    a
        &= \sigma^2 + \I \frac{\hbar}{2 m} t
    \\
    b
        &= 2 \sigma^2 k_0 + \I x
    \\
    c
        &= -\sigma^2 k_0^2
\end{aligned}
$$
For general $a, b, c$, this integral can be evaluated rather easily:
$$
\begin{aligned}
    \int_\infty \ddv{z} e^{-a z^2 + b z + c}
        &= \int_\infty \ddv{z}
            e^{-a z^2 + b x - \frac{b^2}{4 a} + \frac{b^2}{4 a} + c}
        \\
        &= \int_\infty \ddv{z}
            e^{
                -\left( \sqrt{a} x - \frac{b}{2 \sqrt{a}} \right)^2
                + \frac{b^2}{4 a} + c
            }
        \quad\gets\quad
        u
            = \sqrt{a} x - \frac{b}{2 \sqrt{a}}
        \\
        &= \frac{1}{\sqrt{a}} e^{\frac{b^2}{4 a} + c}
            \int_\infty \ddv{u} e^{-u^2}
        \\
        &= \sqrt{\frac{\pi}{a}} e^{\frac{b^2}{4 a} + c}
\end{aligned}
$$
So then the original integral evaluates to
$$
\braket{x | \psi(t)}
    = \frac{1}{\sqrt{2 \pi}} \left( \frac{2 \sigma^2}{\pi} \right)^{1 / 4}
        \sqrt{\frac{\pi}{\sigma^2 + \I \frac{\hbar}{2 m} t}}
        \exp\left(
            \frac{
                \left( 2 \sigma^2 k_0 + \I x \right)^2
            }{
                4 \left( \sigma^2 + \I \frac{\hbar}{2 m} t \right)
            }
            - \sigma^2 k_0^2
        \right)
$$
And then by the Sympy theorem, we can calculate the corresponding probability
density as
$$
|\braket{x | \psi(t)}|^2
    = \sqrt{
        \frac{
            1
        }{
            2 \pi \left( \sigma^2 + \frac{\hbar^2}{4 m^2 \sigma^2} t^2 \right)
        }
    }
    \exp\left(
        -\frac{
            \left( x - \frac{\hbar k_0}{m} t \right)^2
        }{
            2 \left( \sigma^2 + \frac{\hbar^2}{4 m^2 \sigma^2} t^2 \right)
        }
    \right)
$$
where we can note that the center of this wavepacket travels at the group
velocity $v_g = \hbar k_0 / m$, and at $t = 0$ we recover our initial real-space
wavefunction.

