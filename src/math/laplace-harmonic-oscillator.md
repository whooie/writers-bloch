# Solving the harmonic oscillator problem the cooler way

Consider the textbook harmonic oscillator problem,
$$
\left(
    \dnv{2}{t} + \gamma \dv{t} + \omega_0^2
\right) z
        = F(t)
        ,~ t \geq 0
$$
Here, we want to find the solution without making any Ansatz.

---

Look first at the undriven case, $F(t) = 0$ and start by taking the Laplace
transform of both sides. Under the transform, we have
$$
\dv{t} \longrightarrow s
$$
where $s$ is the complex frequency-space variable. Then the problem becomes
$$
\left(
    s^2 + \gamma s + \omega_0^2
\right) \tld{z}
    \equiv f(s) \tld{z}
    = 0
$$

To search for non-trivial solutions, we want to find the roots of $f(s)$:
$$
\begin{aligned}
    s_\pm
        &= -\frac{\gamma}{2}
        \pm \left[ \frac{\gamma^2}{4} - \omega_0^2 \right]^{1 / 2}
        \\
        &= -\frac{\gamma}{2}
        \pm \I \left[ \omega_0^2 - \frac{\gamma^2}{4} \right]^{1 / 2}
            \equiv -\frac{\gamma}{2} \pm \I \omega
\end{aligned}
$$
Then since $f(s) \neq 0$ for all $s \neq s_\pm$, we have
$$
\begin{aligned}
    \tld{z}(s)
        &\equiv \begin{dcases}
            A_\pm
                & s = s_\pm
            \\
            0
                & \t{else}
        \end{dcases}
        \\
        &= A_+ \delta(s - s_+) + A_- \delta(s - s_-)
\end{aligned}
$$
To transform back to the time domain, we apply the inverse Laplace transform:
$$
z(t)
    = \frac{1}{2 \pi \I} \lim\limits_{T \rightarrow \infty}
        \int_{\Gamma - \I T}^{\Gamma + \I T} \ddv{s} \tld{z}(s) e^{s t}
$$
Considering $\delta$ functions to be singularities with
$$
\t{Res}(\tld{z}, s_\pm)
    = A_\pm
$$
(which I don't know for sure is valid), we choose $\Gamma = -\gamma / 4 >
-\gamma / 2$ to satisfy the requirement of the inverse transform that $\Gamma$
be greater than the real part of all $\tld{z}$'s singularities while
ensuring that the integrand goes to zero for large $s$. This second condition is
so that we can apply residue theorem in the typical way, where the path is a
semi-circle over the left half-plane with radius tending to infinity. Thus the
transformation back to the time domain gives
$$
z(t)
    = e^{-\gamma t / 2} \left(
        A_+ e^{\I \omega t} + A_- e^{-\I \omega t}
    \right)
$$

Then we can apply initial conditions
$$
\begin{aligned}
    z(t = 0)
        &= z_0
        \in \setC
    &
    \dot{z}(t = 0)
        &= 0
\end{aligned}
$$
which yield
$$
\begin{aligned}
    A_+
        &= \frac{\I \omega + \gamma / 2}{2 \I \omega} z_0
    &
    A_-
        &= \left(
            1 - \frac{\I \omega + \gamma / 2}{2 \I \omega}
        \right) z_0
\end{aligned}
$$
and that's that.

(Fun fact: normally we'd also take the "actual" solution to be $x(t) = \RE
z(t)$, but if $z_0$ is real, then $z(t)$ is also all real!)

---

Now look at the driven case. It will become apparent later on that the solution
found by this method cannot be fit to initial conditions in the same way as the
previous one (so it must only apply to the steady state), but that's okay
because you'll always still find the particular solution by combining this
solution with the homogeneous one.

Here, since there's no decaying exponential component to the expected solution,
we can use the regular Fourier transform, under which
$$
\dv{t} \longrightarrow \I \omega
$$
Then we have
$$
\left(
    -\omega^2 + \I \gamma \omega + \omega_0^2
\right) \tld{z}
    = \tld{F}(\omega)
$$
which easily gives
$$
\tld{z}
    = \frac{\tld{F}}{\omega_0^2 - \omega^2 + \I \gamma \omega}
$$

Now looking at this Fourier component, we can derive the usual amplitude and
phase of the response to the drive:
$$
\begin{aligned}
    |\tld{z}|
        &= \frac{
            |\tld{F}|
        }{
            [(\omega_0^2 - \omega^2)^2 + (\gamma \omega)^2]^{1 / 2}
        }
    \\
    \arg \tld{z}
        &= \arg \tld{F}
        - \arctan\left( \frac{\gamma \omega}{\omega_0^2 - \omega^2} \right)
\end{aligned}
$$

