# Working with product-space states

We'd like to build up a computational framework for working with states in a
general Hilbert space comprising all product states assembled from a number of
subsystems that may be independent or entangled. Let's start with some
definitions.

Suppose we have $N$ subsystems
$$
Q_k,~ k \in \{ 0,\, \dots,\, N - 1 \}
$$
where each is spanned by basis vectors labeled by a quantum number $q_k \in
\#Q_k$ (where $\#Q_k$ denotes the set of all possible values of $q_k$).

Assembling the complete Hilbert space $\mathcal{H}$ is, of course, then
$$
\mathcal{H}
    = \left\{
        \bigotimes_{k = 0}^{N - 1}
            \ket{q_k}
        ~\middle|~
        q_k \in \#Q_k
    \right\}
$$
and, given some $\ket{\psi} \in \mathcal{H}$, we are interested in recovering
the states of the individual subsystems. Here, it's tempting to think that the
subsystem state can be computed simply working with the state vector; e.g.
something like
$$
\ket{\psi_k}
    = \left[
        \sum_{q_k} \ket{q_k}
            \left(
                \sum_{q_j,\, j \neq k}
                    \bra{q_0 \cdots q_k \cdots q_{N - 1}}
            \right)
    \right] \ket{\psi}
,$$
but the possibility that $\ket{\psi}$ is an entangled state prevents this --
instead, we have to consider states as density matrices, $\rho =
\ketbra{\psi}{\psi}$, where $\rho$ can be described by a complex-valued $D
\times D$ matrix, where
$$
D
    = \prod_{k = 0}^{N - 1} |\#Q_k|
.$$

To see why, one need only consider the maximally entangled Bell state of two
qubits, $\ket{\Phi^+} = (\ket{00} + \ket{11}) / \sqrt{2}$, for which
$$
\rho_{\Phi^+}
    = \ketbra{\Phi^+}{\Phi^+}
    = \frac{1}{2} \begin{pmatrix}
        1 & 0 & 0 & 1 \\
        0 & 0 & 0 & 0 \\
        0 & 0 & 0 & 0 \\
        1 & 0 & 0 & 1
    \end{pmatrix}
.$$
When either of the qubits is traced out, the remaining one is described by the
density matrix
$$
\rho_{\Phi^+}'
    = \frac{1}{2} \begin{pmatrix}
        1 & 0 \\
        0 & 1
    \end{pmatrix}
.$$
This describes a mixed state, which necessarily cannot be described with a state
vector.

Getting back to our general product states, the partial trace operation is easy
to describe with kets and bras -- to trace out the $k$-th subsystem, one
computes
$$
\text{Tr}^{(k)} \rho
    = \sum_{q_k \in \#Q_k} \bra{q_k} \rho \ket{q_k}
.$$
But when doing this with a computer program, it is often much more convenient to
represent the subsystem vectors $\ket{q_k}$ more simply as $|\#Q_k|$-element
arrays (rather than arrays whose length is equal to the dimension of the total
vector space $\mathcal{H}$). As such, these arrays' dimensions won't agree with
those of $\rho$.

To remedy this, we have to find a way to build non-square matrices from the kets
and bras of the subsystems we're tracing over, since we need this operation to
take our original $D \times D$ matrix down to a $(D - |\#Q_k|) \times (D -
|\#Q_k|)$ matrix. It turns out that all we need to do is insert some identities.

In the above expression for $\text{Tr}^{(k)} \rho$, we can replace the single
kets and bras with a kind of "operator" that projects from the total
product space down to one comprising all but the subsystem being traced over. In
principle, the single kets and bras already do that, but to make it explicit, we
can insert resolutions of the identity operator in each of the surviving
subsystems and define the "operator" (perhaps it's more appropriate to simply
call it a matrix) as
$$
T_k^{(q_k)}
    = \left(
        \bigotimes_{a = 0}^{k - 1} \mathbb{I}^{(a)}
    \right)
    \otimes
    \ket{q_k}
    \otimes
    \left(
        \bigotimes_{b = k + 1}^{N - 1} \mathbb{I}^{(b)}
    \right)
$$
where $\mathbb{I}^{(a)}$ is said resolution,
$$
\mathbb{I}^{(a)}
    = \sum_{q_a \in \#Q_a} \ketbra{q_a}{q_a}
.$$
Then the desired partial trace operation is accomplished as
$$
\text{Tr}^{(k)} \rho
    = \sum_{q_k \in \#Q_k} T_k^{(q_k) \dagger} \rho \, T_k^{(q_k)}
.$$

From an analytical point of view, nothing was gained by inserting these
identities, but when it comes time to model everything in a computer program
(perhaps naively) with arrays, this provides the framework by which a small,
one-dimensional array representing a single state vector in the traced subsystem
can be built up into a form that is actually useful for computing the partial
trace of a given density matrix.

For instance, consider the following functions implementing this in Python with
SymPy, for the special case of an `n`-qubit state:
```python
import sympy as sy

def trace_qubit(n: int, k: int, rho: sy.Matrix) -> sy.Matrix:
    """
    Compute the partial trace over the k-th qubit (indexed from zero) out of n
    total qubits for a given density matrix rho.
    """
    T0 = sy.kronecker_product(
        sy.eye(2 ** k), sy.Matrix([[1, 0]]).T, sy.eye(2 ** (n - k - 1))
    )
    T1 = sy.kronecker_product(
        sy.eye(2 ** k), sy.Matrix([[0, 1]]).T, sy.eye(2 ** (n - k - 1))
    )
    return (T0.T * rho * T0) + (T1.T * rho * T1)

def isolate_qubit(n: int, k: int, rho: sy.Matrix) -> sy.Matrix:
    """
    Compute the density matrix for only the k-th qubit (indexed from zero) out
    of n total qubits by progressively tracing out all others.
    """
    if n == 1:
        return rho
    elif k == 0:
        return isolate_qubit(n - 1, k, trace_qubit(n, n - 1, rho))
    else:
        return isolate_qubit(n - 1, k - 1, trace_qubit(n, 0, rho))

from itertools import product

def isolate_qubit_2(n: int, k: int, rho: sy.Matrix) -> sy.Matrix:
    """
    Non-recursive implementation of the above.
    """
    zero = sy.Matrix([[1, 0]]).T
    one = sy.Matrix([[0, 1]]).T
    Q = product(*((n - 1) * [[zero, one]]))
    tracers = (
        sy.kronecker_product(*[
            sy.eye(2) if a == k else q[a if a < k else a - 1]
            for a in range(n)
        ])
        for q in Q
    )
    terms = (T.T * rho * T for T in tracers)
    return sum(terms, start=sy.zeros(2))
```

