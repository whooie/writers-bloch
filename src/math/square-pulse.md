# Square pulse Fourier transform

Consider a square pulse,
$$
    f(t)
        = \begin{dcases}
            A
                & \t{if}~ |t| < a
            \\
            0
                & \t{else}
        \end{dcases}
$$
The Fourier transform of this is easy to compute,
$$
\begin{aligned}
    \hat{f}(\omega)
        &= \int_\OO \ddv{t} f(t) e^{-\I \omega t}
        \\
        &= A \int_{-a}^a \ddv{t} e^{-\I \omega t}
        \\
        &= -\frac{A}{\I \omega} \left(e^{-\I \omega a} - e^{\I \omega a}\right)
        \\
        &= 2 A \frac{\sin(\omega a)}{\omega}
\end{aligned}
$$

The inverse transform of this, however, is less so. The integral we wish to
compute is
$$
\begin{aligned}
    f(t)
        &= \frac{1}{2 \pi} \int_\OO \ddv{\omega}
            \hat{f}(\omega) e^{\I \omega t}
        \\
        &= \frac{A}{\pi} \int_{-\OO}^\OO \ddv{\omega}
            \frac{\sin(\omega a)}{\omega}
\end{aligned}
$$
Start by rewriting in a more convenient form,
$$
\begin{aligned}
    f(t)
        &= \frac{A}{2 \pi \I} \int_{-\OO}^\OO \ddv{\omega}
            \frac{e^{\I \omega a} - e^{-\I \omega a}}{\omega} e^{\I \omega t}
        \\
        &= \frac{A}{2 \pi \I} \int_{-\OO}^\OO \ddv{\omega}
            \frac{e^{\I \omega (t + a)}}{\omega}
        - \frac{A}{2 \pi \I} \int_{-\OO}^\OO \ddv{\omega}
            \frac{e^{\I \omega (t - a)}}{\omega}
\end{aligned}
$$
and considering the two integrals separately:
$$
\begin{aligned}
    I_1
        &= \int_{-\OO}^\OO \ddv{\omega} g_1(\omega)
        ,\quad g_1(\omega)
            = \frac{e^{\I \omega (t + a)}}{\omega}
    \\
    I_2
        &= \int_{-\OO}^\OO \ddv{\omega} g_2(\omega)
        ,\quad g_2(\omega)
            = \frac{e^{\I \omega (t - a)}}{\omega}
\end{aligned}
$$

Looking at $g_1$ and $g_2$, it is clear that they both have simple poles at
$\omega = 0$, which makes them difficult to integrate through straightforward
means. But by allowing $\omega \in \setC$, $I_1$ and $I_2$ may be computed by
complex contour integration in the usual way. The exponential factors will cause
both integrands to decay sufficiently fast to zero for $\IM \omega > 0$ or $\IM
\omega < 0$ (depending on the signs of $t + a$ and $t - a$) such that additional
integrals of $g_1$ and $g_2$ over semicircles $S_1$ and $S_2$ of radius $R$
extending over either the upper or lower half of the complex plane, will vanish
as $R \to \OO$. Thus they can be added appropriately to $I_1$ and $I_2$ to form
a closed integration path and hence allow residue theorem to be
applied{{footnote:
    Strictly, we must also replace the integration path close to $\omega = 0$
    with a small semicircle of radius $\eeps$ and orientation opposite to that
    of the larger one (such that the closed path contains the pole at $\omega =
    0$) and take $\eeps \to 0$ to properly apply the residue theorem.
}}.
First note that both $g_1(\omega)$ and $g_2(\omega)$ can be written in the form
$a(\omega)/b(\omega)$ where both $a(\omega)$ and $b(\omega)$ are holomorphic and
$\ddv{b}/\ddv{\omega} \neq 0$ at $\omega = 0$. Thus we have
$$
    \t{Res}\left( \frac{e^{\I \omega (t + a)}}{\omega}, 0 \right)
        = \t{Res}\left( \frac{e^{\I \omega (t - a)}}{\omega}, 0 \right)
        = 1
$$

Now consider regions in $t$ between which $t + a$ and $t - a$ switch signs. If
$t < -a$, both $t + a$ and $t - a$ are negative, so choose $S_1$ and $S_2$ to
both lie in the lower half-plane. Noting the left-handed orientation of the
resulting integration loop, residue theorem gives $I_1 = I_2 = -\pi \I$, which
in turn gives
$$
    f(t < -a)
        = \frac{A}{2 \pi \I} (-\pi \I + \pi \I) = 0
$$

If $t > a$, both $t + a$ and $t - a$ are positive, so choose $S_1$ and $S_2$ to
both lie in the upper half-plane. Similar to the previous case (the single
exception being the right-handed orientation of these loops), $I_1 = I_2 = +\pi
\I$, so
$$
    f(t > a)
        = \frac{A}{2 \pi \I} (\pi \I - \pi \I) = 0
$$

If $-a < t < a$, then $t + a$ is positive while $t - a$ is negative. Choosing
$S_1$ in the upper half-plane and $S_2$ in the lower half-plane then results in
$I_1 = +\pi \I$ and $I_2 = -\pi \I$ (with the relative sign being due to the
handedness of the two loops), yielding
$$
    f(-a < t < a)
        = \frac{A}{2 \pi \I} (\pi \I + \pi \I)
        = A
$$

Thus we recover $f(t)$ in its original form,
$$
    f(t)
        = \begin{dcases}
            A
                & \t{if}~ |t| < a
            \\
            0
                & \t{else}
        \end{dcases}
$$

<!-- --- -->
<!--  -->
<!-- [^1]: Strictly, we must also replace the integration path close to $\omega = 0$  -->
<!-- with a small semicircle of radius $\eeps$ and orientation opposite to that of -->
<!-- the larger one (such that the close path contains the pole at $\omega = 0$) -->
<!-- and take $\eeps \to 0$ to properly apply residue theorem. -->

