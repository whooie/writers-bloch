# Mathematical machines

- programming is like playing in a mathematical sandbox
- making machines that run on pure math
- programming is the purest form of applied math
