# Mental illnesses and addictions as memes

- obviously, memes in the Darwinian sense
- illnesses and addictions probably don't even have to be "glorified" to spread
- someone comes up to you or posts on the internet "how do you personally deal
  with \<condition X\>" that you've never heard of before, whose symptoms don't
  easily map onto you

