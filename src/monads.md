# `Maybe` from first principles

<details>
<summary>Changelog</summary>

- Originally posted 2024.07.31
- 2024.08.02: Rephrasing in the last paragraph before the categorical monad
  section.
- 2024.08.04: Correction a couple dumb typos and add more descriptive signatures
  in the `Either` and `State` discussions.
</details>

Monads have become something often discussed by functional programmers and
lauded as elegant ways to model errors and side effects when such things are
formally nonexistent in a language. This is true, but the answer to the question
of what a monad actually *is* remains famously opaque to newcomers. One can
usually end up with a pretty good intuitive feeling for what a monad is just by
using any of their various implementations in programming languages, but it's
significantly more difficult to understand it from the true mathematical
definition.

Let's try it anyway. Here I'll first provide the necessary background to
understand monads in the mathematical sense, then map those concepts to Haskell
with a few illustrative examples. Finally, I'll attempt to give a more intuitive
explanation of monads in terms of what they can actually do for us.

> What follows assumes a passing familiarity with the basic elements of Haskell
> and functional programming patterns (i.e. ML-style syntax, first-class
> functions, currying, and recursion). Familiarity with mathematical phrasing
> and logical quantifiers (e.g. "for all $x$") is also assumed.

## Contents
<!-- toc -->

See also the [supplementary material][supplements].

## Some category theory
Despite its famous opaqueness, let's start off with the mathematical definition
which will, if nothing else, give us some milestones to aim for in concept
space.

> A monad is a monoid in the category of endofunctors of a fixed category.

The aforementioned opaqueness is apparent. But right off the bat, we have our
milestone questions:
1. What's a category?
2. What's an endofunctor?
3. What's a category of endofunctors?
4. What's a monoid?

In answering these, we'll undoubtedly accrue additional sub-questions, but these
are useful high-level guides to what must follow.

### Categories
Let's look at the first. *What is a category?* To actually answer this in a
satisfying way, it's necessary to start with some motivation. Specifically, we
want to think about the concept of an *abstraction*. What do I mean by this?
There are a few ways to look at it, but I think maybe the best here is with a
couple examples.

#### What's in an abstraction?
Suppose we have some pebbles. A natural thing that any child will do is to try
grouping them together in various ways. Given any collection of pebbles, it's
possible to separate them into piles, and likewise take some collection of piles
and combine them in various pairings. In this process, one seeks a way to
distinguish piles--what makes one pile different from another? Maybe there are a
few pebbles in one pile that are shinier than those in another, but for the
moment let's assume all the pebbles are just boring, small rocks. Absent any
sort of inherent specialness, one notices that the piles of pebbles can be
characterized by counting each pebble in the pile. When two piles are combined
into one, counting each of the pebbles in the final pile produces a result that
has a specific relationship to what one obtains by counting the pebbles in the
original piles.

And of course, as one notices, this phenomenon doesn't only happen with pebbles.
It could be seeds, candies, potatoes, whatever--as long as we're only counting,
these properties hold for any kind of discrete thing! And just like that, we've
discovered the natural numbers and addition. The natural numbers are an
*abstraction* over discrete physical quantities: It doesn't matter what the
objects being counted are; as long as they're discrete and countable, they
behave like the natural numbers with addition.

<div style="text-align: center; width: 60%; margin: auto auto;">
    <img
        src="assets/monads/addition.svg"
        alt="addition"
        style="width: 100%"
    ></img>
</div>

Besides being more convenient to work with, numbers provide an interface to the
pure concepts at play here. Using numbers, one can see the various additive
properties of physical objects and even extend them. Subtraction and
multiplication can be seen as direct logical extensions of addition, and
division comes about by asking what would happen if the objects could be reduced
in some way into smaller pieces. From there, it's natural to then define other
types of numbers like the negative integers, the rational numbers, the real
numbers, complex numbers, quaternions, ...

And further, abstractions can also be built on other abstractions.
Algebra{{footnote:
    To be clear, we're talking about everyday, "solve for $x$" algebra and not
    *abstract* algebra, which is the generalization of different sets of objects
    and operations on those objects.
}}
is an abstraction over numbers. Instead of dealing directly with the arithmetic
operations on numerical inputs and outputs, we can notice patterns in how these
operations affect their operands and use those patterns to write generalized
statements that all children learn as equations: $y = m x + b$ uses symbols to
denote spots that could be filled in by any number, and the usual rules for
manipulating these symbols to "solve for $x$" are derived by analyzing
observable patterns in how addition, subtraction, multiplication, and division
operate on the numbers those symbols stand for. Then one can define other
operations on numbers based on these generalized, symbolic operations like
integration and differentiation, and hopefully you (dear reader) can see that
there's a real power to be found in abstraction.

Group theory is another, more interesting example of an abstraction built on a
physical phenomenon. Without getting into it too much, group theory is based not
on quantities of objects, but instead transformations that leave some kind of
observable thing unchanged. Suppose we have a square lying in the plane. There
are several things we can do to the square, like stretch it, move it around,
rotate it, flip it around, etc. But there are certain transformations we can
apply that leave the square looking the same. These are the symmetries of the
square:

1. Rotate by +90°
1. Rotate by –90°
1. Rotate by 180°
1. Reflect across the $y$ axis
1. Reflect across the $x$ axis
1. Reflect across the line $y = +x$
1. Reflect across the line $y = -x$

which look like this:

<div style="text-align: center; width: 80%; margin: auto auto;">
    <img
        src="assets/monads/square-symmetries.svg"
        alt="square symmetries"
        style="width: 100%"
    ></img>
</div>

And the same sort of thing applies to other shapes, like hexagons and circles,
and in other dimensions, like cubes and pyramids. But it also applies to other
things for which "symmetry" takes on a more general definition, like how
stepping forward or backward in time by a multiple of 12 hours will leave a
clock's hands in the same positions, or how the molecular structure of a crystal
looks the same under certain translations in space.

Group theory abstracts over these transformations and treats them as base
objects: It doesn't matter what they are or what the thing is that they're being
applied to--given a collection of things that satisfy certain conditions (that
are also satisfied by symmetrical transformations), group theory reasons about
and extends the properties of the collection to find non-trivial deductions in
the same way that algebra and calculus reason about and extend the various
properties of numbers and arithmetic. This abstraction in particular has been
quite important in various fields of physics in the past several decades.

#### Defining a category
So now that I've thoroughly belabored the idea of an abstraction, we finally
come to categories. Broadly, categories are abstractions over the concept of
things having directed, composable relationships. What does this mean? Well,
let's briefly come back to numbers. Instead of arithmetic, though, we'll talk
about $\leq$. In mathematical terms, $\leq$ is a total ordering on the
integers{{footnote:
    It's a total ordering on the reals too (interestingly, not on IEEE
    floating-point numbers), but let's keep things discrete.
}},
meaning that under $\leq$, any two integers $a$ and $b$ can always be compared,
giving $a \leq b$, $b \leq a$, or if and only if $a = b$, $a \leq b$ *and* $b
\leq a$. This ordering can be seen as a directed relationship between $a$ and
$b$. $\leq$ is also transitive: If $a \leq b$ and $b \leq c$, then we must also
have $a \leq c$. Another way to say this is that the relationships imposed on
$a$, $b$, and $c$ are composable: The relationships $a \leq b$ and $b \leq c$
can be "put together" to get another relationship, $a \leq c$.

These are examples of the core ideas of a category. Here's the mathematical
definition.

> A category $\mathcal{C}$ is a collection of objects (things)
> $\text{Ob}(\mathcal{C})$, a collection of morphisms (relationships)
> $\text{Hom}(\mathcal{C})$, and a binary operation $(\,\circ\,)$ (composition)
> taking two morphisms as input and returning another morphism, such that the
> following hold for any object $X \in \text{Ob}(\mathcal{C})$ and morphisms
> $f, g, h \in \text{Hom}(\mathcal{C})$:
>
> 1. The composition of two morphisms in a category is another morphism in the
>    category,
>    $$f \circ g \in \text{Hom}(\mathcal{C})$$
> 1. Composition is associative,
>    $$(f \circ g) \circ h = f \circ (g \circ h)$$
> 1. Each object has an "identity" morphism, $\text{id}_X$, that does nothing
>    with respect to composition,
>    $$\text{id}_X \circ f = f = f \circ \text{id}_X$$

(It should also be said that morphisms are usually thought of as "arrows"
pointing from one object to another. As a matter of notation, the morphism $f$
pointing from $A$ to $B$ is written $f \colon A \to B$, and another morphism $g
\colon C \to D$ can be composed with $f$, i.e. $g \circ f$ is valid, if and only
if $B$ and $C$ are the same.)

The example I gave above with $\leq$ is an example of a category, where objects
are integers, morphisms are the orderings imposed by $\leq$, and $\circ$
represents the transitivity of $\leq$. Categories can also be drawn as directed
graphs, with objects as nodes and morphisms as arrows:

<div style="text-align: center; width: 80%; margin: auto auto;">
    <img
        src="assets/monads/leq-arrows.svg"
        alt="leq ordering"
        style="width: 100%"
    ></img>
</div>

But what does it all mean intuitively? Well, most of the point of category
theory is to provide a very general framework for reasoning about relationships
between things. The list of conditions that need to be satisfied in order for
something to be a category is relatively short and lax, and as a result the
language of categories can be applied in many contexts. This allows category
theorists to, for example, prove something in one domain, and very easily map it
to another. That sort of thing doesn't really apply here to this post, though,
since we mostly want to focus on category theory only as it pertains to
programming.

So the first real thing to note is that although $\leq$ on the integers is a
valid category, we more often think of objects as having some internal
structure. That structure is *probed* by the existence and behavior of the
various morphisms that connect the objects. A common example of a category is
**Set**, the category of sets and functions (some more well-studied categories
have names, usually written in bold). In **Set**, objects are sets, morphisms
are functions in the usual sense, and $\circ$ is ordinary function composition
where we take the output of one function and feed it as input to another.

<div style="text-align: center; width: 55%; margin: auto auto;">
    <img src="assets/monads/set.svg" alt="set" style="width: 100%"></img>
</div>

Here we have a (sub)category of **Set**, with three objects (sets) $A$, $B$, and
$C$, and morphisms $f \colon A \to B$, $g \colon B \to C$, and $g \circ f \colon
A \to C$, along with identity morphisms. From there, we can reason about these
sets based on the morphisms between them. For example, we can think about a
category of sets in which there exists one object $I$ corresponding to the empty
set, and another object $T$ corresponding to the singleton set, which has only
one element. These two sets are indeed characterized by the properties by which
we've defined them. We know by construction what these sets are, but we can also
see these characteristic properties based on what morphisms exist between them
and other sets in the category. Namely, for any other object (set) $X$, there
exists exactly one unique morphism $I \to X$, and exactly one unique morphism $X
\to T$. This identifies $I$ as an "initial" object and $T$ as a "terminal"
object in the category. By translating set-theoretic ideas to a more general,
category-based framework, the ideas themselves can be reasoned about more
generally and applied to other areas. It can be shown that any other sets (or
completely different kinds of objects) obeying either of these properties must
have the respective number of elements (or equivalent property); hence the
morphisms in the category characterize them.

This is a general pattern in category theory: We don't talk explicitly about any
structures within objects; instead, we rely on the morphisms to elucidate
information. This keeps the reasoning as general as possible so that if, for
instance, we can prove something non-trivial about initial or terminal objects,
and we have some category $\mathcal{C}'$ for which there also exists an initial
or terminal object, then we could immediately use what we've proved to say
something about the objects in $\mathcal{C}'$. And moreover, because we'd be
working on so general a level of reasoning, the fact that such a relationship
exists between **Set** and $\mathcal{C}'$ at all could then suggest something
interesting about $\mathcal{C}'$ or something analogous to empty or singleton
sets.

To continue with other categories, we can also have **Vect**, where objects
are vector spaces, morphisms are linear transformations, and $\circ$ is the
composition of these transformations which in some cases corresponds to matrix
multiplication. Or we could have **Top**, where objects are topological spaces,
morphisms are continuous functions, and $\circ$ is ordinary function
composition. Or we could have a category of logical statements, where objects
are propositions, morphisms are implications, and $\circ$ is the transitivity of
implication.

But the kind of category we want to consider is the category of a programming
language's type system. This category is basically **Set**, since types can be
seen as sets of values, and the language's functions and composition act
basically the same as ordinary functions and composition. But if the language is
pure (i.e. no mutation, side effects, or errors), then this category is also
related to the category of logical statements and implications through the
[Curry-Howard correspondence][curry-howard]. And if the language allows for
user-defined types{{footnote:
    In particular, higher-kinded types are important for some more
    interesting bits from category theory.
}},
then the category of the type system takes on some additional properties that
we'll get into down below.

<!-- In short, here we'll be thinking of categories mostly in terms of **Set**, -->
<!-- because it most closely models how things work in a programming language in -->
<!-- order to understand, in particular, Haskell's `Monad`. -->

### Functors
So then what are endofunctors and categories of endofunctors? As you can
probably guess from the section header, this really translates to a question of
what functors are, since--from the name--endofunctors are actually a special
case of functors.

So what are functors? Simply put, a functor is a map from one category to
another; it maps objects to objects and morphisms to morphisms, with the
constraint that it preserves identity morphisms and composition. More precisely:
For all objects $X$ and morphisms $f, g$, the functor $F$ must satisfy
$$
\begin{aligned}
    F(\text{id}_X) &= \text{id}_{F(X)}
    ,&
    F(f \circ g) &= F(f) \circ F(g)
.\end{aligned}
$$

Functors are conceptually very important in the study of categories. To give
just a single example, we can look at what happens when we interpret a group as
a category. First, we'll quickly define a group:

> A group $\mathcal{G} = (G, \cdot)$ is a set $G$ and a binary operation
> $(\,\cdot\,)$ on the elements of $G$, subject to the following constraints for
> all $g, f, h \in G$:
>
> 1. $G$ is closed under $(\,\cdot\,)$,
>    $$g \cdot f \in G$$
> 1. $(\,\cdot\,)$ is associative,
>    $$(g \cdot f) \cdot h = g \cdot (f \cdot h)$$
> 1. $G$ has an identity element under $(\,\cdot\,)$: There exists some
>    $e \in G$ such that, for any $g \in G$,
>    $$e \cdot g = g = g \cdot e$$
> 1. Every element in $G$ has an inverse under $(\,\cdot\,)$: For every
>    $g \in G$, there exists some $g^{-1} \in G$ such that
>    $$g \cdot g^{-1} = e = g^{-1} \cdot g$$

So right away, we can make some comparisons between categories and groups. The
first three constraints look an awful lot like those in the definition of a
category. The elements of a group $G$ under $(\,\cdot\,)$ are like the morphisms
of the Hom-set $\text{Hom}(\mathcal{C})$ of a category $\mathcal{C}$ under
$(\,\circ\,)$: we require that both operations
1. combine two elements of their domain to return another element of that domain
   (closure),
1. produce the same output no matter the order of application (associativity),
1. and have certain elements on which their action is trivial (identities).

On the other hand, groups only have one kind of thing (the group elements),
while categories have two (objects and morphisms), and groups have an additional
constraint that each element be invertible. The similarity is there, though, and
I'll just cut to the chase. Any group can be interpreted as a category with a
single object, where each morphism is an element of the group and composition of
morphisms is the group operation.

<div style="text-align: center; width: 50%; margin: auto auto;">
    <img
        src="assets/monads/group.svg"
        alt="group cateogry"
        style="width: 100%"
    ></img>
</div>

An important thing to note is that every morphism in this one-object category is
an *isomorphism*, meaning that it's invertible, thanks to the fourth constraint
we placed on groups above. If we relaxed that constraint and allowed
non-invertible morphisms/group elements, then we'd have a "monoid" (referring in
a category-theoretic sense to the single object and its morphisms, and not
necessarily to the whole category), which is another important concept on the
way to monads.

But coming back to groups, though, we also have the concept of a "group
homomorphism", which is a "structure-preserving" map between groups.
Specifically, a group homomorphism $H$ maps the elements of one group
$\mathcal{G}_1 = (G_1, \cdot)$ to another group $\mathcal{G}_2 = (G_2, *)$
in a way that preserves products, identities, and inverses. That is,
1. For all $u, v \in G_1$, $H(u \cdot v) = H(u) * H(v)$.
1. For identities $e_1 \in G_1$ and $e_2 \in G_2$, $H(e_1) = e_2$.
1. For all $u \in G_1$, $H(u^{-1}) = H(u)^{-1}$.

Notice how, like when we interpreted a group as a category, the first two
conditions are essentially those of functors, while the last is specific to
groups (i.e. coming from the fact that group elements are invertible). This
means we can think of groups as special categories with some internal structure
modeled by their morphisms, and group homomorphisms as special functors that
preserve that structure while transforming between groups. But notice *also*
that this is essentially how we think of morphisms between objects. This leads
us to another special category called **Grp**, the category of groups and group
homomorphisms. Here, **Grp** is interesting because it's also a category that
can be thought of as a kind of "category of categories" -- each group/object is
itself also a category, and each morphism can be thought of as a functor between
the object categories.

This is a general pattern that can be repeated for other kinds of categories,
too. Instead of only thinking about group-like categories, we could also imagine
other categories modeled after different mathematical objects like sets or
topological spaces. In this case, functors would be very powerful indeed since
they would describe transformations between different areas of math--
intuitively, functors can be seen as a formalization of the analogy.

### Natural transformations
Consider that, in the process of constructing a functor, one must effectively
define a mapping on morphisms, which are themselves usually some kind of
mapping. And, given a suitable set of categories and a suitable set of functors
between them, one can then construct a category of categories{{footnote:
    There are some special requirements for the kinds of categories used to this
    purpose, but it can be done.
}}.
A reasonable question to then ask is, "what happens when we try to construct a
mapping between functors?"

This kind of mapping is called a natural transformation. Specifically, natural
transformations are maps from functors to other functors with the same input and
output categories that preserve the structure of those functors (which
themselves preserve the structure of the underlying categories). More precisely,
any natural transformation mapping between functors $F \colon \mathcal{C} \to
\mathcal{C}'$ and $G \colon \mathcal{C} \to \mathcal{C}'$ must assign to every
object $X \in \text{Ob}(\mathcal{C})$ a morphism $\eta_X \colon F(X) \to G(X)$,
such that for a morphism $(f \colon X \to Y) \in \text{Hom}(\mathcal{C})$,
$\eta_Y \circ F(f) = G(f) \circ \eta_X$.

This can be rephrased as the following. Suppose there is some transformation
$\eta$ between functors $F$ and $G$, which are themselves transformations
between the objects and morphisms of a category $\mathcal{C}$ and those of
another, $\mathcal{C}'$. Then for every fixed object $X$ in $\mathcal{C}$,
$\eta$ effectively provides two morphisms, $\eta_X \colon F(X) \to G(X)$ and
$\eta_Y \colon F(Y) \to G(Y)$. The condition that a natural transformation
preserves structure translates to the condition that, given a morphism $f \colon
X \to Y$ in $\mathcal{C}$, applying $F(f)$ to $F(X)$ and then $\eta_Y$ is the
same as applying $\eta_X$ on $F(X)$ and then $G(f)$.

This can be drawn as a "commutative diagram," where any two paths with the same
starting and ending objects are equivalent.
<div style="text-align: center; width: 80%; margin: auto auto;">
    <img
        src="assets/monads/natural-transformation.svg"
        alt="natural transformation"
        style="width: 100%"
    ></img>
</div>

If, for every object $X$, the morphism $\eta_X$ is an isomorphism -- that is, if
the morphism $\eta_X$ is invertible -- then $\eta$ is called a "natural
isomorphism."

Just as with categories and functors, it is also sometimes possible to construct
categories of functors and natural transformations, where we then think of
functors as objects, and their natural transformations as morphisms. The last
piece to the monad puzzle is the concept of an *endo*functor, which is simply a
functor that points from a given category back to itself.

### Monads
So finally we come to monads. Now that we know what categories, (endo)functors,
and monoids are, it's time to put it all together. Here's the definition again:

> A monad is a monoid in the category of endofunctors of a fixed category.

We'll start with this fixed category, calling it $\mathcal{C}$, which is left
entirely unconstrained. Here $\mathcal{C}$ is treated as a kind of backdrop, and
we work in the category of its endofunctors, which we'll call $\mathcal{F}$. In
$\mathcal{F}$, the objects are endofunctors on $\mathcal{C}$, morphisms are
natural transformations, and composition is ordinary composition of the natural
transformations.

<div style="text-align: center; width: 70%; margin: auto auto;">
    <img
        src="assets/monads/endofunctor-category.svg"
        alt="endofunctor category"
        style="width: 100%"
    ></img>
</div>

Specifically, a monad is a monoid in this category. Recalling the brief
discussion of groups and monoids above, this means we have a single endofunctor
$M$ in $\mathcal{F}$ (as an object) that has a collection of natural
transformations (*endo*morphisms) that point from that object back to itself
and are not necessarily the identity.

<div style="text-align: center; width: 65%; margin: auto auto;">
    <img
        src="assets/monads/endofunctor-monoid.svg"
        alt="group cateogry"
        style="width: 100%"
    ></img>
</div>

But of course, monoids have special constraints. Specifically, monoids require
1. closure,
1. associativity, and
1. an identity.

These require two special natural transformations on $M$ in order to ensure full
agreement with ordinary (non-category theoretic) monoids. These are, with
$1_\mathcal{C}$ being the identity endofunctor on $\mathcal{C}$,
- $\mu \colon (M \circ M) \to M$ enforcing closure, and
- $\eta \colon 1_\mathcal{C} \to M$ enforcing the existence of the
  identity.

(Associativity comes for free from the assumed properties of a category.) 
{{footnote:
    Fully general category-theoretic monoids can of course be discussed outside
    the context of monads (in which case we work only with ordinary objects and
    morphisms), but this requires additional discussion of what are called
    "monoidal" categories, and this post is long enough as it is. In short,
    though, the double-application of $M$ that's soon to follow this note is
    actually a special case of a "monoidal product," which is, in general,
    merely a way to combine two objects or morphisms of a category together.
    More information in the [supplementary material][monoidal-categories].
}}
$\mu$ and $\eta$ must satisfy some additional constraints known as coherence
conditions, but these are conceptually not important here and are left as
technical details{{footnote:
    The coherence conditions for $\mu$ and $\eta$ on an endofunctor $T$ are:

1. $\mu_X \circ T(\mu_X) = \mu_X \circ \mu_{T(X)}$
1. $\mu_X \circ T(\eta_X) = \mu_X \circ \eta_{T(X)}$

The first of these is an equation between two morphisms $T(T(T(X))) \to
T(X)$, and the second is one for $T(X) \to T(X)$. I don't have very much
intuition for what these mean, except to say that these conditions are the
minimal ones that must hold in order for other required conditions (about
monads) to hold as well.
}}.

So we're left, in fact, with three relevant objects in $\mathcal{F}$, but
everything is still conceptually centered around $M$. Here's the complete
picture:

<div style="text-align: center; width: 65%; margin: auto auto;">
    <img
        src="assets/monads/monad.svg"
        alt="group cateogry"
        style="width: 100%"
    ></img>
</div>

Feeling a bit underwhelmed or confused? A lot of that is probably because
completely mathematical explanations very often don't naturally lend themselves
to intuition. In other words, we've answered the *what*, but not the *why*. But
another important factor is that monads, as with other abstract mathematical
constructs (especially category-theoretic ones), seemingly *need* a context in
which they can be applied in order to get to the *why*.

## Monads in programming
Programming is a particularly nice context for exploring monads. Generically, we
can think of the type system of a programming language as a category where
objects are types, morphisms are functions between those types, and composition
is ordinary function composition. These categories are pretty much **Set**
(since each type can be thought of as a set of values), but you can play around
with them quite easily without any pen-and-paper work just by typing a few
commands on a computer.

There are a couple restrictions to this, however. The first is that there are
important constraints on the choice of language. Namely, we need a language that
has strong types, can assign types to functions and allow functions to be passed
as values to other functions, and allows programmers to create their own types
that can be parameterized by another type. The second restriction is that, in
order to properly constrain the space of possible functions, all functions must
be completely pure--there can be no side effects, e.g. writing to files,
printing to the screen, or changing the state of a random number generator, that
do not have a corresponding encoding in a type.

An obvious choice of language here is Haskell. It's already essentially just a
notation for category theory, but the fact that it still has to work as a
programming language (i.e. describe a computation that can be run on a real
computer) has introduced some interesting design choices that can provide some
insight into the underlying math. For convenience, people generally refer to the
category-of-types-and-pure-functions of Haskell as **Hask**{{footnote:
    It should be said that Haskell's type system actually isn't a category due
    to some technical details, but it's good enough for all intents and purposes
    here.
}}.

### Parametric types
First, we need to talk about parametric types. As the name suggests, it's a type
that is parameterized by another type, which in Haskell looks like this:
```haskell
data Foo a = -- constructors here ...
```
Here, `Foo` is the parametric type and `a` is the type parameter. The
constructors for this type are left undefined, but there can be any number
(including zero!) that take any number of arguments{{footnote:
    Parametric types can also have more than one type parameter, but we'll keep
    it to one for now.
}}.
Crucially, however, `Foo` is *not* actually a type. Instead, `Foo` on its own
really names a particular *family* of types; only when `a` is specified (e.g.
`Foo String` or `Foo Int`) is it a "real," concrete type. That concrete type can
then be instantiated by calling one of its constructors on some input. It's
important to define what we mean by a "constructor," though. Just based on the
name, it's easy to imagine a constructor as any function that takes some data
and "constructs" the concrete type, e.g.
```haskell
constructFoo :: {- some input data... -} -> Foo a
```
but for our purposes, we want to say that a constructor is a specific function
whose only action is to "wrap" the input data in the parametric type. In other
words, if we define a particular constructor called `Make`,
```haskell
data Foo a = Make a
           | -- could be more...
```
then it can only construct a `Foo a` by trivially wrapping up the input `a`: We
want to keep the action of turning an `a` into a `Foo a` separate from whatever
could be done to the particular value passed to `Make` on the value level.

Anyway, the parametricity of `Foo` has a couple important implications. The
first is that, as a construct that requires some type to produce a type, `Foo`
in itself actually defines an object-to-object mapping. In other terms, `Foo` by
itself has a type in Haskell that is expressible as
```haskell
import Data.Kind (Type)

Foo :: Type -> Type
```

The second implication is that any function that takes a `Foo a` (as a generic
object) as argument and returns a `Foo b` (for any type `b`), i.e.
```haskell
bar :: Foo a -> Foo b
```
must define an action not just for any *value* the argument could be, like a
regular function, but for any *types* that `a` and `b` could be as well.

A key insight here is that any parametric type together with any functions on it
together actually define a functor in the proper category-theoretic sense, since
they define a complete mapping over both objects and morphisms, and we only
consider "wrapper" constructors.

To shed more light on the idea of a "wrapper" constructor, let's take a brief
detour into some actual Haskell to see how it all works in practice. In
principle, `Foo a` (for concrete type `a`) is a proper type, like `Int` or
`String`. But if `Foo` actually acted as a regular function between types, we'd
start to loose some of the specialness of what functors do. Such a thing would
also demand that some kind of semantics be developed for performing non-trivial
operations and logic on types themselves, which is a *huge* task. Instead,
Haskell leaves the action of `Foo` on `a` as a kind of unevaluated atom: A `Foo
Int` is simply a `Foo Int`, which is distinct from a regular `Int` or a `Foo
String`. This translates to a near-identical treatment on the value level as
well, when we call `Make` on a particular value: The result of calling `Make 5`
is simply `Make 5`:
```haskell
-- `Foo` is a functor
Foo :: Type -> Type

-- `Foo` called on a type is a type
Foo Int :: Type

-- `Make` is a function that takes a value of type `a` and returns a value of
-- type `Foo a`
Make :: a -> Foo a

-- which means...
Make 5 :: Foo Int
```
`Make 5` is treated as a concrete value on its own since the constructor `Make`
is assumed as a trivial action on the concrete value `5`; it's effectively a
simple "wrapper" around it instead of a regular function that could do something
non-trivial like add one. If we wanted to perform a non-trivial action on `5`
and return it as a `Foo Int`, we'd have to implement a separate function for it,
e.g.
```haskell
-- `makePlusOne` has the same type as `Make` for `a == Int`,
-- but isn't a pure constructor
makePlusOne :: Int -> Foo Int
makePlusOne val = Make (val + 1)
```
And just as we can "wrap" a value using a constructor, we can also "unwrap" it
with the constructor as well:
```haskell
-- bind the value `Make 5` to the variable `someFoo`
someFoo = Make 5

-- bind the value `5` to the variable `x` using the constructor
Make x = someFoo
```
which also only works because the action of `Make` is trivial.

It'd be nice be able to describe this concept in greater generality. While a
function of type `Foo a -> Foo b` will in general entail an operation that will
act in the proper functorial way, every such function will have an
implementation looking something like this:
```haskell
bar :: Foo a -> Foo b
-- we pattern-match on the input to "unwrap" the inner value
bar (Make val) = Make val'
  where val' = -- some operation on `val`...
```

This is kind of annoying to write all the time, and we'd have to define a
different function for every use of the inner value. This leads us to the
concept of a typeclass, which defines a class of types by their behavior under a
certain set of functions. In Haskell, we have a typeclass called `Functor`,
which is defined roughly as follows:
```haskell
class Functor (f :: Type -> Type) where
  fmap :: (a -> b) -> f a -> f b
```
This defines a `Functor` as any parametric type `f :: Type -> Type` that's
associated with a certain function called `fmap` to transform the inner type
wrapped by `f`. To make it useful, `fmap` "factors out" the actions that can be
performed on the inner value as a function `a -> b` that's passed to `fmap` as
an additional argument. `fmap` as a whole therefore has the type `(a -> b) -> f
a -> f b`, meaning that it takes the function and a value wrapped by the
functor, and returns the functor wrapped around the output of the action. We can
then declare that our toy functor `Foo` meets this definition by implementing
`fmap`:
```haskell
-- we declare that `Foo` is an "instance" of the `Functor` typeclass:
instance Functor Foo where
  fmap func (Make val) = Make val'
    where val' = func val
```
But factoring `func` out of the definition comes with a consequence: Now we have
to note the condition that any `Functor` instance must preserve composition and
the identity. This condition is not feasibly expressible in code (the operations
required to actually check that it holds for all uses of a `Functor` are
prohibitively expensive), so we conventionally note as a "law" that, with
composition operator `.` (the same thing as $\circ$) and identity function `id`:
- `fmap id x == id x`, and
- `(fmap f . fmap g) x == fmap (f . g) x`.

Now coming back to the discussion of functors on **Hask**, another key insight
is that any resulting concrete type or function instantiated whenever a concrete
type is provided for `a` *must* be representable in the language (how would the
language function otherwise!), which means it must also be a member of **Hask**.
Therefore, every functor defined by a parametric type and its functions
(`Functor` and `fmap`) is an endofunctor!

So then if parametric types are (endo)functors, what's the equivalent of a
natural transformation? The answer isn't so simple. Rather, in a way it is, but
we have to use a bit of imagination. The basic answer is that any ordinary
function with generic parametric types as both inputs and outputs can act as a
natural transformation. This can be seen by
[recalling](#natural-transformations) the commutative diagram that defines
naturality: Given `Foo a` and another parametric type
```haskell
data Baz b = -- ...
```
the function
```haskell
foobar :: Foo a -> Baz b
```
is effectively a mapping between (endo)functors. To be clear, not every function
with parametric input and output types is a natural transformation, but a
function will need to have them in order to be a natural transformation.

### Monads (again)
Now we have (endo)functors and natural transformations; the only thing left to
discuss is how to actually define monads with Haskell constructs. In principle,
the thing to do would be to define a `Monoid` typeclass, but it turns out we
don't really need one--after all, a monad *is* an endofunctor monoid, so we can
just just skip to defining a `Monad` typeclass instead.

Recall that a monad is essentially an endofunctor $M$ along with two special
natural transformations $\eta \colon 1 \to M$ and $\mu \colon (M \circ M) \to
M$, where $1$ is the identity endofunctor. There are two things here that need
to be translated to Haskell. The first is the identity endofunctor, which simply
takes every object or morphism in **Hask**, and returns it unmodified. In
Haskell, we'd therefore want a parametric type (call it `Id`) with only a single
constructor and a simple implementation of `Functor`. This ends up being exactly
what we've already done for `Foo` above:
```haskell
data Id a = Id a -- `Id` is the name for both the type and the constructor

instance Functor a where
  fmap func (Id val) = Id (func val)
```
But of course this is unnecessary. Since the point of the identity endofunctor
is to do nothing, we'll do exactly that and just say that any type `a` is the
same as `Id a` and any function `f :: a -> *` is the same thing as `fmap f (Id
a)` and save a bit of CPU time.

The second is $M \circ M$. We already know that parametric types are in
themselves functors that act on types by wrapping them and on morphisms with
`fmap`. So a double application of some endofunctor `M` should look like a
double-wrapping of some concrete type, and require two calls to `fmap` for an
ordinary function. Thus we have, for any object/type $A$/`a` and
morphism/function $f \colon A \to B$/`f :: a -> b`:
- $(M \circ M)(A)$ translates to `M (M a)`, and
- $(M \circ M)(f)$ translates to `fmap (fmap f) :: M (M a) -> M (M b)`.

So in Haskell, $\eta$ and $\mu$ translate to two functions (conventionally
called `return` and `join`) that look like this:
```haskell
return :: a -> M a
join :: M (M a) -> M a
```
And in a manner similar to `fmap`, `join` has its internal action "factored out"
into an extra function argument, with the result called `bind`. The more proper
definition goes like this:
```haskell
class Functor m => Monad (m :: Type -> Type) where
  return :: a -> m a
  bind :: (a -> m b) -> m a -> m b
```
The `Functor m =>` bit is called a "context" in Haskell and means that whatever
follows for `m` is only valid in the context that `m` is also a
`Functor`{{footnote:
    The ["actual" definition][haskell-monad] of `Monad` in Haskell's standard
    library has `bind` replaced by an operator `>>=` in order to allow multiple
    `bind` calls to be strung together cleanly, and the `Functor` context
    replaced by an `Applicative` context (which is defined in a `Functor`
    context).
}}.
This definition also comes with the laws that `return` be essentially the same
as a constructor for `m` (i.e. not perform any non-trivial work on the
argument), and that `bind` behave in a way that agrees with the monad coherence
conditions. In Haskell, the requirements are called the monad laws, and can be
expressed as:
- `return` is an identity: `bind f (return x) == f x` and `bind return m == m`
- `bind` respects composition: `bind (\x -> bind g (f x)) m == bind g (bind f
  m)`{{footnote:
    If you're unfamiliar, the `(\_ -> _)` syntax defines an anonymous "lambda"
    function that takes some input argument following the `\` and returns the
    expression following the `->`.
}}

### Examples
So here we'll go through a few examples of monads in Haskell, and relate their
implementations to the category theory above.

#### `Maybe`/`Either`
The simplest non-trivial example of a monad that people usually start with (and
the title of this post) is `Maybe`. Here we're grouping this together with
another monad called `Either` for reasons that will shortly become clear.

The `Maybe a` type refers to values that are nullifyable--that is, it can be
either an ordinary value of type `a` *or* some kind of null value in the case
that things don't go as planned. This is its definition:
```haskell
data Maybe a = Just a
             | Nothing
```
`Maybe` has two constructors: `Just`, which is used for the case where there is
a value, and `Nothing`, which models the null case. Its `Functor` and `Monad`
instances are:
```haskell
instance Functor Maybe where
  fmap f (Just x) = Just (f x) -- `Just` behaves like the identity endofunctor
  fmap _ Nothing  = Nothing    -- can't do anything with a null object

instance Monad Maybe where
  return = Just             -- `return`ing a value is "just" wrapping it

  bind f (Just x) = f x     -- `f` takes the role of a nullifyable operation
  bind _ Nothing  = Nothing -- can't do anything with null
```

`Either` behaves similarly. Like the name suggests, an `Either` is one of two
things, which are called `Left` and `Right`.
```haskell
data Either l r = Right r
                | Left l
```
Notice that `Either` has two type parameters, while we're used to thinking of
functors taking only one argument. This may seem odd, but recall that Haskell
has currying, which means that we can treat `Either` as a functor of one
argument by just supplying a type for the first, e.g.
```haskell
Either :: Type -> Type -> Type

Either Int    :: Type -> Type
Either String :: Type -> Type
-- and so on...
```
So then like `Maybe`, we can declare `Either l` to be a `Monad` -- as long as we
make sure to supply the first type:
```haskell
-- this instance is generic over `l`
instance Functor (Either l) where
  fmap f (Right r) = Right (f r)
  fmap _ (Left l)  = Left l

instance Monad (Either l) where
  return = Right

  bind f (Right r) = f r
  bind _ (Left l)  = Left l
```
Look how similar the instance declarations are! We can see that `Nothing` and
`Left` are just treated as default null values -- the difference is only that
`Either` allows some extra data to be attached to the null case, since `Left`
takes an argument. This makes `Either` a common case for describing the return
value of a computation that can fail (the common pneumonic is that `Right`
indicates the correct or "right" value).

So what does this all mean? From the instances, we can see that `Just`/`Right`
are essentially the identity functor on **Hask**: They're only used to wrap
values, and all functions are simply passed through for those variants. On the
other hand, `Nothing`/`Left` act to essentially map all of **Hask** down to a
single object, and turn all morphisms into the identity. These features also end
up satisfying the monad laws essentially for free.

But the key point is that this behavior turns `Maybe` and `Either` into what are
probably the most well-known and generally useful monads, which most new
programming languages seem to be adopting. This is because `bind` ends up making
for a particularly elegant way to handle chains of nullifyable or fallible
computations. If we work with the operator version of `bind`, defined as
```haskell
(>>=) :: Monad m => m a -> (a -> m b) -> m b
x >>= f = bind f x
```
then we can we can write code that "automatically" handles nulls and errors
without any explicit logic:
```haskell
-- `get` returns `Just` the element of the list at an index, or `Nothing` if the
-- index is out of bounds
get :: Int -> [a] -> Maybe a
get _   []         = Nothing
get 0   (h : _)    = Just h
get idx (h : rest) = get (idx - 1) rest

-- `divideBy` divides one `Float` by another, returning `Just` if the divisor is
-- non-zero, otherwise `Nothing`
divide :: Float -> Float -> Maybe Float
divide num denom = if denom == 0.0
                   then Nothing
                   else Just (num / denom)

-- `Show` is a typeclass that converts values to `String`s that provides a
-- method `show`
toString :: Show a => a -> String
toString = show

-- assume we have a list of numbers
someData :: [Float]

-- example computation: divide 100 by the fifth element of `someData`, and
-- convert the result to a String.
foo :: Maybe String
foo = get 4 someData          -- start with a value that could be null
      >>= (divide 100.0)      -- `divide 100.0` is a curried function
      >>= (return . toString) -- compose of `Maybe`'s `return` and `toString`
                              -- return . toString :: a -> Maybe String
```
In `foo`, `>>=`/`bind` is used to take a value that could be null, and apply two
more operations to the wrapped value. At each step, we apply a function to the
wrapped value (if it exists) that then returns another wrapped value to be fed
into the next application of `>>=`. The definition of `>>=` on `Maybe` allows
for each operation to fail and, more importantly, preserves the null value
through the operational pipeline in the case that one is produced. For example,
if `someData` were less than five items long, the initial `get` would return
`Nothing`; if the fifth item in `someData` were zero, then `divide 100.0` would
return `Nothing`. In either case, the final result of the computation would then
also be `Nothing`, indicating that something went wrong while preserving the
pipeline itself. If we wanted to track where the error came from, we could have
`get` and `divide` return, for example, `Either String _`s instead, where the
`Left` constructor would be used to hold a message describing the error.

#### `List`
`Maybe`/`Either`, in some ways, have become the quintessential monad. But
lists--homogeneous, ordered, linear sequences of items--are another example that
usually comes as a small surprise to people first learning about monads.

In Haskell, lists are usually written `[a]`, but to conform to notation above,
we can also write them as `List a`. It's helpful to look at how they're actually
defined:
```haskell
data List a = []           -- the empty list
            | a : (List a) -- a head element attached to a (possibly empty) body
```
This definition makes it a singly linked list, but it's worth noting that what
follows works for basically all homogeneous, ordered, linear sequences (provided
definitions of `return` and `bind` that amount to identical operations). More
specifically, we can note that every `List a` is the empty list, or an `a`
attached to the front of another `List a`. Wait a second, this looks a lot like
`Maybe`! Let's explore this further...

We can think of a `List` as a container of data (that's what it's for, after
all) in the same way as `Just`. Thus it makes sense to define `fmap` in the
same way, except here we have the possibility that the non-empty variant of the
list contains multiple elements:
```haskell
instance Functor List where
  fmap _ []            = [] -- like `Nothing`: can't do anything with an empty list
  fmap f (head : rest) = (f head) : (fmap f rest) -- like `Just`
```
To get the typing to work out and maintain homogeneity in the list, we define
`fmap` to apply `f` to all elements in the list, which we do recursively. This
is essentially the only definition of `fmap` that (a) satisfies functoriality
requirements, and (b) produces a valid output `List`.

But then how should we implement `Monad`? `return` is straightforward: Given a
single `a`, the only operation that makes sense to convert it to a `List a`
(given functoriality and monadicity requirements) is to return a single-element
`List` containing that item. But then we have to contend with `bind`, which
should apply a function of type `a -> List b` to every element of the list and
return a single `List b`. The naive implementation that behaves like `fmap`
would produce a `List (List b)`, so what is there to do that makes sense under
the monad laws?

The answer ends up being what is commonly called a "flat map" in some languages,
or "map + concat" in others. Specifically, the operation is to apply the `a ->
List b` function to all elements of the initial list, and then "flatten" the
resulting `List (List b)` value by appending all nested lists end-to-end.
Translated to a `Monad` instance:
```haskell
instance Monad List where
  return x = x : [] -- can also be written as `[x]`

  bind _ [] = [] -- can't do anything with the empty list
  bind f (x0 : rest) = concat (f x0) (bind f rest)

-- `concat` takes two lists and sticks them end-to-end, preserving order:
-- concat [0, 1, 2] [3, 4, 5] == [0, 1, 2, 3, 4, 5]
concat :: List a -> List a -> List a
concat []          b = b
concat (a0 : rest) b = a0 : (concat rest b)
```

From here, it's easy to see that these definitions satisfy the monad laws, so
`List` is indeed a *bona fide* monad that actually works quite similarly to
`Maybe`. `[]` is the null variant like `Nothing`, and `(:)` behaves like `Just`,
with the exception that when `List`s are composed, the inner values are combined
rather than replaced.

#### `State`
`Maybe`, `Either` and `List` were fairly trivial (but quite useful!) examples,
so here's a more complicated one. A preeminent goal of any pure, functional
language (or corresponding mathematical model) is to be able to provide an
elegant description of stateful computations. For example, suppose we have an
algorithm that needs to mutate the elements of an array, or we're trying to
describe how a game evolves from moment to moment. In either case, we
generically want to think about two things:
1. the "state" being managed by the computation, and
1. the actual output produced by it.

From the mathematical (and, by extension, Haskell-ian) perspective, this looks
like a function that takes an initial state of type `s` and produces another
state (also of type `s`) along with some output, which is of type `a`:
```haskell
runState :: s -> (s, a)
```
In this picture, evolutions of the state then look like functions of both the
state and intermediate output values:
```haskell
stepState :: ((s, a) -> (s, b)) -> (s, a) -> (s, b)
--           ^                     ^         ^
--           |                     |         `- output state/value
--           |                     `- input state/value
--           `- arbitrary stateful computation
```
In the above, we could allow the type of the state to change in the
transformation, but in principle we should be able to define a single type that
is capable of modeling all possible states.

This already looks somewhat monadic: It looks like we can take some
unconstrained value, wrap it together with some additional data, and feed it
through a series of (composable) transformations to produce a final state and
output. Let's see what we can come up with for our `Functor` and `Monad`
instances.
```haskell
data State s a = State (s, a)

-- like `Either`, `State` is functorial/monadic only in the second type
instance Functor (State s) where
  fmap f (State (state, value)) = State (state, f value)

instance Monad (State s) where
  return x = State (defaultState, x) -- problem: what is `defaultState`?

  bind f (State (state, value)) = f value -- problem: `f` should be able to use
                                          -- `state` to produce its output
```
The problems with this approach were perhaps a bit subtle to have foreseen from
the outset, but they're made apparent when we get to brass tacks. There are two
of them. First, `return` doesn't really work as it should; its type signature
forces us to choose some default state when initially wrapping a value. This
means that the first monad law won't be satisfied, since it means we have to
discard any state information when we pass `return` to `bind`. Second, `bind`
doesn't work as it should, since we can't pass any state data to `f`; this means
the resulting computations (when constrained to `Monad` methods) won't actually
be stateful, since they can't depend on state data.

Instead, the solution (used in roughly the following form by an official
library) shifts perspective a bit. Rather than of thinking about merely the end
state and the output value of the stateful computation, we actually want to
think about the process itself, which is to say the `s -> (s, a)` *function* as
the base object:
```haskell
data State s a = State (s -> (s, a))
```
This definition is perhaps a bit more complicated, but immediately solves the
problem noted above with `return`: We don't have to choose a default state
because the value stored by `State` is inherently agnostic over all states. It
also suggests that the more natural way to think about stateful computations is
in terms of the methods to get from one state to another, rather than a
particular sequence of states{{footnote:
    It also means that `State` is a bit of a misnomer. It isn't really a state
    in itself; it's actually more of a state processor.
}}.
Now let's see how we can get working `Functor` and `Monad` instances.

Here we'll again think of `State s a` as functorial or monadic only in the
second type parameter, which means the functions we have to implement have the
types
```haskell
fmap :: (a -> b) -> State s a -> State s b

return :: a -> State s b
bind :: (a -> State s b) -> State s a -> State s b
```
Removing the indirection created by the `State` constructor, these are
```haskell
fmap :: (a -> b) -> (s -> (s, a)) -> (s -> (s, b))

return :: a -> (s -> (s, a))
bind :: (a -> (s -> (s, a))) -> (s -> (s, a)) -> (s -> (s, b))
```

Our new `Functor` instance looks pretty much the same as the old one, except
that we have to apply the `fmap` argument function through composition using `.`
instead of directly applying it:
```haskell
instance Functor (State s) where
  -- the action of fmap is to compose `f` with the internal state processor
  -- function in a particular way
  fmap f (State prc) = State prc'
    where applyF = apply f      -- aaplyF :: (s, a) -> (s, b)
          prc'   = applyF . prc -- prc'   :: s -> (s, b)

-- this is essentially our old definition of `fmap`: apply `f` to only the
-- value, leaving the state `s` unchanged
apply :: (a -> b) -> (s, a) -> (s, b)
apply f (state, value) = (state, f value)
```

For `Monad`, `return` is again relatively simple--we just construct a function
that combines an initial state with a value, leaving the state untouched.
`bind`, however, is made a bit more complicated by the fact that we need to
think in terms of composition instead of simple application.
```haskell
instance Monad (State s) where
  return x = State (withState x) -- withState x :: s -> (s, a)

  bind f (State prc) = State prc'
    where
      -- unwrap the output of `f` to get the bare transformation on `prc`
      f' = unwrapState . f         -- f' :: a -> s -> (s, b)
      -- define `prc'` as the correct composition of `f` with `prc`
      prc' s0 = (s2, val'')        -- prc' :: s -> (s, b)
        where
          -- define the intermediate state/value using `prc`
          (s1, val')  = prc s0     -- (s1, val')  :: (s, a)
          -- define the new output state/value using `f`
          (s2, val'') = f' val' s1 -- (s2, val'') :: (s, b)

-- `withState` simply combines two things into a tuple
-- we'll only be using this in its curried form above
withState :: a -> s -> (s, a)
withState value state = (state, value)

-- `unwrapState` removes the trivial indirection created by the `State`
-- constructor to expose the bare processor function
unwrapState :: State s a -> (s -> (s, a))
unwrapState (State prc) = prc
```
In the end, this extra complication produces an actual monad! To see a quick
usage example, check out the [supplementary material][state-example].

Categorically, `State` does a bit more work than the previous two examples. As
we can see from its definition, the action of `State` as a functor is really to
transform types into functions{{footnote:
    This is kind of a weird thing to say in a category-theoretic context since
    it seems at first like we have a functor that maps objects to morphisms,
    which is not allowed. But the resolution is that **Hask** includes function
    types, which blurs the difference between the two in some ways
    (e.g. `a -> b` is a function/morphism, but can also be represented as a
    type/object in its own right).
}}.
And as we've seen above, the transformation is rather non-trivial when it comes
to how the transformed objects can be manipulated. This is broadly similar to
the action of what's known as a covariant hom-functor, formally a functor from a
category to **Set**, where each object is mapped to the set of all morphisms
pointing to it, and morphisms are mapped to functions between those sets.
Discussion of [these functors][hom-functors] is well beyond the scope of this
post, but the fact that `State` is still an endofunctor on **Hask** is a
testament to the power of Haskell's type system.

## Intuition
So what insight can we gain from all this? The most basic thing to note is that
monads are a way to wrap values in a context that can contain extra data. In the
case of `Maybe` and `Either`, this data is essentially a boolean flag (augmented
with additional data in the case of `Either`) that encodes whether the value is
valid. For `List`, this data is how many items are being strung together, and in
what order. And for `State`, it's how to compute a value and evolve a state,
given some initial state.

But there's more to a monad than just this data--it's the data *in combination*
with `return` and `bind`/`>>=`, which give a framework for how to create and
compose these wrapped values. The fact that this framework exists and can be
formalized as such allows something to be said about how all this translates to
*impure* computations, which is often thought of as a fundamental tension in
computing theory. Further, a monad's status as an endofunctor over a whole
category--here, all of **Hask**--means that this framework is valid *for any
type that is describable in the Haskell language*.

One interesting way to think about it is to imagine starting from the
programmer's perspective. Depending on the computation, we may want to augment
some value with an extra bit of data, and we may want this behavior to be
generically true for essentially any type that currently exists in the type
system, and we want this to work as a general system that one opts into, just in
case it doesn't really apply to a certain use-case. This is a natural way to
arrive at a parametric type: The parametric type itself encodes some kind of
logical bounds on the data it encloses, and the type parameter allows it to be
applied in different contexts.

Then, we'd want some ways to work with values wrapped in the new type. These
will of course vary between exact use-cases, but it makes sense to want two
basic things:

- A method to wrap bare values
- A method to perform operations on wrapped values within the wrapping context

Unsurprisingly, these are the functions provided by the `Functor` and `Monad`
typeclasses, but from this perspective it only *just so happens* to align with
the ideas of endofunctors and monoids thereof--all the category theory building
up to monads can be viewed simply as formalizations of how to work with wrapped
values. The only difference between the mathematical understanding and the line
of reasoning that thinks of monads as [burritos][burritos] is that (as usual),
the mathematical definitions are much stricter with covering edge cases and
logical phrasings.

---

## Semi-related remarks
I originally wanted to write this post to give code examples in both Haskell and
Rust--since Haskell's syntax is famously quite terse (as mentioned, it's
essentially mathematical notation anyway), I thought Rust's relative verbosity
could make the programming section somewhat easier to follow. But although
Rust's type system does allow for functors and monads in the manner I described
for Haskell, it turns out that traits are just barely not flexible enough to
describe the `Functor` and `Monad` typeclasses in full generality and in a
directly analogous way to how Haskell does it.

Instead, I'll just say that [`Option`][option] and [`Result`][result] are direct
analogues of `Maybe` and `Either`, with `Option::map`/`Result::map` being the
functorial `fmap`, and `Option::Some`/`Result::Ok` and
`Option::and_then`/`Result::and_then` being the monadic `return` and `bind`,
respectively. I'll also link to the crate [`monadic`][monadic], which provides a
trait definition that works but isn't very idiomatic, and [this blog
post][idiomatic-monads], which makes a case for some extensions to the trait
system that would allow a more idiomatic definition.

Alternatively, OCaml's functors are *kind of* flexible enough to define monads.
This would by done by declaring a module type like so:
```ocaml
module type Monad = sig
  (* define a parametric type `m` with type parameter `'a` *)
  type 'a m

  val return : 'a -> 'a m

  val (>>=) : 'a m -> ('a -> 'b m) -> 'b m
end
```
and, correspondingly, we'd have an awkwardly named `Functor` module type as
well:
```ocaml
module type Functor = sig
  type 'a f

  val fmap : ('a -> 'b) -> 'a f -> 'b f
end
```
These module types can sort of be thought of as typeclasses, but the way to
actually use them is awkward for this sort of thing. If we wanted to just define
some functions using some monad, we'd write something like this:
```ocaml
(* some operation(s) using an unspecified monad type belonging to a module `M`
   that has types and functions matching the `Monad` signature

   in order to leave things generic over the monad type, we need to have these
   definitions in what OCaml called a "functor" -- essentially, a module of code
   that depends on another module *)
module MonadicOps (M: Monad) = struct
  (* this is a function that applies a list of functions to some monadically
     wrapped value *)
  let rec foldm (ops: ('a -> 'a M.m) list) (init: 'a M.m): 'a M.m =
    match ops with (* `list` is a linked list type with `:` -> `::` *)
    | f :: rest -> foldm rest M.(init >>= f)
    | []        -> init
end

(* example monad *)
module Maybe = struct
  type 'a m =
    | Just of 'a
    | Nothing

  let return (value: 'a): 'a m = Just value

  let (>>=) (maybe: 'a m) (f: 'a -> 'b m): 'b m =
    match maybe with
    | Just a  -> f a
    | Nothing -> Nothing
end

(* in order to use `foldm` on a concrete `'a Maybe.t`, we need to supply a
   module to `MonadicOps` in order to instantiate a new module that has an
   operable definition of `foldm` *)
module MaybeMethods = MonadicOps (Maybe)

(* now we have a concrete instance of `foldm` that only works on `Maybe.t`s. *)
let fold_maybe = MaybeMethods.foldm
```
This is rather awkward (especially for a blog post), and doesn't allow one to
properly define `Monad` in the context of `Functor`. To impose this constraint,
we'd have to specify it in a separate interface file or at each call site of
`MonadicOps`, since we can't define module types with module parameters (I
think--it actually might be impossible to fully express in code the proper
relationship between `Monad` and `Functor`!).

[curry-howard]: https://en.wikipedia.org/wiki/Curry%E2%80%93Howard_correspondence
[haskell-monad]: https://hackage.haskell.org/package/base-4.20.0.1/docs/Prelude.html#t:Monad
[supplements]: supplements/monads/contents.md
[monoidal-categories]: supplements/monads/monoidal-categories.md
[state-example]: supplements/monads/state-example.md
[hom-functors]: https://en.wikipedia.org/wiki/Hom_functor
[burritos]: https://blog.plover.com/prog/burritos.html
[option]: https://doc.rust-lang.org/std/option/enum.Option.html
[result]: https://doc.rust-lang.org/std/result/enum.Result.html
[monadic]: https://docs.rs/monadic/latest/monadic/index.html
[idiomatic-monads]: https://varkor.github.io/blog/2019/03/28/idiomatic-monads-in-rust.html

