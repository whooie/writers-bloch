# A swift introduction to matrix product states

<details>
<summary>Changelog</summary>

- Originally posted 2024.07.31
</details>

The [previous post][prev] discussed some ideas regarding Clifford circuits and
the theoretical background for why this specific subset of states and circuits
can be simulated with sub-exponential requirements. But of course, this imposes
a rather strict limitation on the kinds of things that can be simulated.
[Clifford circuits with mid-circuit measurements][mipt] will allow you to see a
phase transition in entanglement entropy, but it's generally believed that
Clifford circuits (probably) [aren't even universal for classical
computation][aaronson-gottesman]. If we exit this regime, though, we have to
break the assumptions that allow the Gottesman-Knill theorem to work. This in
turn means that we really just have to accept exponential scaling requirements
in both time and space in order to model states and gates generally. But rather
than resorting to naive vectors and matrices, there are a couple clever things
we can still do to make the simulation a bit nicer.

## Contents
<!-- toc -->

## Entanglement entropy and the Schmidt decomposition
The main target quantity we want to calculate from the simulated states (at
least with respect to measurement-induced phase transitions) is something called
the entanglement entropy. Like most other things that are called entropies, the
entanglement entropy is broadly a way to quantify the information contained in
some system. Here, it's specifically the information that's contained within the
entanglement between two parts of a given system of quantum objects.

People generally use two such entropies. For some density matrix $\rho$ of a
system $\mathcal{S}$ under a given bipartition $\mathcal{S} = A \cup B$ with $A
\cap B = \varnothing$, and reduced density matrices $\rho_A = \text{tr}_B \rho$
and $\rho_B = \text{tr}_A \rho$ for each partition:
- The Von Neumann entropy,
  $$
    S_A(\rho)
        = -\text{tr}(\rho_A \ln \rho_A)
        = -\text{tr}(\rho_B \ln \rho_B)
        = S_B(\rho)
  $$
- The generalized Rényi entropy,
  $$
    S_A^{(\alpha)}(\rho)
        = \frac{1}{1 - \alpha} \ln \text{tr}(\rho_A^\alpha)
  $$
  with $\alpha \geq 0$, which equals the Von Neumann entropy in the limit where
  $\alpha \to 1$

And it's worth noting that usually we only calculate these for pure states,
since the entropy of a density matrix with some amount of mixing will include
contributions from the associated classical distributions.

The naive calculation of these entropies incurs a large runtime cost: Given a
pure state of $N$ qubits, we have to calculate the density matrix
($\mathcal{O}(2^{2 N})$), trace out a subsystem ($\mathcal{O}(2^{3 N})$), either
diagonalize to compute the matrix logarithm ($\mathcal{O}(2^{3 N})$) or compute
a (possibly non-integer) matrix power ($\mathcal{O}^{3 N})$), and then trace
over a resulting matrix ($\mathcal{O}(2^N)$). This is obviously non-ideal.

For some state $\ket{\psi}$ in the product space of two Hilbert spaces
$\mathcal{H}_A \otimes \mathcal{H}_B$, the Schmidt decomposition of $\ket{\psi}$
is the combination of two orthonormal bases $\{ \ket{u_k} \} \subset
\mathcal{H}_A$ and $\{ \ket{v_k} \} \subset \mathcal{H}_B$ together with a
vector of real, non-negative coefficients $\{ \lambda_k \}$ such that
$$
\ket{\psi}
    = \sum_k \lambda_k \ket{u_k} \otimes \ket{v_k}
.$$
It can then be shown that the corresponding reduced density matrices for either
subsystem, when written in the corresponding basis, are diagonal with
eigenvalues $\lambda_k^2$. Thus, the Schmidt decomposition reduces the
calculation of the above entropies to the forms
$$
\begin{gathered}
    S_A(\rho)
        = -\sum_k \lambda_k^2 \ln \lambda_k^2 = S_B(\rho)
    \\
    S_A^{(\alpha)}
        = \frac{1}{1 - \alpha} \ln \sum_k \lambda_k^{2 \alpha}
\end{gathered}
$$

with the caveats that, of course, one still eventually has to perform the Schmidt
decomposition (which is here $\mathcal{O}(2^{3 N})$) and that the Rényi entropy
is for the density matrix in the Schmidt basis, which (I think) gives a
different result from that for the standard (computational) basis.

Moreover, the Schmidt decomposition of a state is related to the singular value
decomposition (SVD) of a matrix in the following way. Given a matrix $A$ of
shape $m \times n$, the SVD of $A$ is the matrices $U$, $\Lambda$, and $V$ such
that $A = U \Lambda V^\dagger$. Specifically, $U$ and $V$ are orthonormal
matrices with shapes $m \times r$ and $r \times n$ with $r = \min\{ m, n \}$.
$\Lambda$ can be seen as either a diagonal matrix of shape $r \times r$ where
the diagonal entries are the "singular values" of $A$, or as a simple vector of
length $r$ containing the same (in which case we instead write $A_{u, v} = U_{u,
j} \Lambda_j (V^\dagger)_{j, v}$ in Einstein summation notation). A state
$\ket{\psi}$ for $N$ particles with the $k$-th particle being characterized by
some quantum number $\sigma_k$ ranging over values $\{ 1, \dots, \#\sigma_k \}$
can be represented as a rank-$N$ tensor{{footnote:
    Here, we'll simply think of a rank-$N$ tensor as a $N$-dimensional array of
    numbers.
}} $\psi_{\sigma_1, \dots, \sigma_N} \in \mathbb{C}^{\#\sigma_1 \times \cdots
\times \#\sigma_N}$. In the same way that $\psi_{\sigma_1, \dots, \sigma_N}$ can
be "flattened" into a one-dimensional vector $\psi_j \equiv \psi_{\langle
\sigma_1, \dots, \sigma_N \rangle} \in \mathbb{C}^{\prod_k \#\sigma_k}$ (where
$\langle \sigma_1, \dots, \sigma_N \rangle$ denotes the fusion of $\sigma_1,
\dots, \sigma_N$){{footnote:
    The "fusion" $j = \langle \sigma_1, \dots, \sigma_N \rangle$ relates the
    single index $j$ of a flattened vector to the $N$ indices of the original
    tensor via the tensor's "strides",
    $$
        j
            = \sum_{k = 1}^N \sigma_k \prod_{m = k + 1}^N \#\sigma_m
    .$$
    This aligns with how arrays are flattened and indices "unraveled" in NumPy.
}}, $\psi_{\sigma_1, \dots, \sigma_N}$ can also be "partially" flattened into
any tensor of rank $1 \leq M \leq N$. For any given bipartition of the system
that divides particles $1, \dots, j$ from particles $j + 1, \dots, N$, we can
have a rank-2 tensor (a matrix) $\psi_{\langle \sigma_1, \dots, \sigma_j
\rangle, \langle \sigma_{j + 1}, \dots, \sigma_N \rangle}$. The SVD of this
matrix exactly corresponds to the Schmidt decomposition of $\psi$ for the same
bipartition, where the columns of $U$ are $\ket{u_k}$, the columns of $V$ (note
the presence/absence of the ${}^\dagger$) are $\ket{v_k}$, and the (diagonal)
elements of $\Lambda$ are $\lambda_k$.

## Schmidt decomposition as a factorization
Matrix product states (MPSs) offer several computational benefits, all of which
stem from the central fact that a MPS is a way to factor states and operators
into local components that can be dealt with individually. A generic MPS is
usually written minimally as
$$
\ket{\psi} = \text{tr}(A_1 \cdots A_N)
$$
where $A_k$ is a matrix or vector whose entries are quantum states, and it's
understood that when two of these matrices are multiplied together, their
entries are multiplied using the tensor product $\otimes$, which usually looks
something like this:
$$
\begin{pmatrix}
    \alpha \ket{a} & \beta \ket{b} \\
    \varsigma \ket{c} & \delta \ket{d}
\end{pmatrix}
\begin{pmatrix}
    \varepsilon \ket{e} & \varphi \ket{f} \\
    \gamma \ket{g} & \eta \ket{h}
\end{pmatrix}
= \begin{pmatrix}
    \alpha \varepsilon \ket{a e} + \beta \gamma \ket{b g}
        & \alpha \varphi \ket{a f} + \beta \eta \ket{b h}
    \\
    \varsigma \varepsilon \ket{c e} + \delta \gamma \ket{d g}
        & \varsigma \varphi \ket{c f} + \delta \eta \ket{d h}
\end{pmatrix}
$$

If $A_1$ and $A_N$ are vectors, then the trace operation can be dropped.

This is rather unintuitive and disguises the use of $A_k$ as containes of data,
though, and usually it's better to think of a MPS as a collection of rank-3
tensors (possibly rank-2 on the ends),
$$
\ket{\psi}
    = (A_1)_{\sigma_1, u_1}
        (A_2)_{u_1, \sigma_2, u_2}
        \cdots (A_k)_{u_{k - 1}, \sigma_k, u_k}
        \cdots (A_N)_{u_{N - 1}, \sigma_N}
        \ket{\sigma_1, \dots, \sigma_N}
$$
where the entries of each tensor are ordinary complex numbers. As a tensor
network{{footnote:
    In a tensor network, a multi-tensor multiplication operation is encoded into
    a graph where nodes are tensors and edges are their indices. An edge
    connects two nodes if that index is summed over, and dangling edges (ones
    that are only connected to one node) are allowed.
}}, this looks like a linear chain of tensors, where tensor $k$ is bonded
tensors $k \pm 1$ and has an unbonded index $\sigma_k$:
<div style="text-align: center; width: 95%; margin: auto auto;">
<img src="assets/mps/mps-generic.gv.png" alt="mps-generic"></img>
</div>

The bond indices $u_k$ can be thought of as characterizing the quantum
correlations (read: entanglement) between the different components of the state.
In a minimal representation (we'll get to what exactly that means below), the
dimension of those bonds (the number of values $u_k$ ranges over) is a rough
measure of how much entanglement connects a particle to the rest of the system.

Working with the state in this form means that operators, the vast majority of
which usually only deal with one or two particles in quantum computing, can
operate on only the relevant particles. In a naive representation using a 1D
state vector, a single particle operator would need potentially many tensor
product with the identity to act on the full $N$-particle state, which is
wasteful in both runtime and memory. For an MPS, the operator acting on the
$j$-th particle needs to be represented only the subspace of the $j$-th
particle -- no need for any applications of the tensor product because the
separation of all physical indices makes the tensoring with the identity
implicit.

The Schmidt decomposition offers a way to transform an arbitrary quantum state
into a MPS. The algorithm is as follows, from a concrete perspective suited to a
classical computer.
1. Interpret the state as a rank-$N$ tensor, with each axis corresponding to the
   relevant quantum number of a single particle, $\ket{\psi} \to \psi_{\sigma_1,
   \dots, \sigma_N}$.
1. Reshape the tensor into a matrix with the first particle's quantum numbers as
   rows and all other quantum numbers fused into the column index,
   $\psi_{\sigma_1, \dots, \sigma_N} \to \psi_{\sigma_1, \langle \sigma_2,
   \dots, \sigma_N \rangle}$.
1. Perform a SVD on $\psi_{\sigma_1, \langle \sigma_2, \dots, \sigma_N
   \rangle}$, yielding $(\Gamma_1)_{\sigma_1, u_1} (\Lambda_1)_{u_1}
   (V_1^\dagger)_{u_1, \langle \sigma_2, \dots, \sigma_N \rangle}$. Store
   $\Gamma_1$ and $\Lambda_1$.
1. Multiply the rows of $V_1^\dagger$ by the elements of $\Lambda_1$, yielding
   $(\phi_1)_{u_1, \langle \sigma_2, \dots, \sigma_N \rangle}$, and reshape to
   fuse $\sigma_2$ with $u_1$, $(\phi_1)_{u_1, \langle \sigma_2, \dots, \sigma_N
   \rangle} \to (\phi_1)_{\langle u_1, \sigma_2 \rangle, \langle \sigma_3,
   \dots, \sigma_N \rangle}$.
1. Perform a SVD on $\phi_1$, yielding $(U_2)_{\langle u_1, \sigma_2 \rangle,
   u_2} (\Lambda_2)_{u_2} (V_2^\dagger)_{u_2, \langle \sigma_3, \dots, \sigma_N
   \rangle}$. Reshape $U_2$ to un-fuse $u_1$ from $\sigma_2$, $(U_2)_{\langle
   u_1, \sigma_2 \rangle, u_2} \to (U_2)_{u_1, \sigma_2, u_2}$, and divide the
   $u_1$ slices of $U_2$ by the non-zero elements of $\Lambda_1$ to get
   $(\Gamma_2)_{u_1, \sigma_2, u_2}$. Store $\Gamma_2$ and $\Lambda_2$.
1. Multiply the rows of $V_2^\dagger$ by the elements of $\Lambda_2$, yielding
   $(\phi_2)_{u_2, \langle \sigma_3, \dots, \sigma_N \rangle}$, and reshape to
   fuse $\sigma_3$ with $u_2$, $(\phi_2)_{u_2, \langle \sigma_3, \dots, \sigma_N
   \rangle} \to (\phi_2)_{\langle u_2, \sigma_3 \rangle, \langle \sigma_4,
   \dots, \sigma_N \rangle}$.
1. Continue until a total of $N - 1$ SVDs have been performed. $\Gamma_N$ can be
   computed from the last $(V_{N - 1}^\dagger)_{u_{N - 1}, \sigma_N}$ by
   dividing its rows by the non-zero elements of $\Lambda_{N - 1}$.

The total decomposition looks like this:
$$
\psi_{\sigma_1, \dots, \sigma_N}
    = (\Gamma_1)_{\sigma_1, u_1}
        (\Lambda_1)_{u_1}
        (\Gamma_2)_{u_1, \sigma_2, u_2}
        (\Lambda_2)_{u_2}
        \cdots (\Lambda_{N - 1})_{u_{N - 1}}
        (\Gamma_N)_{u_{N - 1}, \sigma_N}
$$
<div style="text-align: center; width: 95%; margin: auto auto;">
<img src="assets/mps/mps-canonical.gv.png" alt="mps-canonical"></img>
</div>

This is essentially the generic MPS shown above, except that now we have scalars
$(\Lambda_k)_{u_k}$ on all of the bond indices, which are the singular values of
each decomposition.

In general, some singular values will be zero. When contracting the MPS into a
single object, this means all $u_j$ slices of $\Gamma_j$ or $\Gamma_{j + 1}$ for
which any $(\Lambda_j)_{u_j}$ is zero will have no contribution to the total
sum, and hence those slices can be discarded during the algorithm above without
losing any information about the total state. Since the Schmidt decomposition is
unique, this means we have a minimal, lossless representation of an arbitrary
state after the discards are performed. Hence, this is called a canonical form
of the MPS. If we instead filter singular values by some positive threshold (and
renormalize), we can then have a rigorous way to further reduce the
representation, at the cost of losing some information.

Once in the canonical form, though, the entanglement entropy across any
bipartition can simply be read off from the relevant singular values. Although
the runtime cost of factoring an arbitrary $N$-qubit state is still essentially
$\mathcal{O}(2^{3 N})$, we can note that the $\ket{0, \dots, 0}$ state has an
easy, fixed factorization that is independent of $N$ with
$$
\begin{aligned}
    (\Gamma_k)_{u_{k - 1, \sigma_k, u_k}}
        &= \begin{dcases}
            1 & \text{if } \sigma_k = 0
            \\
            0 & \text{otherwise}
        \end{dcases}
    \quad&\quad
    (\Lambda_k)_{u_k}
        &= \begin{dcases}
            1 & \text{if } u_k = 0
            \\
            0 & \text{otherwise}
        \end{dcases}
.\end{aligned}
$$
so if we limit initialization to $\ket{0, \dots, 0}$ or any other equally
trivial product state, we can avoid that initialization cost.

## The canonical form
Another advantage to the canonical form is that all $\Gamma$ tensor, when
contracted through the relevant $\Lambda$ vectors, are orthonormal. This means
that the following contractions are all equal to the identity (which can be
considered as an empty wire in tensor network language):

<div style="width: 100%; overflow: hidden;">
    <div style="width: 49%; display: inline-block;">
        $$
        (\Gamma_1)_{\sigma_1, u_1} (\Gamma_1^\dagger)_{u_1', \sigma_1}
        $$
    </div>
    <div style="width: 49%; display: inline-block;">
        <div style="text-align: center; width: 45%; margin: auto auto;">
            <img
                src="assets/mps/canonical-id-edgel.gv.png"
                alt="canonical-id-edgel"
                style="vertical-align: middle"
            ></img>
        </div>
    </div>
</div>

<div style="width: 100%; overflow: hidden;">
    <div style="width: 49%; display: inline-block;">
        $$
        (\Gamma_N)_{u_{N - 1}, \sigma_N} (\Gamma_1^\dagger)_{\sigma_N, u_{N - 1}'}
        $$
    </div>
    <div style="width: 49%; display: inline-block;">
        <div style="text-align: center; width: 45%; margin: auto auto;">
            <img
                src="assets/mps/canonical-id-edger.gv.png"
                alt="canonical-id-edger"
                style="vertical-align: middle"
            ></img>
        </div>
    </div>
</div>

<div style="width: 100%; overflow: hidden;">
    <div style="width: 49%; display: inline-block;">
        $$
        (\Gamma_k)_{u_{k - 1}, \sigma_k, u_k}
            (\Lambda_{k - 1})_{u_{k - 1}}^2
            (\Gamma_k^\dagger)_{u_{k - 1}, \sigma_k, u_k'}
        $$
    </div>
    <div style="width: 49%; display: inline-block;">
        <div style="text-align: center; width: 85%; margin: auto auto;">
            <img
                src="assets/mps/canonical-id-midl.gv.png"
                alt="canonical-id-midl"
                style="vertical-align: middle"
            ></img>
        </div>
    </div>
</div>

<div style="width: 100%; overflow: hidden;">
    <div style="width: 49%; display: inline-block;">
        $$
        (\Gamma_k)_{u_{k - 1}, \sigma_k, u_k}
            (\Lambda_{k})_{u_k}^2
            (\Gamma_k^\dagger)_{u_{k - 1}', \sigma_k, u_k}
        $$
    </div>
    <div style="width: 49%; display: inline-block;">
        <div style="text-align: center; width: 85%; margin: auto auto;">
            <img
                src="assets/mps/canonical-id-midr.gv.png"
                alt="canonical-id-midr"
                style="vertical-align: middle"
            ></img>
        </div>
    </div>
</div>

Hence, the expectation value of any local operator $O_{\sigma_k', \sigma_k}$
simplifies to the contraction with only one $\Gamma$ tensor (and its conjugate)
and either 1 or 2 $\Lambda$ vectors, depending on whether $k$ is at an end of
the chain:
$$
\langle O \rangle
    = (\Lambda_{k - 1})_{u_{k - 1}}^2
        (\Gamma_k)_{u_{k - 1}, \sigma_k, u_k}
        O_{\sigma_k', \sigma_k}
        (\Gamma_k^\dagger)_{u_{k - 1}, \sigma_k', u_k}
        (\Lambda_k)_{u_k}^2
$$
<div style="text-align: center; width: 60%; margin: auto auto;">
    <img
        src="assets/mps/canonical-ev.gv.png"
        alt="canonical-ev"
        style="vertical-align: middle"
    ></img>
</div>
and we get similar simplifications for computing partial traces over the state
if we ever really needed them.

## Circuit operations
There are three kinds of operations that we need to fit into this formalism:
1. Single-qubit gates
1. Two-qubit gates
1. Randomized, site-resolved projective measurements

Single-qubit gates are trivial -- as long as they're unitary (which they should
be), they can be applied indiscriminately to any $\Gamma_k$ in the MPS while
maintaining the proper normalization conditions that allow the above identities.

The latter two are a bit trickier. The reason is that both two-qubit gates and
projective measurements are things that affect the amount of entanglement
between particles, and hence need to modify the magnitude and/or number of
singular values across some number of bonds; this means they have non-trivial
action on a given Schmidt decomposition. Although simulators benefit from a MPS
factorization when applying single-qubit gates, the inherent locality in the MPS
works to our detriment when we want to effect behavior that extends beyond a
single particle. These behaviors include things like generating entanglement
through a multi-qubit gate or collapsing a multi-body wavefunction with a
projective measurement (which also acts on the $\Gamma$ tensors that the
measured particle is entangled to).

Since the exact actions on the singular values and $\Gamma$ tensors are
difficult to predict in general (hence the need for a simulator), we have to
resort to a scheme where we locally contract part of the MPS, apply the
two-qubit gate or projective measurement, and then re-factor those particles.
For two-qubit gates, we need to contract and refactor every bond between
the qubits that the gate acts on, and for measurement there's no way to
generally predict where entanglements lie (again, hence the need for
simulation), so this basically amounts to contracting the entire state.

One important consequence of this is that the runtime of a circuit simulation
has super-quadratic scaling with the exponential of the number of qubits, and
additionally has explicit dependence on the measurement probability $p$ (in the
case of a randomized circuit for measurement-induced phase transitions). When
$p$ is small, the bond dimensions in the MPS grow larger than when $p$ is large,
which means that more work has to be done for each measurement or gate
application. To losslessly represent a completely random state in the $N$-qubit
Hilbert space, the maximum bond dimension occurs at the middle of the MPS at
$D_\text{max} = 2^{\lfloor N / 2 \rfloor}$. Note that the SVD of a matrix of
shape $m \times n$ has runtime $\mathcal{O}(\max\{ m n^2, m^2 n \})$.

## Wrapping up
So how well does all this work in practice? Well, everything here still suffers
from the same exponential scaling as the naive approach -- it's been shown that
any classical simulation of a completely general quantum simulation will share
this as well. But the MPS representation allows us to carve out certain niches
in the total space of states and operations where we can beat it. For everything
else, MPSs pretty much break even with the naive approach.


[prev]: efficient-sim.md
[mipt]: efficient-sim.md#introductionmotivation
[aaronson-gottesman]: https://arxiv.org/abs/quant-ph/0406196

