# One Must Imagine Schrödinger Happy

- nobody gets the point of Schrödinger's cat because they're all fucking normies
- or entropy: https://twitter.com/fermatslibrary/status/1398273869648179204
- part of the reason is because normies are hugely susceptible to mysticisms:
  when a mysticism is encountered, they completely ignore intuitions
- there's no reason to respect any normie opinion
- except there is, because the world is full of normies
- how can we live with the understanding that the vast majority of people don't
  have the knowledge necessary to talk about anything important?
- we can (a) bend over backwards for whoever claims to be an expert, or (b) stop
  being normies
- something something Moloch
- also people not understanding distributions and averaging

