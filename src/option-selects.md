# Linguistic option-selects

- in fighting games, an option select is a combination of inputs that commits to
  multiple outcomes without extra thought on your part, depending on how an
  upcoming interaction with your opponent plays out
    - examples:
        - up-B into the ledge with a shield input -- wall-tech if you get hit,
          nothing if you miss
        - full-press shield for L-cancel to tech
        - wavedash out of shield option-selects for tech
        - hard shield input and then C-stick down + left stick down-back
          option-selects for Amsah tech or down-smash on a knocked-down opponent
- a linguistic option select is when you say a phrase with multiple meanings and
  mean all of them
