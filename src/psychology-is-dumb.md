# Psychology is dumb

- replication crisis, etc.
- coupling to humans cuts your signal/noise by a huge amount, and psychologists
  rarely ever have the quantities of data needed to overcome it
- lack of a 'BEC theory'
- psychology is the control group of science
- nothing is correlated for anything interesting, and anything that does show
  correlation is trivial

