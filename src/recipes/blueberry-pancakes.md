# Blueberry Pancakes

Taken from [/u/sprinklesapple](https://www.reddit.com/r/food/comments/poe28g/homemade_blueberry_pancakes/hcwokt8/).

## Ingredients for pancakes
- 2 c. all-purpose flour
- 3 tbsp. sugar
- 1.5 tsp. baking powder
- 1.5 tsp. baking soda
- 1.25 tsp. kosher salt
- 2.5 c. buttermilk
- 2 large eggs, beaten
- 3 tbsp. unsalted butter, melted
- 1 tbsp. vanilla extract
- 1 pinch ground cinnamon
- lemon zest
- 1 c. blueberries

## Method for pancakes
1.  Whisk flour, brown sugar, baking powder, baking soda, cinnamon, zest, and
    salt together in a large bowl.
1.  Make a well in the center using your whisk. Pour buttermilk, eggs, extract,
    and butter into the well. Starting from the center, whisk together until
    everything is incorporated. Fold blueberries into the batter. Avoid
    over-mixing, even if lumps remain. To avoid coloration of the cake, dust
    fresh blueberries with flour or avoid thawing frozen blueberries. Let rest
    for 30 minutes at room temperature.
1.  Heat a skillet on medium-low heat and add a dollop of butter. Once melted,
    add 1/3 c. of batter into the skillet. (If the mixture is too thick, add a
    splash of milk.)
1.  Flip pancakes once bubbles rise to the surface and the bottom has browned,
    usually around 3 minutes per side.

## Ingredients for blueberry compote
- ~1.25 c. blueberries
- 0.25 c. white sugar
- 2 tsp. lemon juice
- 0.25 tsp. lemon zest
- 3 tbsp. water

## Method for blueberry compote
1.  Combine 1 c. blueberries, water, zest, juice, and sugar into a saucepan.
    Cook over low heat until the sugar has dissolved. Increase heat to medium
    and bring to a gentle boil. Stir quite often until thickened, about 10-15
    minutes.
1.  Take off heat and let cool for 5 minutes. Mix in the remaining blueberries.

