# Bread Pudding

From the [New York Times][link]

> Note: There's plenty of room for customization here: consider adding fresh or
> dried fruit or a combination of spices like cinnamon, nutmeg, allspice, and
> cardamom.

## Ingredients
- 2 c milk
- 2 tbsp unsalted butter
- 1 tsp vanilla extract
- 0.33 c sugar
- 1 pinch salt
- 0.5 loaf sweet egg bread, cut into 2-inch cubes (about 5-6 c)
- 2 eggs, beaten

## Method
1. Heat oven to 350F. In a small saucepan over low heat, warm milk, butter,
vanilla, sugar, and salt. Continue cooking just until butter melts; allow to
cool. Meanwhile, butter a 4-6-c baking dish and fill it with cubed bread.

1. Add eggs to cooled milk mixture and whisk; pour mixture over bread and wait
for the mixture to thoroughly soak into the bread.

1. Bake for 30-45 minutes, or until custard is set but still a little wobbly and
the edges of the bread have browned. Serve warm or at room temperature.

[link]: https://cooking.nytimes.com/recipes/1012636-simple-bread-pudding

