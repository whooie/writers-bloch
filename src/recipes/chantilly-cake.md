# Chantilly Cake

From [Love From the Oven][link].

## Ingredients
Sponge:
- 3 c cake flour
- 1 tbsp baking powder
- 0.5 tsp salt
- 0.75 c unsalted butter (softened)
- 1.5 c sugar
- 2 tsp vanilla extract
- 1 tsp almond extract (optional)
- 6 large egg whites
- 0.75 c milk

Simple syrup:
- 2 c water
- 1 c sugar

Frosting:
- 2 c heavy cream
- 1 tsp vanilla
- 8 oz cream cheese (softened)
- 8 oz mascarpone (softened)
- 2 c powdered sugar

Fruits:
- 1 c blueberries
- 1 c strawberries (sliced)
- 1 c raspberries (sliced)

## Method
Sponge:
1. Prepare two 8-inch cake pans by lining the bottom with a round piece of
parchment paper. Lightly spray the sides of the pan with nonstick baking spray.
Preheat an oven to 350F.
1. In a medium bowl, stir together the flour, baking powder, and salt and set
aside.
1. In a large mixing bowl or stand mixer, beat together the butter and sugar
until creamy. Beat in the egg whites and vanilla, as well as almond extract.
Slowly add in the flour mixture and milk, mixing as you add the ingredients. The
batter should look light and fluffy.
1. Divide the batter evenly between prepared cake pans. Bake 25-28 minutes. The
cakes should spring to the touch when gently poked in the center. Remove the
cakes from the oven and allow them to cool.

Simple syrup:
1. In a medium saucepan, bring water and sugar to a boil, stirring until the
sugar has completely dissolved, then set aside.

Frosting:
1. Whisk the vanilla and heavy cream until it's almost stiff like shaving cream.
1. In a medium bowl, beat together the cream cheese, mascarpone, and pwdered
sugar until well-combined and creamy.
1. Gently combine the cream mixture with the cheese mixture until smooth and
creamy.

Cake assembly:
1. Once cooled, level the caked by cutting the top off each cake. Then cut each
cake in half, using a long knife.
1. Brush the top of each cake with the simple syrup.
1. Place a light layer of the frosting on the top of each cake, and then
sprinkle in the fruit. Lightly press the fruit into the frosting.
1. Add each layer of cake and repeat frosting and adding fruit until all four
layers are done. Frost the entire outside of the cake and top with fruits or
decorate as desired.

[link]: https://www.lovefromtheoven.com/chantilly-cake-recipe/
