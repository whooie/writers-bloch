# Chicken Tikka Masala

From [Bon Appétit][link]

[link]: https://www.bonappetit.com/recipe/chicken-tikka-masala

## Ingredients
- 6 cloves garlic, finely grated
- 4 tsp ginger, peeled and finely grated
- 4 tsp ground turmeric
- 2 tsp garam masala
- 2 tsp ground coriander
- 2 tsp ground cumin
- 1.5 c whole-milk yogurt (not Greek)
- 1 tbsp kosher salt
- 2 lb skinless, boneless chicken thighs
- 3 tbsp ghee or vegetable oil
- 1 small onion, thinly sliced
- 0.25 c tomato paste
- 6 pods cardamom, crushed
- 2 árbol chilis or 0.5 tsp crushed red pepper flakes
- 1 28-oz can whole peeled tomatoes
- 2 c heavy cream
- 0.75 c chopped cilantro, plus sprigs for garnish

## Method
1. Combine garlic, turmeric, garam masala, coriander, and cumin in a small bowl.
Whisk yogurt, salt, and half of spice mixture in a medium bowl; add chicken and
turn to coat. Cover and chill 4-6 hours. Cover and chill remaining spice
mixture.
1. Heat ghee in a large, heavy pot over medium heat. Add onion, tomato paste,
cardamom, and chilis and cook, stirring often, until tomato paste has darkened
and onion is soft, about 5 minutes. Add remaining half of spice mixture and
cook, stirring often, until bottom of pot begins to brown, about 4 minutes.
1. Add tomatoes with juices, crushing them with your hands as you add them.
Bring to a boil, reduce heat, and simmer, stirring often and scraping up browned
bits from the bottom of the pot, until sauce thickens, 8-10 minutes.
1. Add cream and chopped cilantro. Simmer, stirring occasionally, until sauce
thickens, 30-40 minutes.
1. Meanwhile, preheat broiler. Line a rimmed baking sheet with foil and set a
wire rake inside the sheet. Arrange the chicken on the rack in a single layer.
Broil until the chicken started to blacken in spots (it will not be cooked
through), about 10 minutes.
1. Cut the chicken into bite-sized pieces, add to the sauce, and simmer,
stirring occasionally, until cooked through, 8-10 minutes. Serve with rice and
cilantro sprigs.

