# Chocolate Chip Cookies

Adapted from *Milk Bar Life* by Christina Tosi
([source](https://www.bloomberg.com/news/articles/2015-03-13/how-to-make-perfect-chocolate-chip-cookies-recipe-by-momofuku-milk-bar-s-christina-tosi))

Makes about 18 cookies

## Ingredients
- 1/2 lb. (2 sticks) unsalted butter, melted and just warm to the touch
- 3/4 c. packed light brown sugar
- 1/2 c. granulated sugar
- 1 egg
- 2 tsp. vanilla extract
- 1 3/4 c. all-purpose flour
- 2 tbsp. non-fat milk powder
- 1 1/4 tsp. kosher salt
- 1/2 tsp. baking powder
- 1/4 tsp. baking soda
- 12 oz. semisweet chocolate chips

## Method
1. Heat the oven to 375$\deg$F. With a wooden spoon, mix together the melted
   butter and sugars in a large bowl until homogeneous, about a minute. Add the
   egg and vanilla and stir until combined.
1. Mix in the flour, milk powder, salt, baking powder, and baking soda until
   just combined. Add the chips and mix until evenly distributed.
1. Portion 2 3/4 oz. scoops of dough about 2 to 3 in. apart onto a parchment
   paper-lined baking tray and bake for 10 to 12 min. until golden brown. Cool
   completely on the pan.

