# Imitation Unagi Sauce

Everything here is pretty approximate. There's quite a bit of room for error.

## Ingredients
- 1/2 c. soy sauce
- 1/4 c. thick mirin
- 1/4 c. pulverized rock sugar
- 1/4 c. rice vinegar

## Method
1. Combine everything in a saucepan over very low heat until the rock sugar has
   melted, then raise to medium-low to reduce until the sugar begins to drop out
   of solution and rise to the surface as a persistent brown foam. Use sparingly
   in other dishes.
