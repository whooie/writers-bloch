# Miso-Marinated Salmon

Adapted from "The Splendid Table" by America's Test Kitchen/Cook's Illustrated

## Ingredients
- 1/2 c. white miso
- 1/4 c. maple syrup (less about 2 tsp.)
- 3 tbsp. sake
- 3 tbsp. mirin
- 4 or 5 6-8 oz. skin-on salmon fillets
- Lemon wedges

## Method
1. Whisk miso, sugar, sake, and mirin together in a bowl until sugar and miso
   are dissolved. Dip each fillet into miso mixture to evenly coat all flesh
   sides. Place fish skin side-down in baking dish and pour any remaining miso
   mixture over fillets. Cover with plastic wrap and refrigerate for at least 6
   hrs. or up to 24 hrs.
1. Adjust oven rack 8 in. from broiler element and heat broiler. Place wire rack
   in rimmed backing sheet and cover with aluminum foil. Using your fingers,
   scrape miso mixture from fillets (do not rinse) and place fish skin side-down
   on foil, leaving 1 in. between fillets.
1. Broil salmon until deeply browned and centers of fillets register 125$\deg$F
   (8-12 min.), rotating sheet halfway through cooking and shielding edges of
   fillets with foil if necessary. Transfer to platter and serve with lemon
   wedges.

