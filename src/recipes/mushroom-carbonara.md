# Mushroom Carbonara

From [Bon Appetit][link]

## Ingredients
- Salt
- 1.5 lb crimini or button mushrooms
- 6 cloves garlic
- 2 medium shallots
- 1 c parsley leaves with tender stems
- 5 large egg yolks
- 1 large egg
- 4 oz parmesan, grated
- 1.5 tsp black pepper
- 0.25 c extra-virgin olive oil
- 1 lb pasta

## Method
1. Bring a pot of water for the pasta (will a few big pinches of salt) to a
boil. Add the pasta and cook until ~2 minutes shy of al dente.
1. Tear off the stems of the mushrooms, slicing in half. Quarter the heads.
Lightly smash and peel the garlic, then thinly slice. Peel and finely chop dice
the shallots. Coarsely chop the parsley.
1. Whisk the eggs and parmesan together with the black pepper in a medium bowl
and set aside.
1. Heat a large pan over medium-high and fry the mushrooms (with salt) in the
olive oil until golden brown. Then add the garlic and shallots and cook until
softened but not browned.
1. When pasta is cooked reserve 2 c of the water and drain pasta. Add the pasta
to the Dutch oven and cook over medium-low, adding 1 c of the pasta water
bit-by-bit to finish cooking the pasta. Once done, set aside to cool for 1
minute.
1. Add 0.5 c pasta water to the egg mixture and whisk to combine and loosen.
Gradually add the egg mixture to the Dutch oven, stirring vigorously and adding
more pasta water as needed to loosen things up.
1. Finish with parsley and serve, topping with more grated parmesan and black
pepper.

[link]: https://www.bonappetit.com/recipe/mushroom-carbonara

