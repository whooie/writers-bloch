# Pork Belly Stew

Adapted from Philosokitchen's recipe by Filippo Trapella

## Ingredients
- 2 lbs. pork belly (preferably with skin on)
- 10 spring onions
- 4 cloves garlic
- 1 c. liao jiu (a.k.a. Shaoxing wine)
- 2/3 c. soy sauce
- 1 1/2 c. vegetable broth
- 4 star anise pods
- 2 cloves
- 6 tbsp. pulverized rock sugar
- A bit of ginger (enough for a couple large slices)
- Daikon, carrots to preference

## Method
1. Cut the pork belly into strips 1-1.5 in. wide and parboil for 8-10 min.
   Afterwards, wash thoroughly, cut into chunks, and dry with a paper towel.
1. Pour 2-3 tbsp. cooking oil into a wok or large pan along with 4 tbsp. of the
   rock sugar over medium-low heat. When the sugar is well melted and turns
   color, add pork belly and fry until golden-brown, then add the liao jiu and
   raise the flame to medium. Cook for 5 min., stirring occasionally.
1. Pour 2 tbsp. cooking oil into a thick-bottomed, heavy pot. Slice the spring
   onions into pieces about a quarter of an inch wide, peel and crush the
   garlic, and slice the ginger thinly. Add the onions, garlic, ginger, star
   anise, and cloves, and fry until fragrant.
1. Add the pork and associated liquid with the remaining rock sugar first, then
   the vegetables, soy sauce, and vegetable broth. About 3/4 of the total solids
   should be submerged. Cover and simmer until the pork is tender.

