# Pulled Pork

## Ingredients
- Pork shoulder, bone-in and preferably with some skin, 3-4 lbs
- 1 large white onion (yellow may be a bit sweeter)
- 1 large orange, preferably somewhat sour
- 2 limes
- 2 large cloves of garlic
- 1-3 jalapenos, depending on their spiciness
- 2-3 bay leaves
- 1 tsp whole black peppercorns
- 1 tsp whole coriander seeds
- 1 tbsp soy sauce
- 1 tbsp liquid smoke
- dried oregano
- cayenne pepper
- chipotle powder
- garlic powder
- mustard powder
- white pepper powder

## Prep
(Feel free to intersperse with steps 1 and 2)
- Chop the onion. The exact form of your chopping doesn't really matter, since
  they'll end up cooking long enough to almost completely break down.
- Zest and juice the orange and limes. Collect in a bowl.
- Slice off the tops of the jalapenos.
- Roughly chop the garlic. (Or finely mince, it doesn't really matter.)

## Method
**Night before:**
1. Remove the skin (if present) and excess fat from the pork. Lightly score the 
   skin in a cross-hatch pattern, with ~1.5 cm spacing. Save the skin and fat   
   for later in the refrigerator. Make the dry rub. Ratios are roughly:
    - 2: salt
    - 2: oregano
    - 1: cayenne
    - 1: chipotle powder
    - 1/2: garlic powder
    - 1/2: white pepper powder
    - 1/5: mustard powder

   Tip: make about 20% more than you think you'd need to cover the whole piece  
   of pork. Onion powder would probably also be pretty good here, but I don't use
   any. Feel free to play around with the ratios, it's all pretty forgiving.
   Apply the rub to the pork (making sure to get it in any/every available
   crevice), and set uncovered in the refrigerator.

**Day of:**
1.  Take the pork/skin/fat out of the refrigerator. Add the skin (fat-side down)
    and fat to a cold pan and heat over medium-low to render the fat. Once hot
    (or the pieces of fat are beginning to brown), flip the skin over and fry
    lightly. You may wish to start with just a bit of neutral oil in the pan so
    that things don't stick.
2.  Once the fat is golden brown, or you have about a good amount of it already
    rendered, remove the skin and bits of fat, then turn the heat up to
    medium-high. Fry the pork on all sides for about a minute each or until you
    get some good color. Be careful not to let the oregano in the rub burn.
3.  Remove the pork, turn the heat down to medium and add the onion and garlic.
    Add salt to draw water out of the onion to deglaze the pan. If you're not
    getting enough liquid out soon enough, use some water / chicken stock / dry
    sherry / shaoxing / white wine / whatever instead. Add a large pinch more
    oregano and cook until fragrant.
4.  Transfer contents to a slow-cooking vessel. With the onions on the bottom,
    add the skin first, and then the pork, plus any associated liquids that may
    have been released while resting. Add the jalapenos, bay leaves, black
    peppercorns, coriander seeds, and zest/juice, along with the soy sauce and
    liquid smoke.
5.  Slow cook fairly aggressively (I use the "high" setting on my instant pot)
    for 8-10 hours. If there ends up not being enough liquid to fully submerge
    the piece of pork (which is likely), flip every 2-3 hours.
6.  When done, remove the pork, skin, jalapenos, and any pieces of onion that
    have managed to stay together and set in a large bowl. Shred all of it
    together.
7.  While shredding, transfer the braising liquid to a small pot and reduce to a
    glaze. Mix into the shredded pork when fully reduced.

# Optional Quick Pickles

## Ingredients
- 3 small Persian cucumbers
- 1/2 cup apple cider vinegar
- 1 tbsp whole-grain mustard or mustard seeds
- a few sprigs of fresh dill
- 2 cloves garlic
- 1/2 tbsp coriander seeds
- 1/2 tbsp whole black peppercorns

## Method
1.  Thinly slice the cucumbers and smash the garlic, and add to a large pickling
    jar.
2.  Add everything else.
3.  add enough water to top off the jar.
4.  Leave to sit in the refrigerator. Flavor will begin to develop after a
    couple days.

