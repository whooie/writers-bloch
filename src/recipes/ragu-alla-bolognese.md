# Ragù alla Bolognese

From reddit user `/u/danos95`

Makes about 6 servings

## Ingredients
- 250 g. beef, hand minced (chuck, brisket, short rib, shank all work)
- 250 g. pork, hand minced (shoulder is best)
- extra-virgin olive oil
- 1 onion, diced
- 1 rib celery, diced
- 1 carrot, diced
- 2.5 tbsp. tomato paste
- 1 c. red wine
- 1 c. chicken or beef stock, plus a bit more on reserve
- 1 c. whole milk
- 1 cinnamon stick
- 1 bay leaf
- a large-ish segment of lemon peel
- a pinch of nutmeg

## Method
1. Use a sharp knife or a heavy cleaver to finely chop the meat into a coarse
   mince.
1. Heat a heavy bottomed pot on high-heat add enough olive oil to cover the
   bottom about 1/4 inch thick then add beef and saute for 6-7 minutes until
   browned on the outside. Remove the beef with a slotted spoon ensuring you
   leave rendered fat in the pot and set aside.
1. Repeat the above step for the pork.
1. Add onions, carrot, celery and saute for 15 - 20 minutes.
1. Add red wine, then the beef and pork back in, then tomato paste, stock, milk,
   cinnammon, bay leaf, nutmeg, lemon peel. Give everything a good stir and
   season with salt (about 1 tbs) and black pepper
1. Simmer on low heat for 3-6 hours - taste every half an hour; if it gets dry
   top up with either water or stock. It's done when the meat is tender and no
   longer chewy; obviously the longer you cook the more tender/melt in your
   mouth it will get. 

