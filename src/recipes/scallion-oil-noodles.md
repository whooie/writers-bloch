# Scallion Oil Noodles

From [Wok & Kin][link]

## Ingredients
- 500 mL oil
- 2 slices of ginger
- 300 g scallion (~1 bundle)
- 50 g red shallot
- 4 tbsp light soy sauce
- 3 tbsp dark soy sauce
- 2 tsp sugar
- noodles

Optional additional aromatics to try:
- Star anise
- Coriander roots and/or seeds
- Bay leaves
- Allspice

## Method
For the oil:
1. Cut the scallions into 10 cm segments. Separate the whites from the greens.
1. Use a cleaver to gently slap the scallion whites.
1. Slice the shallots.
1. Pour oil into a wok or pan and add the scallion whites, shallots, and ginger
in. Brink the heat up to medium.
1. Let the aromatics gently fry for 3 minutes or until golden, then turn off the
heat and use chopsticks to remove them from the oil and discard.
1. Add the scallion greens. Fry for 3 minutes or until they start to change
color, then use chopsticks to remove them from the pan. These can be saved for
garnish.
1. Transfer the oil to a heatproof jar or container. Use it for any future
savory recipes.

For the soy sauce:
1. Combine light and dark soy sauce and sugar in a pan and turn the heat to
medium-low. Let simmer for 20 seconds, then transfer to a heatproof jar or
container.

Assembly:
1. Add 2 tbsp of the scallion oil and 1.5 tbsp of the soy sauce to a bowl.
1. Add noodles to the bowl and stir until the sauce and oil evenly coat the
noodles.
1. Garnish with fried or fresh scallions, or lightly fried greens or mushrooms.

[link]: https://www.wokandkin.com/scallion-oil-noodles/

