# Sourdough English Muffins

## Ingredients
Levain:
- Flour (100% / 238%)
- Sourdough (42% / 100%)
- Milk (106% / 258%)

Dough:
- Flour (100% / 11.6%)
- Levain (861% / 100%)
- Salt (6% / 0.697%)
- Baking soda (6.4% / 0.743%)
- Honey (14% / 1.63%)

## Method
1. Mix levain, rest for 8 hours.
1. Add final dough ingredients to levain, mix until smooth, only slightly
   sticky.
1. Flour hands and work surface well, roll or pat the dough out to 1/2 in.
   thick. Cut dough into 3 in. circles and place onto semolina-dusted parchment
   paper. Proof for 45-60 min. covered.
1. Oil or butter (lightly) a griddle, medium-low heat. Cook for a total of 7-8
   min. each side, until sides are browned and firm. Flip every couple minutes
   for the first few minutes.
1. Cool on wire rack. Split with fork.

