# Split Pea Soup with Ham

Adapted from Amanda Biddle's "Striped Spatula"

## Ingredients
- 1 large onion
- 1-ish clove garlic
- 1.5 lbs. dried split peas
- 2-ish smoked ham hocks
- 1-2 ham steaks, depending on how much meat comes from the hocks
- 2/3-ish lb. bacon
- 1 large bay leaf
- 2 tsp. fresh thyme leaves
- 6 c. chicken stock
- 2 c. water

## Method
1. Dice the onion and mince the garlic. In a strainer, rinse the split peas and
   remove any shriveled or otherwise unshapely bits. Slice the bacon strips into
   smaller sections according to preference.
1. In a large pot or Dutch oven, cook the bacon over low heat to render fat.
   Remove once done.
1. Add the onion and garlic, and cook over medium heat until enough liquid has
   been released from the onions to scrape all the fond from the bacon off the
   bottom of the pot. Use a bit of chicken stock to deglaze if the fond starts
   to burn.
1. Nestle the ham steak among the onion. Optionally add the bacon back in (the
   bacon may be used as garnish later). Add 1 tsp. of thyme and bay leaf, then
   the hocks, split peas, stock, and water. Bring to a boil, then reduce heat
   and summer uncovered, stirring occasionally, until the split peas are cooked
   down and the soup reaches the desired consistency (60-90 min.).
1. When the soup reaches the desired endpoint, shred the ham steaks and hocks in
   a separate bowl, then add back to the soup. Remove the bay leaf, add the
   remaining thyme, and season to taste. Serve with bacon (if previously
   reserved), croutons, and cracked black pepper.

