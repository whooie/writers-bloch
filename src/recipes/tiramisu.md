# Tiramisu

From Joshua Weissman

## Ingredients
Ladyfingers:
- 3 eggs
- 1/2 c. granulated sugar
- 1 tsp. vanilla extract
- 1 c. AP flour
- powdered sugar

Coffee dunking mixture:
- 2 c. strong-flavored coffee
- 1/4 c. granulated sugar
- 3 tbsp. rum/brandy
- a bit of maple syrup

Cake filling:
- 6-7 egg yolks, depending on size
- 3/4 c. granulated sugar
- 2 c. mascarpone
- 1 c. heavy cream

Miscellaneous:
- cocoa powder
- dark chocolate

## Method
Ladyfingers:
1. In a large bowl, combine eggs and sugar, and whisk constantly over a pot of
    simmering water until the mixture reaches 160F (approximately less than 5
    minutes).
1. Beat the mixture (high speed if using an electrical implement) until expanded
    to ~2.5 times its original volume and holds soft peaks.
1. Beat in the vanilla extract, and gently fold in the flour.
1. Transfer to a piping bag, and pipe onto a baking sheet lined with parchment
    paper, forming ~3 inch-long, ~1 inch-wide strips, with 1 inch of separation
    between each finger (should yield around 30 fingers).
1. Just before baking, generously dust the entire assembly with powdered sugar,
    then bake at 350F for 8-12 minutes or until lightly golden and crisp.

Cake filling:
1. In a bowl, mix egg yolks and sugar, and whisk constantly over a pot of
    simmering water until the sugar is mostly incorporated.
1. Beat the mixture (high speed if using an electrical implement) until expanded
    to ~2.5 times its original volume and holds very soft peaks. Remove from
    heat.
1. In a separate bowl, beat the mascarpone until smooth, then gently fold into
    beaten egg mixture.
1. Beat the creamy until it holds medium peaks before gently folding in the
    egg-mascarpone mixture.

Assembly:
1. Whisk together all the ingredients for the coffee mixture and submerge the
    ladyfingers long enough that they absorb a significant amount, but not so
    long that they start to fall apart.
1. Lay the dunked ladyfingers in one layer on the bottom of the vessel for the
    cake (the vessel does not need any lining), and add half the filling. Repeat
    for a second layer of ladyfingers and the rest of the filling.
1. Cover the vessel with plastic wrap, making sure that none of the wrap
    contacts the surface of the filling and chill in the refrigerator overnight.
1. Right before serving, dust the top with cocoa powder, slice, and grate the
    chocolate over it.

