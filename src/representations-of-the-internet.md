# Representations of computers and the internet

- https://www.youtube.com/watch?v=uFfq2zblGXw
- Binging through Every Frame a Painting -- how to visually depict the internet
  and computers? I would guess that, to first order, the principal factor in
  determining what style would look good to a person is how well they appreciate
  what's going on behind the scenes, which is what makes this an interesting
  problem. To some people, the internet is indistinguishable from computers and
  phones -- everything is a button to click or press in a window that just leads
  spawns other windows and buttons. But to others, the internet is more about
  the flow and quantity of information, and whatever's on screen is totally
  trivial

