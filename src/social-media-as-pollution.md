# The optimal amount of social media is not zero

- Social media interactions should be thought of as a kind of pollution on the
  collective mental behavior/health of humans. But like real pollution, the
  optimal amount of this is not zero.

