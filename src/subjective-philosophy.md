# Philosophies relating to subjective experiences are stupid

- A lot of philosophies are like enormous implication trees -- all branches
  depend on certain "facts" being true to fully realize which implications are
  real. But a lot of what goes into determining the "facts" are ginormous
  scientific questions, like "is the universe deterministic" in the question of
  free will, so it seems highly likely that the root nodes of the trees
  themselves will never be determined and the whole tree is a bit of a waste.
- Philosophies that deal with subjective experiences of things (e.g. how we
  "know" that an object can persist as the same object through time, basically
  all of theory of mind) are total bunk -- everything is filtered through the
  lens of our experience of it, so until there is a first-principles explanation
  of how the brain translates experiences into thought (and what thought even
  is), subjective experiences need to be taken as atomic in the aggregate

