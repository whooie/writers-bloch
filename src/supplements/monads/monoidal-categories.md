# Monoids and monoidal categories

In the [main post][main], we talked about groups and monoids in the context of
functors and what it means to have a monoid in a category of endofunctions. But
the particular interpretation of a monoid that we used for endofunctors is kind
of a specific case of a more general pattern in categories. The core elements
are still the same, but certain parts of what we said for endofunctors can be
generalized using the notion of an aptly named "monoidal" category, which we'll
talk about here.

A monoidal category is characterized by a particular functor, which is the
"tensor" product $\otimes$. The (categorical) tensor product isn't an
endofunctor -- rather than mapping directly from a category to itself, it
instead maps two things (objects or morphism) from a category back to a single
object or morphism in the category -- but it'll get the job done. It also isn't
really related directly to tensors. Instead, it captures the idea of what that
kind of product does, which is to say that it combines things together. Just
like the linear-algebraic tensor product of two vectors in (possibly) different
vector spaces produces a vector in their joint space, the categorical tensor
product takes two objects (or morphisms, considering its status as a functor),
and joins them together into a single object or morphism that exists within the
same category. In other terms, for some category $\mathcal{C}$,
$$
\otimes \colon \mathcal{C} \times \mathcal{C} \to \mathcal{C}
.$$
$\otimes$ must also have some object $I \in \text{Ob}(\mathcal{C})$ that acts as
a "identity" or "unit" under $\otimes$, which is to say that it effectively does
nothing.

What does "doing nothing" mean here? The precise way to say it is that these two
commutative diagrams must be satisfied:

<div style="text-align: center; width: 80%; margin: auto auto;">
    <img
        src="../../assets/monads/monoidal-pentagon.svg"
        alt="monoidal pentagon"
        style="width: 100%"
    ></img>
</div>

<div style="text-align: center; width: 80%; margin: auto auto;">
    <img
        src="../../assets/monads/monoidal-triangle.svg"
        alt="monoidal triangle"
        style="width: 100%"
    ></img>
</div>

Intuitively, we say that $\alpha$ is an "associator" natural isomorphism -- its
only job is to shuffle around the order in which $\otimes$ is applied (note the
parentheses) while acting trivially on the objects themselves. $\lambda$ and
$\rho$ (for *left* and *right*) are "unitor" natural isomorphisms that act on a
monoidal product of an object with the unit, retuning the product back to the
ordinary object -- kind of like "dividing" by the unit. Requiring that the
diagrams commute imposes the condition that $\otimes$ is associative (order of
application doesn't matter), and that the tensor product of an object with the
unit actually is trivial.

If $\otimes$ and $I$ can be defined for some category with the above properties,
then the category together with $\otimes$ and $I$ is called "monoidal." In the
category of natural numbers, multiplication can be viewed as a tensor product,
with $1$ being the unit; in **Vect**, the categorical tensor product and
linear-algebraic tensor product coincide, with any one-dimensional vector space
being acceptable as a unit. To help disambiguate in the general case, $\otimes$
is often just called the "monoidal product." Monoidal categories are relevant
to, for example, the categorical study of physics. Categorical quantum mechanics
is the textbook case, where $\otimes$ describes how to compose physical systems
(e.g. describing the dynamics of two particles in terms of those for both
particles individually). The constraints encoded by the commutative diagrams
above then align with the intuition that the laws of physics be principally
invariant with respect to the number and order of the physical systems being
combined.

So then when it comes to monoids, recall that we have a single object in some
category with potentially many endomorphisms--morphisms that go from this object
back to itself, without necessarily being the identity.

<div style="text-align: center; width: 50%; margin: auto auto;">
    <img
        src="../../assets/monads/group.svg"
        alt="group cateogry"
        style="width: 100%"
    ></img>
</div>

For monads, the object $\bullet$ is an endofunctor $M$ that is constrained with
two natural transformations $\mu \colon (M \circ M) \to M$ and $\eta \colon
1_\mathcal{C} \to M$, with $\mathcal{C}$ being the fixed category. For general
monoids, we only want to think about $\bullet$ as a plain object in some
category. The question is, what do $\mu$ and $\eta$ "back-translate" to in this
picture?

If $\bullet$ is an object, then it makes sense for $\mu$ and $\eta$ to be
morphisms. Additionally, $\mu$ should point from the combination of $\bullet$
with itself back to $\bullet$, and $\eta$ should point from some kind of unital
object to $\bullet$. Given the discussion of monoidal categories above, you can
probably guess that the "combination" and "unital object" correspond to the
monoidal product $\otimes$ and the monoidal unit $I$. Thus we have
- $\mu \colon \bullet \otimes\, \bullet \to \bullet$
- $\eta \colon I \to \bullet$

Then, the coherence conditions for this monoid are back-translated to two
commuting diagrams,
<div style="text-align: center; width: 80%; margin: auto auto;">
    <img
        src="../../assets/monads/monoid-pentagon.svg"
        alt="monoidal pentagon"
        style="width: 100%"
    ></img>
</div>

<div style="text-align: center; width: 80%; margin: auto auto;">
    <img
        src="../../assets/monads/monoidal-unitor.svg"
        alt="monoidal triangle"
        style="width: 100%"
    ></img>
</div>


[main]: ../../monads.md#monads

