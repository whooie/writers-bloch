# The system

- choose your systems wisely
- the universe is (as far as we know) a closed system
    - everything is conserved
    - musical analogy -> that thing soloists do when they hang onto certain
      beats, but have to speed up later
    - like you have a bunch of stuff in a closed room: you can move stuff around
      into piles, but can't ever get stuff to leave the room
- term definitions are conserved as well
    - so choose your definitions wisely
      https://slatestarcodex.com/2018/07/18/the-whole-city-is-center/

