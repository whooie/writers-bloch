# The $[0, 1]$ Interval

- games of telephone apply sigmoid functions and extreme-ize the space of
  ratings
- people need to apply an inverse sigmoid to combat this
- further, develop a more mathematical/quantitative view of ratings and
  transformations that can be applied to the $[0,1]$ (or $[-1,1]$) interval
- play into the meta-perspective
