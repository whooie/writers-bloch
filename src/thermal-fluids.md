# Thermal fluids

- People think of the American melting pot as an assimilation machine that
  produces a homogeneous mix of cultures, but it's more like various immiscible
  fluids connected to a thermal bath. The bath produces a baseline amount of
  fluctuation over time that creates interesting interactions at the interfaces.

