# The present state of things must be reckoned with

- "you're not bad at math, the American schooling system just sucks"
- "this piece of art isn't bad, you're just not its target audience"
- statements like these suck even if they're correct because things are still
  bad at the end of the day -- they're usually said at a time past the point
  where something can actually be done to rectify it
- everything bottoms out at physical reality
    - should jaywalking be illegal? probably not, but only fools expect laws and
      fines to keep their bones from breaking

