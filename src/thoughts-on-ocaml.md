# Thoughts on OCaml and 'light' programming

## Contents
<!-- toc -->

I'm sure many people already know this on a very intuitive level, but just to
set the context for this post, programming is treated pretty differently in the
sciences than it is by CS people or even just people with software jobs. Of
course, this makes total sense: Unless it's a computational science, workflows
are much different and there's little to no incentive to write elegant,
efficient, maintainable, or even really just readable code because most of it is
treated as a one-off thing used to get a computer to spit out some numbers or
produce a plot. I think it is mostly for this reason that Python is actually a
great language for (at least the experimental) sciences -- it's expressive, easy
to learn, and (with the right third-party libraries) is often fast enough for
anything you'd normally want a computer to do. Up to the various distributions
of Python, it's also very portable in a certain sense, and you don't have to
mess around with a build system -- it's a scripting language with powerful
friends.

If you're like me, though, and you've taken even a small peak at what other
languages have to offer, Python really just ceases to be enough. Every time I
have to come back to Python at work, I'm assailed by reminders of all the simple
yet powerful things that Python lacks. I miss real, enforced type systems,
proper lexical scoping, some equivalent to traits/typeclasses, and, most of all,
sum types. And yet still it's hard not to appreciate how easy it is to prototype
things in Python and quickly bang out a script to fit some data or plot a few
lines. (It's worth noting that Mathematica is also relatively a nice choice for
this sort of stuff, but it's an overall different approach that costs money and
I hate notebooks, so let's not get into that.)

I guess it would be true to say that I have a love-hate relationship with
Python, but honestly such a thing is *so passé*. (I mean really, is there anyone
doing anything serious who doesn't?) Instead, I'd like to talk about OCaml.
OCaml is a general-purpose, high-level, multi-paradigm language in the ML
family. The name comes from being an **O**bjective extension of Caml, which is
purely functional. After getting frustrated with Python I started looking for a
replacement in my usual workflow. I thought I'd try a functional language since
those seem to be the ones most likely to have sum types and currying seems to be
something that could make a lot of my small numerical programs a lot nicer.
OCaml seemed to fit the requirements above and additionally looked like a kind
of get-stuff-done version of Haskell because it's less strict about things like
random insertions of statefulness and side effects.

So anyway, here's a loosely organized list of things I like and dislike about
OCaml after playing around with it for a bit and doing a bit less than half of
Advent of Code 2023.

> *Disclaimer: I haven't written anything very "non-trivial" in OCaml, but the
> complaints I have below aren't things that would likely change with further
> use.*

## Practical, functional programming as the default
Let's start with a few reasons why you'd want to use OCaml instead of Python (or
Haskell) in the first place. The biggest reason, by far, is the strict typing. I
can never seem to explain it properly to people, but I think there's something
that's just so *vulgar* about how types are treated in Python. Yes, yes, it's
all duck typing, and I agree there's something nice about the inherent notion
that objects are defined by how they behave and magic methods, and how it's all
just pseudocode, and basically everything that's not a function is a dictionary
in some form or another...

But why, then, is there even a notion of a "type" in the language to begin with?
The obvious answer is that `float`s are different from `int`s, both of them are
different from `str`s, and all of them are different from `list` -- *obviously*,
the data can come in many different forms, and the distinction between them
really does mean something. But that distinction is explicitly and deliberately
thrown out the window in Python. Let's look at this simple function:
```python
def linear_combo(a, x, b, y):
    """
    Computes the linear combination `a * x + b * y`.
    """
    return a * x + b * y
```
We can see that the body of the function does exactly what the docstring
advertises, so for instance `linear_combo(0.5, 2, 0.3, 8)` will produce nice
output that agrees with intuition and the clear intent of the function, which is
indeed to produce a linear combination. With duck typing, we could even make `x`
and `y` vectors with suitable definitions of `__mul__` and we'd still get a nice
linear combination.

But we could also have `linear_combo(5, "a", 6, "b")`. This would produce the
string `"aaaaabbbbbb"`, which is *not at all* a linear combination because
addition and multiplication here have completely different meanings from what's
relevant to the function; it's only by pure happenstance that `__mul__` and
`__add__` are even defined for strings in this way that even allows the code to
run. Insofar as Python programmers want to adhere to ["explicit is better than
implicit" and "readability counts"][python-zen], Python should provide some way
to invalidate even the notion that `linear_combo(5, "a", 6, "b")` is something
that makes sense. Yes, you can add type annotations and do `isinstance` checks,
but at the end of the day, there's still nothing in the Python language to
really prevent this function call from occurring. And even if you made the
function raise an exception on invalid types, that error would be caught at
runtime, perhaps hours into the program's execution{{footnote:
    Yes, there exist type checkers for Python that have recently gotten pretty
    fast, but to me they don't fall in the Python namespace because they're
    clearly distinct entities from the Python interpreter, and still don't
    actually prevent you from executing type-invalid programs.
}}.

The bottom line is that a large portion of the real *meaning* of a program is
encoded in its typing. That is, what a program is actually doing on a conceptual
level is encoded in the types of things it operates on. `linear_combo(a: float,
x: Vector, b: float, y: Vector)` means something very different from
`linear_combo(a: int, x: str, b: int, y: str)`, and when a language allows these
two meanings to be conflated in a nonsensical type system, who can really be
sure what's going on?

Enter OCaml. In addition to having a real type system, OCaml also has sum types.
If you've never used a sum type before, you're really missing out. Here's what
it looks like in OCaml:
```ocaml
(* This is a basic sum type. Any value of type `sum` can be one of these
   variants, which can each carry around data. *)
type sum =
  | A                   (* a "unit" variant, basically just a token *)
  | B of int            (* this carries around an integer *)
  | C of string * float (* this carries a (string, float) tuple *)
  | D of sum            (* types can even contain themselves! *)
  (* ... *)
```
Here, `sum` is the name of the type and each of the labels placed after the `|`
is a constructor for the type: Those (and only those) constructors are the only
way to make a value of type `sum`, and the variants carrying types (denoted by
the `of`) can only be constructed when additional values of those types are
provided. Once constructed, they can be analyzed using `match`:
```ocaml
let sum_value = C ("hello", 5.0) in
match sum_value with
| A        -> "encountered A"
| B n      -> "encountered B with an integer"
| C (s, x) -> "encountered C with a string and a float"
| D v      -> "encountered D with another value"
```
There's a lot to be said for sum types, especially in complicated theoretical
contexts. The most practical reason they're great is because they can model
certain kinds of data really well. Any time there's a finite collection of
things you need to discriminate between, especially when there's additional data
attached -- bam, sum type.

And here are two very common use cases:
```ocaml
(* `option` models a value that can be nullified, since `None` can be used to
   construct the type with no value. *)
type 'a option =
  | None
  | Some of 'a

(* `result` models the endpoint of a fallible processes, returning either a
   successful result or an error with some accompanying data. *)
type ('a, 'e) result
  | Ok of 'a
  | Error of 'e
```
Here, all the `'a` and `'e` bits are type variables, which stand in for concrete
types like `int` and `string`. Having generics in a strictly typed language is a
very powerful feature because it allows operations to be defined in a general
way while still maintaining the exactness of a real type system. For example:
```ocaml
let option_map (f: 'a -> 'b) (opt: 'a option) : 'b option =
  match opt with
  | None   -> None
  | Some x -> Some (f x)
```
This defines a function, `option_map` that takes two arguments. The first is a
function `f` and the second is an option `opt` that could hold a value of some
type `'a`. `f` is a function taking exactly one `'a` as argument and returning
some other type `'b`. `option_map` then applies `f` in the case that `opt` is
`Some`, and does nothing otherwise, to return a final value of type `'b option`.
Here, the specific relationships between input and output types is expressed
while still leaving things free, such that `option_map` can then be used in a
why variety of contexts, no matter what `'a` and `'b` are in each instance.

This kind of thing is impossible in Python, not only because it doesn't have a
real type system, but also because it doesn't have sum types. But even supposing
it did have sum types, the 

---

- there's a real type system with generics and sum types
- the way that constructors are treated is awesome -- matching with `::` is
  fantastic, even if it's fairly superficial
- currying and pipeable operations are great when you're dealing with very
  mathematical relationships
- shallow state management and the ability to discard values is nice for
  print-debugging (cf. Haskell `IO`)

## Modules as objects are great, especially in FP

---
- being able to freely declare multiple modules in a single file encourages
  namespace management and helps to prevent namespace explosions due to there
  being only functions
    - namespace management is an underrated feature of OO languages
- `Module.t` seems stupid at first, but does a great job of communicating how
  OCaml wants you to think about modules
    - modules are basically what you get if you took an OO class and removed
      `self`

## Functors and polymorphism

---
- functors are in principle a great idea as an alternative to
  traits/typeclasses, but it's seriously annoying that you can only require
  module signatures on the per-module level
- there needs to be some way to bound your polymorphism on the per-function
  level, see `Result.map_err`

## opam/dune/mli

---
- opam is annoying, but better than pip because it manages switches
- the dune lisp is annoying -- just use a normal config format
    - also why isn't the directory structure automatically mapped onto the
      module structure?? Why is `#copy_files` needed in the dune file if you
      want to organize your code??
- I hate separating interface definitions out into separate files. It's
  incredibly inconvenient and there needs to just be a `pub` keyword
- package vendoring should be the standard, cf. cargo and stack

## Awkwardness in the standard library

---
- `compare` using integers
- `=` and `==` and automatic implementation of comparisons/equality
- mixing exceptions and `result`
- `Str` regexes are terrible
- if people feel like they need "alternate" standard libraries, you're doing it
  wrong

## Gripes

---
- ML-syntax often turns into word soup -- a bit of syntax is important to help
  you quickly orient yourself in a block of code and help it all flow better,
  sort of like serifs in typefaces
- I appreciate the dedication, but `+.` and friends is stupid
    - OCaml supports a way to make `+` polymorphic, but actually doing so would
      require writing e.g. `Float.(+)`, which illustrates the problem with
      functors and the lack of per-function constrained polymorphism
- "backwards" type notation
- too much reliance on the type inference system -- the language is still
  strictly typed, so explicitly annotating types is annoying if you have to
  enclose function arguments in parentheses
- function arguments need names -- it's a missed opportunity to rely on
  docstrings because function signatures can be very descriptive on their own
  (in a way that's integrated into the language itself!) and to that end, only
  having type signatures is clearly not enough information when nearly every
  docstring in the std docs start with "`<signature>` is..."
    - this is also a problem with e.g. Haskell
- annotating the type of a record is awkward, e.g
  `{ Complex.re = 1.0; im = 1.0 }`
- you have to use exceptions instead of `break`/`continue`

## The good

---
- optional/labeled arguments
- modules
- explicit, context-free syntax for type variables -- `val foo : 'a -> 'b` is
  better than `foo :: a -> b` and `fn foo<A, B>(a: A) -> B`
- explicit `rec` keyword
- `Module.( ... )` isn't often used, but it's great when you need it
- pipe operator is nice
- if exceptions are really needed, it's nice that you have to explicitly declare
  them
- REPL integration is great

## Conclusion

---

```ocaml
module type Monad = sig
  type 'a t

  val return : 'a -> 'a t

  val (>>=) : 'a t -> ('a -> 'b t) -> 'b t

  val (>>) : 'a t -> 'b t -> 'b t
end
```


- opam is annoying, but still a bit better than pip (should try dune
  vendoring/opam monorepo); have to use `opam env` to get the "right"
  environment variables, requiring you to append to `$PATH` and edit your
  `bashrc` -- if you're using something different (like nushell), this is more
  annoying
- compiler error messages should be more descriptive (see rustc)
- dune is cool, but requires too much book-keeping in a lisp rather than a
  standard config format; the lisp is annoying because it has to allow for
  description of weird build-time procedures/relationships that I will likely
  never use (also, apparently you have to use `#copy_files` if you want some of
  your code to live in a sub-directory???)
- pattern matching with operators (e.g. `head :: tail`) is fantastic
- syntax quirks requiring `begin ... end` like nested match statements
  ```ocaml
  match x with
  | y0 ->
      match y with
      | z -> ...
  | y1 -> ...
  ```
- types with `Module.t` is kinda weird
- standard library reliance on exceptions when there's already a `result` type
- `Str` regex functions are weirdly super state-ful
- definition order matters, which sucks
- docstrings and mli files are annoying
- kinda-not-really traits -- there's no sane way to require a generic type to
  satisfy a module signature from within a single functor (e.g.
  `Result.map_err`) -- this is the whole point of Rust `where`
- no unsigned integers for standard use
  [link](https://github.com/ocaml/ocaml/pull/1201)
- dumb `compare` convention is maintained in stdlib code when there should just
  be something like Rust `cmp::Ordering`
- Functors are interesting, but Rust generics with trait bounds accomplish the
  same goal with way better flexibility and ergonomics

[python-zen]: https://peps.python.org/pep-0020/

