# Unrealized degrees of freedom

- There many unrealized degrees of freedom in deciding what to believe or trying
  to figure out what other people believe. If you cannot tell what someone else
  "actually believes" (for whatever that could mean), then who cares? Why should
  it affect you?
- So-called main character syndome and the breakdown of how people conceptualize
  other people.
- Inability to realize when a conclusion can't be reached from a set of data,
  triggering substitution of projected information/perspectives

