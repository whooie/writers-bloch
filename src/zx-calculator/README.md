# Making a calculator for the ZX-calculus

I've spent some time writing a small library for performing calculations in the
ZX-calculus, and here are some notes on the subject. If you're already familiar
with the ZX-calculus, you can probably skip the intro, otherwise keep reading!

## Contents
- [Introduction to the ZX-calculus](intro.md)
- [Naive ket-bra calculator](naive.md)
- [Proper graph-based implementation](graph.md)

