# Introduction to the ZX-calculus

## Contents
<!-- toc -->

## Introduction

The [ZX-calculus][zx.com] is pretty cool. If you aren't familiar, it's a
graphical language for describing the kinds of linear maps we generally apply to
two-level quantum systems and possibly large compositions thereof. Specifically,
it transforms reasoning about systems of qubits that could be involved in a
large circuit from unwieldy products distributed over large sums to something
more like pushing beads around on pieces of string.

The ZX-calculus was created as a result from categorical quantum mechanics by
[Bob Coecke and Ross Duncan][coecke-duncan] and works by encoding a basic set of
linear maps as simple shapes that can be connected with each other to form large
networks representing more complicated maps, in this case giving the overall
operation of a quantum circuit. The ZX-calculus is a specific example of
[Penrose graphical notation][penrose] and, due to the various properties of the
objects involved, is equipped with a set of rules by which a network can be
rewritten and simplified. Using these rules, calculations about a given circuit
can be done entirely within the graphical language.

Here's a small example. On the left, we have a conventional quantum circuit
diagram showing the procedure for teleporting the quantum state $\ket{\psi}$
from Alice to Bob, and on the right, we have the equivalent diagram in the
ZX-calculus. Both notations have conventions for then translating to matrices
describing the complete map from start to finish, but only the ZX-diagram allows
one to work entirely within the diagram.

![image](../assets/zx-calculus/zx-teleportation.png)

This post will be a small introduction to the basics of the ZX-calculus to lay a
foundation for other posts to come. Here I'll assume knowledge of basic
undergrad quantum mechanics -- quantum states, ket-bra notation, operators and
such -- and apply it to the special case of qubits and the usual set of
operations we usually think of when talking about basic quantum computing.

## Review: quantum mechanics
If you're already familiar with ket-bra notation, qubits, and quantum gates, you
can [skip ahead](#the-zx-calculus).

### States
First let's back up a bit for a quick refresher on quantum mechanics. Linear
algebra is the main mathematical language through which pretty much everything
about this particular flavor of quantum mechanics{{footnote:
    As opposed to the path integral formulation or categorical quantum
    mechanics.
}} is understood. Here, quantum states are vectors in a complex Hilbert space --
that is, a vector space with a scalar product satisfying some conditions that
allows it to act generally as a measure of "close-ness" between two vectors.
These vectors are acted upon by linear maps back to the same Hilbert space,
which are known as operators. If you're familiar with vectors and matrices as 1D
and 2D arrays of numbers, you can think of states as column vectors and
operators as matrices. It's worth noting that all scalars are allowed to be
complex-valued for physical reasons related to wave mechanics.

When it comes to quantum computing, we pretty much always reduce to a simple
two-level system with basis states $\ket{0}$ and $\ket{1}$. In other words, the
qubit. A general qubit state is therefore described as
$$
\ket{\psi} = a \ket{0} + b \ket{1}
$$
where $a, b \in \setC$. The physical interpretation is that the absolute squares
of these components $|a|^2$ and $|b|^2$ are the probabilities of measuring the
qubit to be in the corresponding state, at which point the qubit is said to have
"collapsed" completely into that state. Being probabilities, then, we also
require that $|a|^2 + |b|^2 = 1$ and note that, as complex amplitudes associated
with wave components, $a$ and $b$ in themselves encode not only these
probabilities but also the *phases* of these components that disappear upon
measurement. These phases can be made apparent by rewriting them in polar form,
$$
\ket{\psi} = |a| e^{\I \pphi_a} \ket{0} + |b| e^{\I \pphi_b} \ket{1}
$$
where $\pphi_a$ and $\pphi_b$ are these phases.

If we have another quantum state $\ket{\psi'} = c \ket{0} + d \ket{1}$, then we
can take the inner product (or "braket") $\bra{\psi'} \cdot \ket{\psi} \equiv
\braket{\psi'|\psi}$, where
$$
\bra{\psi'} = c^* \bra{0} + d^* \bra{1}
.$$
Taking $\ket{0}$ and $\ket{1}$ to be orthonormal, we have $\braket{\psi'|\psi} =
c^* a + d^* b$. The absolute square of this inner product is interpreted as the
probability that one measures $\ket{\psi}$ as being in $\ket{\psi'}$ (or
vice-versa).

It is often helpful to visualize quantum states (of two-level systems) using
something called the [Bloch sphere][bloch-sphere]. Specifically, we can notice
that, naively, for a general superposition of two basis states there are a total
of four degrees of freedom -- two for each complex coefficient of the
superposition. But as discussed above, we have a "normalization" constraint that
$|a|^2 + |b|^2 = 1$, so that removes one degree of freedom. Additionally, we can
rewrite the form of the superposition by pulling out a global phase $\phi$
$$
\ket{\psi} = e^{\I \phi} \Big(|a| \ket{0} + |b| e^{\I \pphi} \ket{1}\Big)
.$$
For physical reasons, this global phase is "unobservable" and has no bearing on
the outcome of any possible experiment. (Note that the relative phase $\pphi$
still matters, though.) This leaves only two remaining degrees of freedom, and
since we maintain the normalization constraint, we can also rewrite $|a|$ and
$|b|$ as $\cos(\theta / 2)$ and $\sin(\theta / 2)$ where $0 \leq \theta \leq
\pi$. Having now recast both degrees of freedom as angles, we've just mapped all
possible states to points on a unit sphere in $\setR^3$:

<div style="text-align: center; width: 100%; margin: auto auto;">
<img src="../assets/zx-calculus/bloch-sphere.png" alt="image"></img>
</div>

with
$$
\ket{\psi}
    = \cos\left(\frac{\theta}{2}\right) \ket{0}
    + e^{\I \pphi} \sin\left(\frac{\theta}{2}\right) \ket{1}
$$

In this picture, the polar angle $\theta$ encodes the relative proportions of
the superposition between $\ket{0}$ and $\ket{1}$, and the azimuthal angle
$\pphi$ is directly the relative phase. Here we've also defined an additional
two states
$$
\ket{\pm} = \frac{1}{\sqrt{2}} \Big( \ket{0} \pm \ket{1} \Big)
,$$
which form an alternative basis for the qubit space. The reason for this will be
discussed shortly. These two bases are usually called the Z and X bases,
according to where the states lie on the Bloch sphere.

### Operators
Operators are linear maps that can be thought of as matrices acting from the
left on kets (or from the right on bras). Sometimes they're written as actual
matrices, but that sort of representation would rely on a particular choice of
basis. Instead they're often treated more abstractly, not with explicit
reference to any single component of the equivalent matrix, but rather by their
action on each basis vector. If we're working with orthogonal bases, specifying
the action of an operator on each element of the basis is enough to define the
action of the operator over the whole vector space.

To make this a bit more concrete, we could translate a generic $a, b, c, d$
matrix representation of an operator $A$ to this more abstract (and ultimately
more general in form) definition as
$$
\begin{pmatrix} a & b \\ c & d \end{pmatrix}
    \quad\leadsto\quad
        \begin{matrix}
            A \ket{0} \equiv a \ket{0} + c \ket{1}
            \\
            A \ket{1} \equiv b \ket{0} + d \ket{1}
        \end{matrix}
.$$
If we really had to, we could then make reference to specific elements by taking
inner products through $A$:
$$
\begin{aligned}
    \bra{0} A \ket{0} &= a
    &
    \bra{0} A \ket{1} &= b
    \\
    \bra{1} A \ket{0} &= c
    &
    \bra{1} A \ket{1} &= d
.\end{aligned}
$$

But perhaps a less verbose way to define an operator in a way that is still
independent of a particular choice of basis is by using "ketbras". Like the
name suggests, this object is something that looks like
$$
\ketbra{\psi}{\phi}
.$$
But what is a ketbra? One way to see it (in a *very* non-rigorous way), is as a
a kind of opposite of the inner product. In an inner product, we reduce the
dimension of the arrays equivalent to the states being multiplied from one to
zero (we go from 1D arrays of numbers down to a 0D array, otherwise known as a
scalar). In the same flavor, we might expect that a ketbra *increases* the
dimension of the equivalent arrays from one to two, meaning that a ketbra
corresponds to a full 2D matrix.

More formally, a ketbra is understood as the "outer product" between two state
vectors. One nice feature of this product is that it distributes over sums in
the same way that an inner product does. If we have two states $\ket{\psi} = a_0
\ket{0} + a_1 \ket{1}$ and $\ket{\phi} = b_0 \ket{0} + b_1 \ket{1}$, then their
outer product is
$$
\begin{aligned}
    \ketbra{\psi}{\phi}
        &= \Big( a_0 \ket{0} + a_1 \ket{1} \Big)
            \Big( b_0^* \bra{0} + b_1^* \bra{1} \Big)
        \\
        &= a_0 b_0^* \ketbra{0}{0} + a_0 b_1^* \ketbra{0}{1}
        + a_1 b_0^* \ketbra{1}{0} + a_1 b_1^* \ketbra{1}{1}
.\end{aligned}
$$
Relating back to linear algebra, here's one final perspective: if we have two
elements $\ket{i}$ and $\ket{j}$ of an orthonormal basis of a vector space $V$,
then $\ketbra{i}{j}$ is an element of an orthonormal basis of the vector space
$V \times V^*$ (where $V^*$ is the point-wise conjugate space of $V$).

One nice (and, in my opinion, understated) feature of this notation is that
working through the action of an operator on a state using a ketbra definition
is then *exactly what it looks like*: If we have some operator
$$
A = a \ketbra{0}{0} + b \ketbra{0}{1} + c \ketbra{1}{0} + d \ketbra{1}{1}
$$
and some state $\ket{\psi} = \alpha \ket{0} + \beta \ket{1}$, then the action of
the operator is computed by distributing the matrix-vector product over the sums
of all components:
$$
\begin{aligned}
    A \ket{\psi}
        &= \Big(
            a \ketbra{0}{0} + b \ketbra{0}{1} + c \ketbra{1}{0} + d \ketbra{1}{1}
        \Big) \Big( \alpha \ket{0} + \beta \ket{1} \Big)
        \\
        &= a \alpha \ketbra{0}{0} \cdot \ket{0}
            + a \beta \ketbra{0}{0} \cdot \ket{1}
        \\
        &\phantom{=}+ b \alpha \ketbra{0}{1} \cdot \ket{0}
            + b \beta \ketbra{0}{1} \cdot \ket{1}
        \\
        &\phantom{=}+ c \alpha \ketbra{1}{0} \cdot \ket{0}
            + c \beta \ketbra{1}{0} \cdot \ket{1}
        \\
        &\phantom{=}+ d \alpha \ketbra{1}{1} \cdot \ket{0}
            + d \beta \ketbra{1}{1} \cdot \ket{1}
        \\
        &= (a \alpha + b \beta) \ket{0} + (c \alpha + d \beta) \ket{1}
\end{aligned}
$$

### Qubits and quantum circuits
When it comes to computing{{footnote:
    It should be said that there exist several other paradigms for quantum
    computing; we'll be working in just the gate-circuit formalism.
}}, we'll also want to note a few more things before getting into the real meat
of this post. Typically, we think of a quantum "circuit" as just a series of
operations applied to some initial state (there's nothing "circuitous" about a
computation). Each of these operations is described as a unitary linear map --
it has to be unitary in order to obey physical conservation laws -- and
individual operations can be composed naively to form a single unitary linear
map that describes the total action of the circuit as a whole. For the purpose
of being able to simulate a circuit on a classical computer, then, it can be
sometimes preferable to compute this single unitary and apply it to an initial
state rather than applying each operation in the circuit one-by-one.

Since all operations must be unitary{{footnote:
    Strictly speaking, there exists a notion of a "mixed" (as opposed to "pure")
    quantum state where some or all of the phase coherence in the state has been
    removed due to e.g. measurement which would require a modification of this
    understanding, but we'll leave it alone for now.
}}, we often think of them as pure rotations of the state vector around
different axes of the Bloch sphere. For example, a rotation of $\alpha$ about
the z-axis of the Bloch sphere is equivalent to changing the relative phase by
$\alpha$, and is described by the operator
$$
Z(\alpha) = \ketbra{0}{0} + e^{\I \alpha} \ketbra{1}{1}
.$$
And if we wanted to change the proportions of the superposition, the equivalent
rotation would be about the x-axis, which is correspondingly
$$
\begin{aligned}
    X(\alpha)
        &= \ketbra{+}{+} + e^{\I \alpha} \ketbra{-}{-}
        \\
        &= e^{\I \alpha / 2} \Big(
            \cos\frac{\alpha}{2} \ketbra{0}{0}
            - \I \sin\frac{\alpha}{2} \ketbra{0}{1}
            - \I \sin\frac{\alpha}{2} \ketbra{1}{0}
            + \cos\frac{\alpha}{2} \ketbra{1}{1}
        \Big)
.\end{aligned}
$$

These two operations are all one needs to fully manipulate the state of a single
qubit, but how would one treat a qubyte? Of course, there's a formal way to do
it, but we won't bother with it. In short, the answer is what's called the
*Kronecker product*, which I'll refer to with the symbol $\otimes$.

Basically, $\otimes$ can be used to combine not only states together, but
operators as well. If we want to stick two qubits $a$ and $b$ together and talk
about the combined system we can write
$$
\ket{\psi_a} \otimes \ket{\psi_b}
.$$
Note that people generally tend to get looser with their notation at this point.
Often you'll also see people putting subscripts on the outsides of the kets, as
in $\ket{\psi}_a \otimes \ket{\psi}_b$, or leaving out the $\otimes$ symbol,
like $\ket{\psi}_a \ket{\psi}_b$, or sticking everything in one ket, like
$\ket{\psi_a \psi_b}$, but it's generally all the same operation.

The Kronecker product is left- and right-associative and distributes over sums,
so if we were to, say, stick two $\ket{+}$ states together and try to write
everything in the $Z$-basis, we'd do it like this:
$$
\begin{aligned}
    \ket{+_a} \otimes \ket{+_b}
        &= \left[
            \frac{1}{\sqrt{2}} \big( \ket{0_a} + \ket{1_a} \big)
        \right]
            \otimes \left[
                \frac{1}{\sqrt{2}} \big( \ket{0_b} + \ket{1_b} \big)
            \right]
        \\
        &= \frac{1}{2} \Big[
            \ket{0_a} \otimes \ket{0_b}
            + \ket{0_a} \otimes \ket{1_b}
            + \ket{1_a} \otimes \ket{0_b}
            + \ket{1_a} \otimes \ket{1_b}
        \Big]
.\end{aligned}
$$
The Kronecker product is somewhat connected to quantum entanglement -- or
rather, the lack thereof. A primitive in elementary quantum information is this
particular family of two-qubit states:
$$
\begin{aligned}
    \ket{\Phi^+}
        &= \frac{1}{\sqrt{2}} \Big( \ket{00} + \ket{11} \Big)
    &
    \ket{\Phi^-}
        &= \frac{1}{\sqrt{2}} \Big( \ket{00} - \ket{11} \Big)
    \\
    \ket{\Psi^+}
        &= \frac{1}{\sqrt{2}} \Big( \ket{01} + \ket{10} \Big)
    &
    \ket{\Psi^-}
        &= \frac{1}{\sqrt{2}} \Big( \ket{01} - \ket{10} \Big)
.\end{aligned}
$$
These four states span the total space of possible two-qubit states, and are
known as Bell states. They're a bit special because they're maximally{{footnote:
    Meaning that if you had a good way to quantify how entangled two quantum
    systems could be (this is actually an outstanding research question for the
    general N-system case), it would be maximized for these four states.
}} entangled states, and it's actually impossible to write them in terms of a
Kronecker product between two single-qubit states. This implies that they're
somehow irreducible, which is interpreted as the entanglement itself containing
some amount of information that wouldn't be there otherwise.

But coming back to the Kronecker product itself, we can also apply it to
operators. It's a bit messy in ketbra notation, but pretty straightforward: all
the kets get combined with other kets, and all the bras get combined with other
bras. But this is actually something that's a bit more intuitive to understand
using matrices. If we have two matrices $A$ and $B$ with
$$
\begin{aligned}
    A
        &= \begin{pmatrix}
            A_{00} & A_{01} & \cdots \\
            A_{10} & A_{11} & \cdots \\
            \vdots & \vdots & \ddots
        \end{pmatrix}
    &
    B
        &= \begin{pmatrix}
            B_{00} & B_{01} & \cdots \\
            B_{10} & B_{11} & \cdots \\
            \vdots & \vdots & \ddots
        \end{pmatrix}
,\end{aligned}
$$
then their Kronecker product has the form of a block matrix,
$$
\begin{aligned}
    A \otimes B
        &= \begin{pmatrix}
            A_{00} B & A_{01} B & \cdots \\
            A_{10} B & A_{11} B & \cdots \\
            \vdots & \vdots & \ddots
        \end{pmatrix}
        \\
        &= \begin{pmatrix}
            A_{00} B_{00} & A_{00} B_{01} & \cdots & A_{01} B_{00} & A_{01} B_{01} & \cdots \\
            A_{00} B_{10} & A_{00} B_{11} & \cdots & A_{01} B_{10} & A_{01} B_{11} & \cdots \\
            \vdots        & \vdots        & \ddots & \vdots        & \vdots        & \ddots \\
            A_{10} B_{00} & A_{10} B_{01} & \cdots & A_{11} B_{00} & A_{11} B_{01} & \cdots \\
            A_{10} B_{10} & A_{10} B_{11} & \cdots & A_{11} B_{10} & A_{11} B_{11} & \cdots \\
            \vdots        & \vdots        & \ddots & \vdots        & \vdots        & \ddots
        \end{pmatrix}
\end{aligned}
$$
(By the way, this understanding of the product is also valid for states if you
think of the states as single-column matrices.) This is particularly useful if
you want to apply some operator to only one subsystem of a product state: all
you have to do is Apply the Kronecker product to the operator you want with as
many identity operators as there are other subsystems. So for example if we had
three qubits described by $\ket{\psi_a \psi_b \psi_c}$ and wanted to apply a Z
rotation to only qubit $b$, we'd write the operator properly as
$$
Z^{(b)}(\alpha)
    = \mathbb{I}_a \otimes Z(\alpha) \otimes \mathbb{I}_c
$$
where $\mathbb{I}_x$ is the identity operator for the $x$ subsystem.

### Circuits as series of gate operations
So now we're fully equipped to talk about quantum circuits. For our purposes, a
quantum circuit is a series of gate operations applied to one or more qubits,
and unlike what we conventionally think of as everyday electrical circuits,
there's no notion of a "closed" or "open" quantum circuit; nothing loops back
around to where it started. Instead, we think of quantum circuits as applying a
series of unitary linear maps to an initial state to produce a corresponding
output state. If, say, we had a circuit comprising three operations $G_1$,
$G_2$, and $G_3$ on an initial state $\ket{\psi}$, then the final state can be
calculated as
$$
G_3 G_2 G_1 \ket{\psi} = (G_3 (G_2 (G_1 \ket{\psi})))
$$
(Note the reversed order of the multiplication!)

One useful thing to note here is that the product (regular matrix-matrix product
or Kronecker product) of any two unitary matrices is also unitary, so every
quantum circuit can ultimately be described using single unitary matrix, up to
any measurements performed during  the circuit. These measurements are
projective and probabilistic -- they collapse a quantum state into a certain
basis state with probability corresponding to the state's amplitudes -- so they
can't be described with unitary (or linear, for that matter) maps, but every
other circuit operation we talk about here will be.

But writing out states and matrices gets cumbersome at a certain scale, so
there's a graphical notation for quantum circuits as well. Here, each qubit is
represented by a wire, and gates (unitary matrices) are represented by things on
the wire. Many gates operate on just a single qubit, but there can also be
operations on multiple qubits at a time or in ways that rotate the product-state
of multiple qubits as a whole.

<figure>
    <img src="../assets/zx-calculus/qc-everything.png" alt="quantum-circuit" style="width=100%">
    <figcaption>
        <i>There. That's every quantum circuit that can or will ever be, up to
        things like mid-circuit measurement.</i>
    </figcaption>
</figure>

But usually it's more useful to define a base set of operations that are then
combined to form larger unitaries. Typically these are picked such that

1. They form a kind of basis for the set of all possible qubit
   operations{{footnote:
        It's a fun exercise to think about what the minimal requirements for
        this would be!
   }}, and
1. They're simple enough that it's not too big of an ask to expect actual, real
   hardware implementations to provide them.

Such a thing is called a universal gate set; one example is the
Hadamard/Phase/CNOT gate set.

A typical circuit using these gates will look something like this:

![quantum-circuit-hpcnot](../assets/zx-calculus/qc-entanglement-detection.png)

With this in mind, let's now look at our gate set.

#### Hadamard

![hadamard](../assets/zx-calculus/qc-hadamard.png)

The **Hadamard** gate is a single-qubit gate that can be written in matrix form
as
$$
\t{H}
    = \frac{1}{\sqrt{2}} \begin{pmatrix}
        1 & 1 \\
        1 & -1
    \end{pmatrix}
$$
or in ketbra notation as
$$
\t{H}
    = \frac{1}{\sqrt{2}} \Big(
        \ketbra{0}{0} + \ketbra{0}{1} + \ketbra{1}{0} - \ketbra{1}{1}
    \Big)
$$
and describes the linear map by which we have
$$
\begin{aligned}
    \ket{0} &\mapsto \ket{+}
    ,\quad&
    \ket{1} &\mapsto \ket{-}
.\end{aligned}
$$
Or alternatively,
$$
\begin{aligned}
    \ket{+} &\mapsto \ket{0}
    ,\quad&
    \ket{-} &\mapsto \ket{1}
.\end{aligned}
$$
The Hadamard gate is one possible square root of the identity operator, in the
simple sense that $\t{H}^2 = \mathbb{I}$.

#### Phase

![phase](../assets/zx-calculus/qc-phase.png)

**Phase** gates are rotations of the state vector about the $z$-axis of the
Bloch sphere, corresponding to shifts of the qubit phase. They're parameterized
by the angle of rotation, as in
$$
\t{P}(\alpha) \Big( a \ket{0} + b e^{\I \pphi} \ket{1} \Big)
    = a \ket{0} + b e^{\I (\pphi + \alpha)} \ket{1}
$$
which corresponds to the explicit forms
$$
\begin{gathered}
    \t{P}(\alpha)
        = \begin{pmatrix}
            1 & 0 \\ 0 & e^{\I \alpha}
        \end{pmatrix}
        ~\swap~ \ketbra{0}{0} + e^{\I \alpha} \ketbra{1}{1}
\end{gathered}
$$
Note that the quantum equivalent of a bit flip can be accomplished with a
combination of two Hadamards and a $\pi$-phase rotation, $\t{NOT} = \t{H}\,
\t{P}(\pi)\, \t{H}$.

#### CNOT

![cnot](../assets/zx-calculus/qc-cnot.png)

The last element in the universal gate set, the **controlled NOT** (**CNOT**)
gate, is a two-qubit gate that performs a bit-flip on a "target" qubit only in
the case that a "control" qubit is $\ket{1}$. (Or rather, since these are linear
maps, only for the part of the overall qubit state that is $\ket{1}$.) This has
the explicit form
$$
\t{CNOT}_a^b
    = \ketbra{0_a 0_b}{0_a 0_b}
    + \ketbra{0_a 1_b}{0_a 1_b}
    + \ketbra{1_a 1_b}{1_a 0_b}
    + \ketbra{1_a 0_b}{1_a 1_b}
$$
if $a$ is the control qubit and $b$ is the target qubit. If we choose the basis
ordering $\{ \ket{0_a 0_b},\, \ket{0_a 1_b},\, \ket{1_a 0_b},\, \ket{1_a 1_b}
\}$, then the matrix for this works out to be
$$
\t{CNOT}_a^b
    = \begin{pmatrix}
        1 & 0 & 0 & 0 \\
        0 & 1 & 0 & 0 \\
        0 & 0 & 0 & 1 \\
        0 & 0 & 1 & 0
    \end{pmatrix}
.$$
Note that this matrix would be different if, say, $a$ and $b$ were swapped.

Using these gates, any quantum circuit can be described on any number of qubits.
If you wanted to actually calculate the action of a circuit -- that is, create a
quantum computer simulator -- the process of translating the gates in the
circuit to matrices is straightforward, after which the appropriate Kronecker
products can be performed and all the matrices multiplied together to arrive at
the complete unitary matrix for the circuit.

## The ZX-calculus
But this isn't the only way to think about quantum circuits. The standard
circuit notation above, with Hadamards, phase rotations, and CNOTs (oh my!) is,
in principle, able to describe any quantum circuit, but it's really just a
notation. Given a circuit diagram, the only real thing to do in order to find
the action of the circuit is to directly translate to matrices and proceed from
there. While it's straightforward, this approach runs into the fundamental issue
constraining the size of current quantum computer simulators: exponential memory
requirements. Specifically, a system of $N$ qubits will require that the
computer be able to keep track of vectors of size $2^N$, and multiply them by
matrices of size $2^N \times 2^N$. There are certain things you can do to
improve on naive implementations that pass around literal vectors and matrices
of these sizes, but the scaling here is fundamentally exponential.

This is where the ZX-calculus comes in. As stated above, it goes beyond mere
notation to become a full graphical language one can actually use to perform
calculations. In addition to definitions for a set of "generators" representing
a certain base set of linear maps, the ZX-calculus also includes a set of
"rewrite rules" one can use to manipulate diagrams in the calculus so that
complicated diagrams can eventually be simplified down so that the actions of
the circuits they represent can be more easily understood. What's more, states
can be inserted both at the beginnings (to model prepared initial states) or at
the ends (to model measurements or generally constrain the system to a desired
outcome) so that the resulting diagram, when fully simplified down, will
directly show the effect of the circuit on specific states -- and this is still
all within the graphical language, no kets, bras, or matrices required.

The example I gave earlier shows a circuit that implements quantum teleportation
of a qubit $\ket{\psi}$ from Alice to Bob. Alice and Bob each hold one of the
two qubits composing a $\ket{\Phi^+}$ Bell state, and we can see that Alice will
perform a CNOT followed by a Hadamard on her qubits. The last boxes on the wires
of both of her qubits denote measurements, and the double lines coming out of
them correspond to "classical" information containing the results of those
measurements (0 or 1) that then inform operations done to Bob's qubit in order
to produce the same quantum state $\ket{\psi}$ at the end.

![zx-teleportation](../assets/zx-calculus/zx-teleportation.png)

On the right side of the double-arrow, though, I've also drawn the ZX-calculus
equivalent of the circuit. We'll get to what all the symbols mean in a bit, but
just to sell ZX-calculus a bit, here's what the application of those rewrite
rules looks like for this circuit:

![zx-teleportation-rewrites](../assets/zx-calculus/zx-teleportation-rewrites.png)

We won't really be getting into the rewrite rules per se or where they come from
(in short, they come from the various category-theoretic properties of the
abstract transformations the symbols represent) but you can see that the whole
process is pretty much just pushing symbols around. And by the end of the
rewriting process, it shows that the circuit implements quantum teleportation
*entirely within the graphical language*.

### Generators
So now we can talk about the elements of the calculus. Broadly, these come in
the form of "generators", which can be connected in various ways by wires. The
wires themselves are mostly unimportant, but the generators have properties and
transform states carried by the wires. Although one usually aims to work mostly
within the graphical language, each generator is defined principally as an
abstract linear map, which means that it is also straightforward to
alternatively translate them to a linear combination of ketbras or a simple
matrix. These alternative representations are useful for generating intuitions
and understanding the behaviors of diagrams when starting out although, as we'll
see in a bit, there exist scenarios where matrices are insufficient to carry out
a calculation.

#### Spiders

![zx-spiders](../assets/zx-calculus/zx-spiders.png)

The first generator we'll encounter is the **spider**. It comes in two flavors,
$Z$ and $X$, and as you can see, spiders can have more than one wire on each
side. In fact, $n$ and $m$ above should both be read as "zero" or more, meaning
that spiders can have any number of wires going into it or coming out.

The $Z$-spider as shown above with $n$ inputs and $m$ outputs and a number
$\alpha$ in its center is defined as
$$
\ketbra{\underbrace{\,0\, \cdots \,0\,}_m}{\underbrace{\,0\, \cdots \,0\,}_n}
+ e^{\I \alpha} \ketbra{\underbrace{\,1\, \cdots \,1\,}_m}{\underbrace{\,1\, \cdots \,1\,}_n}
$$
where the subscripts enumerate subsystems. Correspondingly, the $X$-spider
counterpart is defined as
$$
\ketbra{\underbrace{+ \cdots +}_m}{\underbrace{+ \cdots +}_n}
+ e^{\I \alpha} \ketbra{\underbrace{- \cdots -}_m}{\underbrace{- \cdots -}_n}
.$$

Spiders have some interesting properties. First, you may notice that in the
general case spiders are *not* unitary maps, and can even correspond to
rectangular matrices when $m \neq n$! This is because the ZX-calculus is, in
fact, more general than regular circuit notation: All the linear maps that can
be described by the ZX-calculus form a superset of all possible quantum
circuits. In other words, every quantum circuit is a ZX-diagram but not all
ZX-diagrams are quantum circuits and, as such, care should be taken to make sure
that the map corresponding to a given diagram is indeed unitary, at least when
reasoning specifically about quantum circuits. Because of this, ZX-diagrams are
usually first translated from quantum circuit notation using a list of
equivalences.

Second, it's easy to see that for $m = n = 1$ and $\alpha = 0$, both $Z$- and
$X$-spiders reduce to the identity. This leads to the first of the rewrite rules
we'll encounter, which is called the spider-popping rule: Any spider with one
input and one output with $\alpha = 0$ is effectively the same as an empty wire,
and can hence be removed.

![zx-spider-pop](../assets/zx-calculus/zx-spider-pop.png)

In addition, any spider with $m = n = 1$ is of the form of the $\t{P}(\alpha)$
phase gate from above, with the exception that for $X$-spiders, this corresponds
to a phase rotation in the $X$ basis. Geometrically, we can think of this as
rotating the qubit state vector about the $x$-axis instead of the $z$-axis, but
we can also see this working with kets and bras:
$$
\begin{aligned}
    \ketbra{+}{+} + e^{\I \alpha} \ketbra{-}{-}
        &= \frac{1}{2}
            \Big( \ket{0} + \ket{1} \Big)
            \Big( \bra{0} + \bra{1} \Big)
        \\
        &\phantom{=}+ \frac{e^{\I \alpha}}{2}
            \Big( \ket{0} - \ket{1} \Big)
            \Big( \bra{0} - \bra{1} \Big)
        \\
        &= \frac{1 + e^{\I \alpha}}{2} \ketbra{0}{0}
        + \frac{1 - e^{\I \alpha}}{2} \ketbra{0}{1}
        \\
        &\phantom{=}+ \frac{1 - e^{\I \alpha}}{2} \ketbra{1}{0}
        + \frac{1 + e^{\I \alpha}}{2} \ketbra{1}{1}
        \\
        &= \Big[
            \cos\frac{\alpha}{2} \ketbra{0}{0}
            - \I \sin\frac{\alpha}{2} \ketbra{0}{1}
            \\
            &\phantom{= \Big[}- \I \sin\frac{\alpha}{2} \ketbra{1}{0}
            +\cos\frac{\alpha}{2} \ketbra{1}{1}
        \Big] e^{\I \alpha / 2}
.\end{aligned}
$$
When written out as a matrix, this is
$$
\ketbra{+}{+} + e^{\I \alpha} \ketbra{-}{-}
    ~\xrightarrow{\t{Z-basis}}~ e^{\I \alpha / 2} \begin{pmatrix}
        \cos\frac{\alpha}{2} & -\I \sin\frac{\alpha}{2} \\
        -\I \sin\frac{\alpha}{2} & \cos\frac{\alpha}{2}
    \end{pmatrix}
.$$
For $\alpha = \pi / 2$, this maps $\ket{0}$ and $\ket{1}$ to the $xy$-plane,
while for $\alpha = \pi$, this is a complete bit flip. And, as rotations around
the Bloch sphere, any two adjacent spiders of the same kind can be combined by
adding their arguments. This is the second rewrite rule we'll encounter, known
as spider fusion.

![zx-spider-fusion](../assets/zx-calculus/zx-spider-fusion.png)

Finally, spiders with only a single wire on one side are particularly useful as
well: A spider with no inputs corresponds to a sum of ketbras where all the bras
are empty, making it a state:

![zx-spider-states](../assets/zx-calculus/zx-spider-states.png)

Similarly, a spider with no outputs corresponds to a sum of ketbras with empty
kets, which is called an effect:

![zx-spider-effects](../assets/zx-calculus/zx-spider-effects.png)

Spiders are the heart of the ZX-calculus and, as we'll see, can actually be used
to describe everything else. You may also notice, though, that we're being
pretty loose with our equal signs here -- a lot of those $=$'s should really be
$\propto$'s because a strict equality for e.g. these diagrams here would require
correction with a factor of $\sqrt{2}$ somewhere. Global scalars like these are
generally ignored, with $=$ understood to be "equal up to a nonzero global
scalar", but of course it's possible to maintain strict equality just by
carrying the appropriate factor around for all the calculations.

Alternatively, spiders can also be used to describe pure scalars:

![zx-scalars](../assets/zx-calculus/zx-scalars.png)

which can be shown to allow for the expression of any complex number $w$, for
$|w| \leq 2$.

#### H-Boxes

#### Swaps

#### Cups and caps

### Rewrite rules

## Further reading

- Check out [zxcalculus.com][zx.com] (also linked at the beginning of the post)
  and [this article][zx-pennylane] via PennyLane for tutorials on the
  ZX-calculus and how to use it.
- If you're so inclined, here's a very thorough explanation of the calculus,
  including extensions going beyond the simple elements included here:
  [arXiv:2012.13966][de-wetering].
- Also check out [Bob Coecke's lecture notes][coecke-lecture-notes] to walk you
  through some basic exercises with the calculus.

[zx.com]: https://zxcalculus.com/
[coecke-duncan]: https://arxiv.org/abs/0906.4725
[penrose]: https://en.wikipedia.org/wiki/Penrose_graphical_notation
[repo]: https://gitlab.com/whooie/rust-lib/-/blob/master/lib/zx.rs?ref_type=heads
[bloch-sphere]: https://en.wikipedia.org/wiki/Bloch_sphere

[zx-pennylane]: https://pennylane.ai/qml/demos/tutorial_zx_calculus
[coecke-lecture-notes]: https://arxiv.org/abs/2303.03163
[de-wetering]: https://arxiv.org/abs/2012.13966
[wiki]: https://en.wikipedia.org/wiki/ZX-calculus
[wiki-quantum-circuit]: https://en.wikipedia.org/wiki/Quantum_circuit

